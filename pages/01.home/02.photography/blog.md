---
title: Photography
media_order: 'site_cover.png,autumn_field.jpg,rocky_coast.jpg,mountain.jpg'
hide_git_sync_repo_link: false
body_classes: 'header-dark header-transparent'
child_type: item
aura:
    pagetype: website
    description: 'A blog written by Mathias Hellquist, covering various topics such as privacy, security, innovation, opinions and various world observations.'
    metadata:
        'twitter:image': 'https://imakethingswork.com/blog/site_cover.png'
hero_classes: 'text-light title-h1h2 overlay-dark-gradient parallax hero-large'
blog_url: /photography
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 10
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
bricklayer_layout: false
display_post_summary:
    enabled: false
metadata:
    description: 'A blog written by Mathias Hellquist, covering various topics such as privacy, security, innovation, opinions and various world observations.'
    'og:url': 'https://imakethingswork.com/home/photography'
    'og:type': website
    'og:title': 'Photography | I Make Things Work'
    'og:description': 'A blog written by Mathias Hellquist, covering various topics such as privacy, security, innovation, opinions and various world observations.'
    'og:image': 'https://imakethingswork.com/home/photography/site_cover.png'
    'og:image:type': image/png
    'og:image:width': '1280'
    'og:image:height': '817'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Photography | I Make Things Work'
    'twitter:description': 'A blog written by Mathias Hellquist, covering various topics such as privacy, security, innovation, opinions and various world observations.'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/site_cover.png'
    'article:published_time': '2020-11-10T09:19:45+01:00'
    'article:modified_time': '2020-11-10T09:19:45+01:00'
    'article:author': 'I Make Things Work'
feed:
    limit: 10
    description: 'I MakeThings Work - Sane shit, different mane'
hide_from_post_list: '0'
sitemap:
    changefreq: monthly
modular_content:
    items: '@self.modular'
    order:
        by: folder
        dir: dsc
pagination: true
published: false
login:
    visibility_requires_access: true
unpublish_date: '2024-03-05 10:30'
---

# I Make **Things** Work
## Sane shit, different mane