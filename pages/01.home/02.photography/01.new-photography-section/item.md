---
title: 'New Photography section'
maintitle: 'New <b>Photography</b> section'
media_order: '_DSC8755-Edit-copy.jpg,_DSC8755-copy.jpg'
date: '2020-11-10 21:00'
taxonomy:
    category:
        - photography
    tag:
        - photography
hide_git_sync_repo_link: false
body_classes: header-transparent
aura:
    pagetype: article
    description: 'Introduction of Photography section on this web site'
    metadata:
        'twitter:image': 'https://imakethingswork.com/photography/new-photography-section/_DSC8755-Edit-copy.jpg'
hero_classes: 'hero-fullscreen parallax title-h1h2 text-light overlay-dark-gradient'
hero_image: _DSC8755-Edit-copy.jpg
blog_url: /photography
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
summary:
    delimiter: '==='
hide_from_post_list: false
feed:
    limit: 10
metadata:
    'og:url': 'https://imakethingswork.com/photography/new-photography-section'
    'og:type': article
    'og:title': 'New Photography section | I Make Things Work'
    'og:image': 'https://imakethingswork.com/photography/new-photography-section/_DSC8755-Edit-copy.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '850'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'New Photography section | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/photography/new-photography-section/_DSC8755-Edit-copy.jpg'
    'article:published_time': '2020-11-10T00:08:00+01:00'
    'article:modified_time': '2020-11-10T13:41:07+01:00'
    'article:author': 'I Make Things Work'
published: false
---

This is the first post in the new section on Photography on this web site.

===

Disclaimer:

Firstly, let's get this out of the way: I'm an [amateur][1] photographer. That means I'm not a professional photographer, which in turn means I am not taking photos to make money, I take photos because I...umm...love taking photos.

That also means that what I will be writing in this new section of this web site possibly isn't useful to anyone but me. That is ok. It will also be highly likely that I will not reveal anything new to professional photographers. That is also ok. Just because I learn something new therefore doesn't mean that I say "this is invented by me/someone", it only means "wow, I just discovered this, I had no idea...!".

I'm adding those two paragraphs above as a disclaimer as I've seen comments on various sites where I have been happily learning about things, and commenters say _"New way you say? Pffftt, this technique has been around since the 60's"_ or _"It could have been done differently. Also, you missed XYZ in your photo whilst doing the thing."_ which has led me to believe there are some out there that possibly have forgotten how it was when they were new and enthusiastic.

### My goals

My photos are quite far from perfect, they probably never will be, as there are soooo much to learn, and to think of. That is one of the things I love about photography: the ultimate combination between creativity and technical know-how, both in handling the camera, the subject, and the post-processing of the image afterwards. My photography goal is therefore two-fold:

1. Take photos that I and my friends and family like to view.
2. Make old discarded photos (RAW files) useful again. Perhaps not perfect, at times not even pretty, but at least watch-able for me, as a document of the memory of that moment. The alternative to this is to leave them unseen on Amazon Glacier where I store all my old RAW-files.

This also means that pretty much all people on my photos indeed are friends, family, colleagues, going about doing their thing, and I happen to be around with a camera, very often despite their wishes. :)

Lately, as I've picked up my camera again with new energy after a couple of years break only shooting with my phone, I've learnt loads of new (to me) things though when it comes to photo-editing. Some of that I will share here. It also made me literally (as hinted above) download my old archived RAW-files from Amazon Glacier, and re-edit them, taking a second look at each one to see if there is something that can be rescued/revived and see what I can do about them today, with what I know now.

### Time travel

They date back to 2007, which apparently was the year when I started shooting RAW. At the time I used a Nikon D50, and surprisingly often I apparently had it set to ISO 800 or ISO 1600. I almost never used a flash. 2007 was the first year of my son (who was born in December 2006), we lived in London, and had a very different life to what we have now.  I took thousands of photos. Pretty much all of them slightly blurry and almost all of them grainy/noisy. A great challenge for photo-editing indeed!

Also, going through old photos has been like time travelling for me. I remember places, names and events that I had pushed back in my memory and not picked up on lately, until I saw them on my iMac monitor again and started smiling. Because that is how it usually is when you (well, I) rip out the camera: it is either "normal" times or "happy" times. Most of my photos are at least. I don't tend to take up my camera when it is "bad" or "sad". This means that photos, to me, also can have a healing aspect to them, full of fond memories over times that have been. Testaments of friendships and/or adventure. To me.

Anyways, this is more of an introduction to this new section on the web site. I uploaded some test photos when setting this up on the web site, of my dear friend Patrik, to check if the "before/after" functionality was working alright. It is. I'll just let Patrik stay where he is though, so you can compare what came out of the camera with what I uploaded to Flickr, where I store [all my finished photos][2].

<figure class="image-caption">
<div class="twentytwenty-container">
 <!-- The before image is first -->
 <img src="/photography/new-photography-section/_DSC8755-copy.jpg" />
 <!-- The after image is last -->
 <img src="/photography/new-photography-section/_DSC8755-Edit-copy.jpg" />
</div>
<figcaption>You can find the final version of this <a href="https://flic.kr/p/2k4QQeG"> image on Flickr</a></figcaption>
</figure>

The story behind the photo is that we were going to go hiking in the Swedish mountains last year, with the entire family, kids and all. As we didn't have any hiking experience we had to practice during the weeks leading up to "the real thing". We went out walking in forests and local hills with the full gear.

On one of those occasions I brought my camera, and I literally just ripped it out of the bag and took a couple of shots, without thinking too much about it. Obviously they were extremely dark on the subject. That was what I tried to rectify with this edit: bring out Patrik and push back the background a bit.

The web treatment (i.e. for this web site) oversharpened this version a bit, which becomes obvious in the background if you focus on that, so don't do that. The version on Flickr is a bit "softer" on that. Instead look at how much face details that were hiding in the shadows! :)


[1]: https://www.etymonline.com/word/amateur
[2]: https://www.flickr.com/photos/hellquist/