---
title: 'Tilt-shifting the background'
media_order: 'DSC_2829-header.jpg,DSC_2829-final.jpg,DSC_2829-original.jpg,DSC_2829-walking_blur.jpg,DSC_2829-walking_noblur.jpg'
published: false
date: '2020-11-11 16:30'
taxonomy:
    category:
        - photography
    tag:
        - photography
hide_git_sync_repo_link: false
body_classes: header-transparent
aura:
    pagetype: article
    description: 'Tilt-shift blur on the background to put focus on foreground'
    image: DSC_2829-final.jpg
    metadata:
        'twitter:image': 'https://imakethingswork.com/photography/tilt-shifting-the-background/DSC_2829-final.jpg'
hero_classes: 'hero-fullscreen parallax title-h1h2 text-light overlay-dark-gradient'
hero_image: DSC_2829-header.jpg
blog_url: /photography
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
summary:
    delimiter: '==='
hide_from_post_list: false
feed:
    limit: 10
maintitle: 'Tilt-shifting the'
subtitle: '<b>background</b>'
metadata:
    description: 'Tilt-shift blur on the background to put focus on foreground'
    'og:url': 'https://imakethingswork.com/photography/tilt-shifting-the-background'
    'og:type': article
    'og:title': 'Tilt-shifting the background | I Make Things Work'
    'og:description': 'Tilt-shift blur on the background to put focus on foreground'
    'og:image': 'https://imakethingswork.com/photography/tilt-shifting-the-background/DSC_2829-final.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '888'
    'og:image:height': '1280'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Tilt-shifting the background | I Make Things Work'
    'twitter:description': 'Tilt-shift blur on the background to put focus on foreground'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/photography/tilt-shifting-the-background/DSC_2829-final.jpg'
    'article:published_time': '2020-11-11T16:30:00+01:00'
    'article:modified_time': '2020-11-11T16:43:05+01:00'
    'article:author': 'I Make Things Work'
---

Sometimes the only sharp photo of a given event isn't overly interesting, but you still would like to keep it as a memory. Using some of the oldest tricks in the book, crop and blurring background, can quickly improve results.

===

Going through my photos of when both my mother and my cousin independently visited us in London at the same time, something that didn't happen that often (well, it was that once) we surprisingly didn't get too many photos of them together. I got photos of each of them separately alright, but only a few when they were in the frame at the same time. Pretty much all of the photos that could have become something, either one was blinking or chewing food (people generally hate being photographed whilst eating) or the photo was unsharp or had things in the foreground that couldn't be removed in a sensible way.

Apparently we were out walking one day, and my wife must have taken the camera and was ahead of us, so she turned around and took a shot. She is quite good at getting good pictures, but this wasn't one of the great ones. It is however one of the few sharp shots of me, my mother and also my cousin, with him pushing my son in his pram, that I had, so I decided to work with it to see what I could get out.

This was the original photo, as it came out of the camera:

![Original photo](DSC_2829-original.jpg?classes=caption "Original photo out of camera")

In Lightroom I set the neutral point but couldn't move many other sliders (highlights/shadows) before things started clipping (which is fancy speak for reaching a black/white point that is so low/high that it starts to burn out details in the image). Lots of stuff going on in there, without any of it being of real interest, except for us walking there. Cars. Walls. Trees. Letting signs. More cars. Boring sky. I decided to straighten it and then crop it, to remove as much distraction as possible.

![Straightened and cropped photo](DSC_2829-walking_noblur.jpg?classes=caption&resize=444,640 "Straightened and cropped photo")

A bit better. This is just like zooming, but without actually using as much of your sensor as you'd get with zooming with optics, we have only lost pixels, not won any. Also, when zooming in it is quite common, at least with my lenses, that the background blurs out more. Not so when cropping, as we've only deleted things outside of our subjects. Oh, and it should be noted that I say the image has been straightened, it is the pavement that hasn't/isn't. London eh?

Anyways, as we have come closer within the image, the noise is also now more apparent. I exported it from Lightroom to Photoshop and ran a very mild noise reduction on it in [Topaz Denoise AI][3]. In Denoise AI I added no sharpening and only a slight touch of denoise as it otherwise started removing details of the now-quite-few-pixels-left-as-we-cropped image which quickly turned us all in to cartoons unless I dialled the controls back a bit.

Resisting an urge to increase vibrance in the trees, as that would put more, not less, focus on them I realised I wanted to blur the background more than the very mild blur it had. I needed to first separate out us from the background though. Seeing as we were all wearing clothes of different colours and everything being a tad...dull...I would struggle to colour/luminocity-mask us (with my new fave Photoshop tool, the [TK7 panel][5]) for a good selection. I realised the simplest way to get us selected these days is built-in to Photoshop: Select Subject. It almost got us fully, I only needed to add a few areas with the Quick Selection Tool.

With us selected, on a new layer, I saved the selection as a new channel. Then I went Edit -> Content-Aware Fill and output that to a new layer. This basically removes us from the background on a new layer. If you do that, don't stress over that the content-awareness appears to lead a (very strange) life of its own, the important part is to fill the background with things from the proximity within the photo to decrease halos or outline blurs. The layer on top, with your actual subjects, will cover "the fill" anyways, we only lifted them/us out to protect them/us from the blurring, which happens now.

The new/surprising thing for me (which I learnt from a video, linked below) here was to use the Tilt-Shift blur as opposed to any of the other flavours of blur. The reason though is that you want to put the area in focus, on our feet or lower body. That is what a camera would do too, as our feet are at the same distance as our heads, from the camera. If I were to use for example Iris blur, everything outside of a centre-point would gradually strive to be equally blurry, even the ground we walk on. With Tilt-Shift blur that is not the case.

Whilst I had us on a separate layer I took the opportunity to add some details ([Detailizer][4]), then merged the foreground with the now-blurry-background and ran it through [Luminar 4][1] (affiliate link actually, just so you know), which I do with most of my photos these days, to make colours pop a bit more. It now looked like this:

<figure class="image-caption">
<div class="twentytwenty-container">
 <!-- The before image is first -->
 <img src="/photography/tilt-shifting-the-background/DSC_2829-walking_noblur.jpg" />
 <!-- The after image is last -->
 <img src="/photography/tilt-shifting-the-background/DSC_2829-walking_blur.jpg" />
</div>
<figcaption>Before/after tilt-shift blur on the background</figcaption>
</figure>

After that it was more about going back to Lightroom and select a decent preset. I pick presets based on what I like that day, and apparently I liked this one that day. I don't disagree today, so it was probably alright.

<figure class="image-caption">
<div class="twentytwenty-container">
 <!-- The before image is first -->
 <img src="/photography/tilt-shifting-the-background/DSC_2829-walking_blur.jpg" />
 <!-- The after image is last -->
 <img src="/photography/tilt-shifting-the-background/DSC_2829-final.jpg" />
</div>
<figcaption>Before/after preset in Lightroom. The After image is the <a href="https://flic.kr/p/2k5iwXt">final image</a> that was uploaded to Flickr.</figcaption>
</figure>

Quite an improvement from the original I think. Most of the cars and the loud signs lost their prominence, whilst we, the people, became more in focus. Not without mistakes though, but I let them pass. One was that I had been sloppy in setting the blur on front of us fully out, but that is somewhat hidden in the vignetting (darkening) of the photo edges in Lightroom when I added a preset there.

Secondly, the black pole on my left (right in the image) should have been included in my mask I guess. Now it starts off sharp by our feet, and we have just taken a step or two from it, but it ends blurry in the top, which is kind of strange behaviour for something that probably is as close to the camera at the top as it is at the bottom. Ah well. If I don't mention it you'll probably not think too much about it. ;)

Anyways, now the image fills the intended purpose. It is not a showstopper in any meaning of the word, but it is a nice addition to that time when my mum and my cousin both visited us in London, and it helps me not to forget that time.

Oh, and also quite important: give credits where credits are due. I learnt about the tilt-shift blur in this excellent video on YouTube. Great channel for learning Photoshop related things!

[plugin:youtube](https://www.youtube.com/watch?v=m_Qk8QsLbAs)


[1]: https://skylum.grsm.io/ntdbajsubx9914
[2]: https://www.flickr.com/photos/hellquist/
[3]: https://topazlabs.com/denoise-ai-2/
[4]: https://cc-extensions.com/products/detailizer3/
[5]: https://goodlight.us/writing/actionspanelv7/tk7.html