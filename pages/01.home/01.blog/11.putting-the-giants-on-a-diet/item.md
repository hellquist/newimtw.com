---
title: 'Putting the giants on a diet'
maintitle: 'Putting the <b>giants</b> on a diet'
media_order: 204512.png
date: '2019-03-19 22:55'
taxonomy:
    tag:
        - privacy
        - facebook
        - google
        - microsoft
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/putting-the-giants-on-a-diet
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: 204512.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

In [my previous post][4] I highlighted how Facebook eerily seems to target advertising based on what you post, comment and like (do the "thumbs up", or "heart" etc). I also described an experience that made me feel like Facebook was listening through not only mine, but also a friends iPhone.

===

I have now been told that [it isn't possible to use the microphone in that way][1], and that it would be technically impossible to do for prolonged time or even continuously (i.e "all the time"), which I do agree with.

However, if we are going to be technical about it, and given what Facebook can figure out without a microphone, such as your location, who is on the same address, where you have just been, what you have discussed (on Facebook) etc, it wouldn't be unthinkable that code can be added that turn on a microphone for say 5-10 seconds every 10 minutes given certain pre-conditions were met. A lo-fi audio file/transmission containing only speech wouldn't use up much bandwidth, and would "cost" (network wise) less than the average web site image (such as the one at the top of this post).

I also suspect that for most people who have Facebook installed on their phone/device, it is quite likely to be the largest battery hog when you check battery/network capacity, and the battery history. Perhaps it is because people love the app/service so much that they use the app all the time. Or...?

### Beyond the Facebook microphone

Anyways, if we assume they indeed can't use the microphone, there is still no doubt that Facebook do a lot of "intelligent" ad placement, based on both you and your friends online/offline living patterns, and of course based on what you say/do/interact within the Facebook app. Because they can. [Check out the video over here for example][2]. (The article is possibly locked for you, but the video at the top should still be possible to play.) Just because they aren't using the microphone doesn't mean they are not stalking you.

Keeping tabs on you and what you do is a lot easier for them if they have unrestricted access, within their app, which ideally lives in your phone, which you in turn bring with you at all times. This means they also discourage you using a web browser in your device if a native app exist for your operating system (iOS, Android etc), over which they have very limited control, to access facebook.com, but instead they wish you to use the app, which they can control fully.

There are even (possible conspiracy theory) [reports][3] that if you use Safari/Chrome browsers on your iPad with an external keyboard to access Facebook, they insert double spaces every time you type something, basically to drive you mad enough to give in and just install the app instead. I don't know. But I do know that double space thing drives me mad when I'm using my iPad, and it **only** happens on Facebook.com. I do not have the Facebook app installed on my iPad, which they obviously also know.

There used to be a plugin for Chrome which was called "[Data Selfie][5]". Sadly it has been discontinued, but it was really interesting as it hooked in to Facebooks API's and could measure and report back how Facebook profiled you in realtime, firstly in general, but more importantly/specifically, for their advertising. It did this by counting how many seconds you spend viewing posts from which other users, the contents of those posts. It could obviously also keep track of if you clicked, liked, commented on posts etc, and in the end it summarized your "advertising profile" that Facebook uses as a base for your personal advertising experience within Facebook. You can still use the plugin to track your activity if you are curious, but a feature it used to have, which was to give you predictions on what you *would* do (i.e. in the future) is no longer working as the development of the plugin now has stopped.

My personal Facebook summary, i.e how Facebook has profiled me, goes like this: *"You're a laid back, liberal male who eats out frequently and prefers style when buying clothes and is more satisfied in life than most"*. Right. Ok.

It also has a whole section of what Facebook deems would be my shopping preferences as well as my Health and Activity preferences. You can actually find a more basic version of your own Facebook shopping profile, amongst a limited set of settings that you can adjust if you [check/uncheck the relevant profile settings][9].

If you are curious on what "mergers and acquisitions" Facebook has made over the years I can also recommend having a [look at this wikipedia post][6]. It can help understanding what directions Facebook is looking at developing now and in the future. It definitely hightlights that if you use Facebook, Instagram, WhatsApp etc, your profile is so much larger than just what you post about on Facebook.

### Speaking of Chrome...

But hey, Facebook is not the only unique case here that you might wish to keep an eye on. If you are reading this in Chrome, or on your Android device, perhaps you regularly use Google search (who doesn't?) and perhaps even have a Gmail account, you might want to consider what your combined profile looks like at Google (or Alphabet, which is the main mothership these days). How much, and what, data are you sharing with Google, who happily will scan it all?

If you, to that mix above, add any type of usage of Google Home, Waze, Google maps, YouTube etc, you can be pretty certain that everything that is said about Facebook in both this post and the previous one, also applies to Google/Alphabet. Here is [the list of mergers and acquisitions by Alphabet on Wikipedia][7].

They have your location, what you like, what you don't like, what you read, what you see, what you watch, what you do and what you say to whom. It is very convenient for the users; they can start things off on one device, and finish it off on another device. Because it is all synchronised. Which means Google knows. Which means they will scan it too. As it happens it is also very convenient for Google that people freely give out all this information, as it makes their business strategy so much easier, not to mention ad placement.

### At the gates

Microsoft on the other hand, used to be in everyones computer, then they lost out a little for a bit, and lots of people were unhappy, made a comeback where the focus was on companies/enterprise, and have now worked themselves back to being in the good graces of regular people again. An impressive journey indeed.

But they also have a unique route in to the decision/project/board rooms in most companies, not to mention they already run the most common PC operating system Windows and MS Office, but also can keep track of "who has ever worked with who" via LinkedIn, they have intranet solutions and plug-ins to integrate with all company data, sensitive or not, almost everywhere, and most developers I know can fight themselves bloody to ensure Github, these days owned by Microsoft, is being used as the code repository for their projects. Also products like Skype and Skype For Business obviously generate usage patterns, highlight business deals before they have even happened or existing ones, connectivity streams and users literally ask Skype to ensure the audio quality should be good.

That is obviously on top of the fact that Windows X has been a great success and that it comes with its own web browser, which you might be using to read this post. For the sake of fairness, here is [the list of mergers and acquisitions for Microsoft on Wikipedia][8] too.

I could go on and list Apple, Amazon, Ebay etc and all the (sometimes surprising) assets/services they own, and how well they integrate with things that you may or may not be surprised by, but I think you get my point by now, right?

### So are they all evil?

Well, not necessarily. But they do wish to make as much money as possible, we know that for sure, and for a company this is a healthy goal, so I can't even argue with it. We also know that they are extremely successful in doing just that, as we are talking about the largest companies on earth. Often offering "free" services. Those services take liberties in the data they collect about you. You trade in your privacy. You gain some convenience, as these services are all top-notch and work beautifully, and you pay by surrendering all your personal details so you will be purchasing goods from advertisers who have paid the platform owners for precise targeting, and the advertisers will pay the developers of the platforms for the ads/goods they are producing. Instead of just paying money for a product you have now paid both money and privacy.

Maybe you feel ok with that. Maybe you trust that they won't be hacked and someone will get to your most personal and sensitive data. Maybe you trust them all to not sell the part of the data that you use or contribute with, to someone with lower moral standards who will sell that data to the highest bidder. Maybe.

I am starting to feel that it is time to reclaim some of my privacy though. I am also quite fed up with Facebook to be honest. It is very useful for quite a few things, like remembering birthdays of people I haven't met in 19 years etc, and it is also (actually) really good for a couple of topic specific groups I participate in. I am dead tired of the bias/opinion bubbles though, which are ensured and amplified on Facebook, due to how it works. Maybe it is that I'm tired of politics, chaos and negativity, which are all alive and well on Facebook.

These are my personal reflections. Therefore I'm going to put the giants, all of them, on a diet when it comes to my private data. I shall try to remove the dependencies of any particular one of them. I shall aim to use more decentralised services, preferably free of advertising. I will have a look at various different alternative solutions that might help, as opposed to hinder, my privacy, whilst still using the bits of the mentioned services above that indeed are really good, in the least privacy invading way I can find.




[1]: https://www.wired.com/story/facebooks-listening-smartphone-microphone/
[2]: https://www.wsj.com/articles/facebook-really-is-spying-on-you-just-not-through-your-phones-mic-1520448644
[3]: https://discussions.apple.com/thread/8627792?page=1
[4]: https://imakethingswork.com/privacy/dont-feed-the-giants
[5]: https://dataselfie.it/
[6]: https://en.wikipedia.org/wiki/List_of_mergers_and_acquisitions_by_Facebook
[7]: https://en.wikipedia.org/wiki/List_of_mergers_and_acquisitions_by_Alphabet
[8]: https://en.wikipedia.org/wiki/List_of_mergers_and_acquisitions_by_Microsoft
[9]: https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen