---
title: 'This post could have been about the climate change'
media_order: 171696.jpg
date: '2018-10-10 21:43'
taxonomy:
    tag:
        - climate
        - facts
        - idiots
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /climate/this-post-could-have-been-about-the-climate
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: 171696.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

I was going to post a link on various social media services, to [an article](https://www.bbc.com/news/science-environment-45775309) I just read on BBC about the horrific development regarding climate change, but realised I probably shouldn't.

===

It won't make a difference, and the usual comments will begin, which will make me despair:

* *-"Look at the source! They can clearly not be trusted! Only 3 years ago they published something that turned out to not be true!"*
* *-"It is all a hoax!"*
* *-"I don't like these so-called facts and find them highly debatable."*
* *-"You are a stupid sheep if you believe things like these!"*
* *-"This is just a feeble attempt at a globalist conspiracy to unite the world for some unknown evil reason!"*
* *-"So what about Israel? Mmm? You are fake if you claim to care about this but don't care about what is going on in Israel!"*
* *-"It is Soros, I told you ages ago!"*
* *-"It is all part of a master plan conspiracy, to keep us suppressed..."*
* *-"The article authors second cousin AS WELL AS her great grand father had something fishy to do 37 years ago with the Home Office (who obviously can't be trusted), this is clearly an attempt to get revenge via a yet-to-be-discovered plan. And Soros."*
* *-"This doesn't apply to me, I live in Sweden, and we are quite good on these things, and besides we are such a small country nothing we do will matter for the big picture anyways."*
* *-"If this was true I should be able to wear shorts on a beach right now. I can't. It must be a lie."*

How did we get to a state where *"it could be right, but then it only truly applies after I've died, but it could also be wrong, so let's do nothing, or if possible, make it worse, quicker."*

How did we get to a state where people see and hear facts, provided by professional experts, and dismiss them because the people themselves don't understand them? Somehow they trust what their doctor/dentist/mechanic says, even if they don't understand it themselves, and they trust the education and experience those experts have...but still the people don't trust most scientific experts within that experts own field, especially if that field of expertise seems to mean we have to change how we do something we like, or at least are used to.

How did we get to a state where conspiracy nutters, often with very little understanding of how science actually happen, are allowed to confuse the scientific meaning of [Scientific Theory](https://en.wikipedia.org/wiki/Scientific_theory) with the more common word "[theory](https://en.wiktionary.org/wiki/theory)" that has a *"he says, she says, it could be either"* meaning, as opposed to the rigorous work that actually goes into presenting a scientific theory, which is testable and so far, after a lot of testing, proven correct?

How did we get to a state where when grave scientific facts are presented, and a vast majority of the scientific community agrees on something, instead the debate amongst people is not related to the topic in question, but instead focuses on if it is 97% or 99% or 90% of the scientists that agree on a specific formulation? And that we use those odd-one-out 3%, 1% or 10% to hide behind, saying *"well, not ALL scientists seem to believe this! Therefore I'm going to do nothing! Hah!"*

How did we get to a state where caring about other people or caring about the environment is sneered on as "[liberal](https://en.wiktionary.org/wiki/liberal)" or ugly and bad?

I don't have the answers. I have my own theories on the reasons for the current state in the world. Those theories could be wrong, so I shall not present them here. Those theories of mine are not Scientific Theories. They are highly subjective self observed theories, based on personal experience, and yes, I know the difference a subjective theory and a Scientific Theory. Do you?