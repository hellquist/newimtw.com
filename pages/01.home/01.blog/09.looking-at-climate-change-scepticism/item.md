---
title: 'Looking at Climate Change Scepticism'
maintitle: 'Looking at'
subtitle: '<b>Climate Change Scepticism</b>'
media_order: '103048.jpg,carbon_brief.jpg,graphs_bloomberg.jpg,graphs.jpg,Predictions1976-2011.png,the_sun.png'
date: '2018-10-26 23:09'
taxonomy:
    tag:
        - climate
        - facts
        - change
        - sceptics
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: 103048.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

Firstly: Climate Change isn't, or at least shouldn't be, a political issue. It should be about facts when it comes to confirming it is happening, or not. The path of actions one might take to combat Climate Change might well be political, as I'm guessing most political parties would like to put their stamp on the solution.

===

That I'm currently not concerning my brain with at the moment. This series of articles is more about the fact Climate Change happens, now, and that it has been confirmed by scientists and politicians, even countries, of opposing sides. It is my opinion we should move the discussion to be about solutions, instead of discussing if it is happening or not.

Secondly: thanks to those who have replied to [my previous post][5]. I have a few things to look into which I will do, and I might make a follow-up post later, *if* I have learnt something new. Keep it coming though.

Meanwhile:

When I have been looking into Climate Change scepticism one question has always come back in my head: *-"if the data sources are public, **why** haven't the sceptics created models based on the data, that **prove** their point?"*. That would at least open up for some **proper** real debate, which then can be (scientifically) proven or disproven, which surely would (or at least should) be the point. The question in my head stems from statements that I keep seeing such as *-"IPCC is using all the wrong models"* and *-"IPCC haven't taken the sun into account in the report I saw"* etc.

### The sun

When I, as a layman, looked into it, the thing that sticks out to me is this:

> *-"The sun's energy fluctuates on a cycle that's about 11 years long. The energy changes by about 0.1% on each cycle. If the Earth's temperature was controlled mainly by the sun, then it should have cooled between 2000 and 2008."* [Read full article here][6]

[ ![6](the_sun.png) ][6]

That (link, above) gives some clarity to that, as earth *clearly* didn't get cooler between 2000 - 2008. The other part of the answer to why the sun isn't represented separately in the [IPCC model][8] (which mainly [concentrates on Climate Change caused by humans][9]) is that it is already accounted for as one (of many) variables. It should also be noted that the [latest IPCC report][10] is a "special report", outside of their regular report schedule, which was targeted with being a status update on how we (earth/humans/mankind) are doing with the 1.5C warming target. There are multiple variables that **have** been taken into account, in this report and more importantly in the other reports released by the IPCC. Some of those variables have natural causes, like the movement of the sun, earths orbit, volcanoes etc, which are very hard to influence, no matter how hard you try. That doesn't mean they aren't observed or that they aren't taken into account. Those things can't be influenced by any actions we humans take though. We can't "fix" them.

I think [this presentation][2] illustrates it best (click the link and slowly scroll downwards to see the animated slides): [ ![2](graphs_bloomberg.jpg) ][2]

**Do note that you can download the data sources yourself** and build your own illustration in whatever fashion you'd like. The data observations are hard to deny, as they are just that: factual observations and measurements from a given point in time, at specific locations. They are not speculation, they are facts. Being able to download the data freely is also true of the [IPCC data][1], which you, me or anyone are free to download as well. Build your own model! Yay!

More importantly, in the Bloomberg data graph above (which is based on data from [NASA][7]) you can see how all the factors are taken into account. That includes sun movements, orbital paths, volcanoes, spray cans, deforestation etc. The only things of those that we can influence, and/or change, are the things caused by humans, such as deforestation, aerosols and greenhouse gases from factory pollution, cows farting etc. Those are the ones we might be able to fix. This is, obviously, also the reason why organisations like IPCC focused on global warming caused by human activity in their latest report, rather than focusing on things we can't change or affect.

That doesn't mean those other variables aren't accounted for. They are just not presented separately as they are of less interest when we talk about what we can do about solving our part of the problem. Pretty simple stuff if you ask me, and totally logical. It is exactly how most of us who work with problem solving, in whatever field, works: things that we can't affect/change are taken into account, but aren't necessarily part of the proposed solution over what we can do, as we literally can't do anything about them.

### So how has the previous IPCC Climate Change projections turned out?

As a layman I'd say: *"Pretty well by the looks of it"*. As we now are talking about historic predictions about a "future-to-come" at the time of the prediction, that historic future-to-come is **now** our past. We can now compare the previous predictions with how it actually turned out in reality. Again, this is very easy to verify, and can't be modified in hindsight: the predictions were made. Time passed and new observations were made. We now compare the prediction to the known reality. The predictions turned out to be pretty accurate. Again, pretty simple stuff.

As we are talking about predictions we should be aware of that it is in all honesty guesswork, based on current/prior knowledge. Sometimes it is slightly higher, sometimes it is slightly lower. Techniques and technology has helped to improve the predictions over the last 4 decades (which is the time span for when predictions have been made, at all), and they keep improving, but overall the predictions by those who claim global warming is happening has panned out more or less as they had predicted (with one glaring exception, Kellogg, see further down). This site showcases it far better than I can:
[https://www.carbonbrief.org/analysis-how-well-have-climate-models-projected-global-warming][3]

[ ![3](carbon_brief.jpg)][3]

### What about models from the sceptics?

It turns out the sceptics also somewhat model the data. Sadly, for them, they repeatedly seem to be quite inaccurate, which obviously doesn't strengthen their arguments. Remember, their historic models and predictions are also factual in the way that the predictions indeed were made. Also remember, they have access to the very same public data as anyone else. Compared to the reality they wished to cover the observed data for that time period turned out to be rather different than the models made by the sceptics. That makes them inaccurate. Simple as that.

I'm not saying that to be mean or because I have any particular agenda; I don't (apart from *"may the science win over stupidity"*...other than that I have no agenda). But they are, so far, clearly inaccurate and that is a fact. That doesn't mean all models or predictions going forward will be incorrect from them. Again though, if you on one side have one or several organisations that repeatedly are more or less correct, and another side of sceptics that claim the correct side *actually* is wrong, but the sceptics don't have the data or predictions to *prove* the others are wrong, the sceptics arguments lose a bit of their value, at least to me.

**Example**: John Maclean predicted 2011 would be the coolest year since 1956, possible even colder than anything previously recorded. It was based on his own model.

**Reality**: 2011 turned out to be the ninth warmest year ever recorded. I think it is safe to say Maclean missed a variable or two in his model.

### ...and all together now

In this graph you can see what the measured temperature was as well as graphs with the various predictions between 1976 - 2011 laid on top of it.

[ ![https://skepticalscience.com/news.php?n=1055](Predictions1976-2011.png) ][4]

<p style="font-size: small; color: #999;">Footnote to that graph: most models and predictions do a couple of scenarios: worst/best/likely cases. The data in that graph only represent the one single case from all sources that is the closest to the actual recorded reality.</p>

As can be seen, some of them are wildly incorrect, whilst some of them are fairly correct. Maclean is the darkblue line at the end of the graph, though I would say Kellogg (1979), Lindzen (1989) and the two Easterbrook predictions (2008) are almost off the charts in their incorrectness as well. It is safe to say Kellogg, who indeed predicted global warming, got it wrong, but it is also interesting that the sceptics not only can't get it right themselves, but repeatedly use that single Kellogg example as an argument to discard all the correct later models too, that actually got it right at later dates, and which uses completely different models than Kellogg used.

That seems to be a tactic used repeatedly in other areas as well: cherry-pick what they don't agree with, and instead of trying to understand that particular nugget they use it to discard everything else, whilst they themselves can't produce models or predictions further than a napkin drawing.

Anyways, just thought I'd share it as it is somewhat related to [my previous post][5].

<p style="font-size: small; color: #666;">Footnote: I'm by no means an expert in Climate Change myself. I'm just interested in facts and I'm tired of attention seekers that scream hoax or claim elaborate conspiracy theories at every opportunity, without being able to prove the topic in question wrong, or to give any hint on the conspiracy either. Noise makers. I do think the future planet Earth will be of importance to my kids though.</p>

[1]: http://www.ipcc-data.org/
[2]: https://www.bloomberg.com/graphics/2015-whats-warming-the-world/
[3]: https://www.carbonbrief.org/analysis-how-well-have-climate-models-projected-global-warming
[4]: https://skepticalscience.com/news.php?n=1055
[5]: https://imakethingswork.com/climate/is-climate-change-a-hoax
[6]: https://skepticalscience.com/solar-activity-sunspots-global-warming.htm
[7]: https://www.giss.nasa.gov/
[8]: https://www.ipcc.ch/ipccreports/tar/wg1/044.htm
[9]: https://en.wikipedia.org/wiki/Intergovernmental_Panel_on_Climate_Change
[10]: http://www.ipcc.ch/report/sr15/