---
title: 'Back to Blogging'
maintitle: 'Back to Blogging'
published: false
date: '2021-07-28 16:28'
taxonomy:
    tag:
        - blogging
hide_git_sync_repo_link: false
body_classes: header-transparent
hero_classes: 'overlay-dark text-light parallax'
hero_image: turtle.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: turtle.jpg
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'Suggest what topics I should prioritize blogging about next'
    image: turtle.jpg
summary:
    enabled: '1'
    delimiter: '==='
metadata:
    description: 'Suggest what topics I should prioritize blogging about next'
    'og:url': 'https://imakethingswork.com/blog/back-to-blogging'
    'og:type': article
    'og:title': 'Back to Blogging | I Make Things Work'
    'og:description': 'Suggest what topics I should prioritize blogging about next'
    'og:image': 'https://imakethingswork.com/user/pages/01.home/01.blog/33.back-to-blogging/turtle.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '712'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Back to Blogging | I Make Things Work'
    'twitter:description': 'Suggest what topics I should prioritize blogging about next'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/user/pages/01.home/01.blog/33.back-to-blogging/turtle.jpg'
    'article:published_time': '2021-07-28T16:28:00+02:00'
    'article:modified_time': '2021-07-28T15:37:15+02:00'
    'article:author': 'I Make Things Work'
---

Suggest what topics I should prioritize blogging about next.

===

As always with me, and many other bloggers-for-fun, I have been quiet for a while. That is just how it is; I blog in bursts when I have something to write about.

Since my last posts a lot has been happening though, on many fronts, and just going through things, in an attempt to post something that is worth writing about, I'm not really sure where to start to be honest.

If you have opinions on what you'd like to hear more about, please let me know. These are some of the things that spring to mind as potential topics:

* My conclusions (time snapshot for now, as it keeps evolving) on alternatives to Social Media services. Things like Fediverse services (Mastodon, Pleroma, Friendica, Pixelfed, Lemmy etc, all of which I have explored quite a bit.

* Password storage services. Basically a continuation on what I started [some time ago when I was using LastPass and Authy][1], but have since moved on from using.

* Secure and encrypted messaging alternatives to Facebook Messenger and WhatsApp. I have tried and tested most of them I think. For me, I have boiled them down to a small selection of preferred solutions, not just [Signal][6], and I could elaborate on "why".

* Note taking solutions. A continuation from [my post on Evernote][2], which I have since moved on from using, after testing a bunch of alternatives.

* Infosec/Cybersecurity, and learning about it. This is where I spend a lot of my time on right now (look at the right-hand side side bar) as I have always been interested in these topics. This is obviously also related to my views on Privacy and IT Security in general.

* I have been exploring options for home (as opposed to business) security software such as anti-virus/anti-malware for the family. This has become more and more interesting to me as my kids, now 14 & 11 years old and with their own devices/computers, are exploring the realms and possibilities of the Internet, whilst I still wish to **not** introduce lots of security issues for my home network. In a sense a continuation on my post of [The Family IT Policy][3].

* As for the [Photography section][4] of this site it seemed like a great thing at the time when I set it up, but since then I've had a disastrous hard drive failure, and I haven't really had much time to spend on my learnings for image editing. Still a passion of mine, but currently in hibernation. Considering just removing that section of the site for now.

* Securing web servers and web applications, such as the server that hosts this very blog, is possibly always of interest for those that do similar things.

* I always have views and opinions on [various conspiracy theories][5] and what drives them.

Those are some of main things that interest me at the moment (apart from the usual "gaming" and "creating music", of which I usually blog about neither as I don't have much to add to the already great selection of blogs and video channels that already cover those topics).

Anything in particular you think I should write about? Get in touch, let me know!

[1]: https://imakethingswork.com/blog/use-a-password-manager
[2]: https://imakethingswork.com/blog/using-evernote-productively
[3]: https://imakethingswork.com/blog/the-family-it-policy
[4]: https://imakethingswork.com/photography
[5]: https://imakethingswork.com/blog/what-is-qanon
[6]: https://imakethingswork.com/blog/you-should-install-signal