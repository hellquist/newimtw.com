---
title: 'Moving from Facebook'
media_order: 112090.jpg
date: '2019-09-29 17:48'
taxonomy:
    tag:
        - privacy
        - facebook
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/moving-from-facebook
hero_classes: 'overlay-dark-gradient text-light title-h1h2'
hero_image: 112090.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
maintitle: 'Moving <b>from</b> Facebook'
subtitle: '<b>Because it is time</b>'
---

So the time has come to cut the Facebook dependency. Cutting the cord if you like. Basically I will limit my Facebook activity quite a lot more than I already have. I will not "quit" Facebook though. This is how it will work.

===

As you might have gathered from my previous posts I've been less than impressed with mine (and others) dependency on Facebook (and also Google services). One might have many concerns about what data we share, and how, when and where we share it, but I think the final trigger for me has been the following questions:

* Do I wish to contribute to [data collection that will, and has, destroyed democracy][7] as we know it?
* Can I be sure that what I think is "right" _actually_ is based on my own critical thinking, and not controlled by Facebook (and Google) by [clever placement of both advertising and "news feed" aggregation][8] from "selected" friends and/or companies who "think alike" and thus has created an opinion bubble for me?

The answers to both questions are "no". That scares me.

It scares me enough to decide I will severely limit my interactions with both Facebook and Google. However, I will not "leave" or "quit" Facebook. There is no point. You can read [this article on the topic of leaving Facebook][1] if you like, but the tl;dr (too-long-didn't-read) is: They will be able to keep track of most of my interactions with other Facebook users anyways, regardless of if I leave or not. That pretty much includes most citizens of the "western world". **I do have some tips on how to limit that tracking too though, but that is for another article**.

So, instead of ignoring the beast and pretending it isn't there, I have come to the conclusion that my best route of action is to limit what I do there, and how I do it. Limit the data they can collect from me going forward. Lately I have mainly re-shared meme's and done "happy birthday" greetings. I have not done any other posts with original content, and have also refrained from commenting/liking on others posts. I will do those things even less going forward. Everything else I will do elsewhere.

* I will keep my Facebook account and (only) participate in closed interest groups I'm in.
* I will share my new posts on this blog on Facebook.
* I will be contactable on Messenger, but only when I login in a (controlled) desktop browser, so if you are in a rush, use something else to contact me (see below).
* I have already started leaving "groups" and/or "chat groups". It isn't personal, but do feel free to join me elsewhere, where we can talk/discuss freely without being put under surveillance, and without contributing to creating a data pool that is worth doing illegal activities to get hold of.

You will be able to find me here:

* Email (mathias.hellquist@ this web site domain for example, or firstname@lastname.eu as well)
* Phone (and text/SMS, though I prefer Signal as it is encrypted and private)
* [Instagram][2]
* [Twitter][3]
* [Signal][4] (you need my phone number to be able to add me though)
* [Mastodon][5] (social network alternative to Twitter)
* [MeWe][6] (social network alternative to Facebook)

On the topic of email I spent quite some time setting up secure [PGP encrypted email capabilities][10] on most my email addresses, only to realise not that many uses encrypted emails anyways. If you do however, you can [download my public keys][9]. Do remember that encrypted emails only encrypt the body text, not the to/from field and also, more importantly, not the subject field.

That's it. See you next time, wherever that might be.
 
[1]: https://interestingengineering.com/deactivating-your-facebook-is-a-waste-of-time
[2]: https://www.instagram.com/mathiashellquist/
[3]: https://twitter.com/hellquist
[4]: https://www.signal.org/
[5]: https://mastodon.social/@hellquist
[6]: https://mewe.com/profile/5d4166a9ad0d76040d68f92c
[7]: https://imakethingswork.com/privacy/watch-the-great-hack
[8]: https://imakethingswork.com/privacy/are-you-leaking-personal-details
[9]: https://keys.openpgp.org/
[10]: https://protonmail.com/blog/what-is-pgp-encryption/