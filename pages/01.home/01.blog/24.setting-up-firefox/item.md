---
title: 'Setting up Firefox'
media_order: 'firefox-wallpaper-2.jpg,badges.png,facebook_barred.png,map.png,fbpurity_stylish.png, pref_content_blocking.jpg'
published: true
date: '2019-09-30 15:08'
taxonomy:
    tag:
        - privacy
        - browser
        - firefox
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/setting-up-firefox
hero_classes: 'text-light overlay-dark-gradient title-h1h2'
hero_image: firefox-wallpaper-2.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: firefox-wallpaper-2.jpg
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'You should use Firefox instead of Chrome, but you should set up Firefox correctly. This is how.'
    image: firefox-wallpaper-2.jpg
maintitle: 'Setting up <b>Firefox</b>'
subtitle: 'for <b>Privacy</b> and <b>Security</b>'
---

I have [previously said][1] you probably want to, or at least should want to, move away from Google Chrome as your web browser. I also listed some alternatives, but reading back I noticed I wasn't more specific in how you can, and probably should, set those alternatives up. Let's start with Firefox.

===

After some deliberation, I have now decided to use Firefox as my main web browser. I had some quality time with [Vivaldi][2], but unfortunately Vivaldi isn't available on my mobile devices. It is still my second browser on my desktops though, and I have it fully configured, etc.

On my mobile devices I had some good experiences of Brave browser. Then I learnt that Brave browser had some [worrying funding schemas][3] going to extreme right-wing groups, and also that the former (sacked) Mozilla CEO, who was funding campaigns out of his own pocket to stop LGBT-people from marrying etc, now was working in Brave, I figured I should revisit my options, as there are some things I do not wish to support.

### Set up Firefox

[Firefox][4] has been around for a while, and its latest generation of browsers are top notch. They have also built in privacy conscious controls for pretty much everything, and if you know what settings to tweak and what add-ons to...umm...add, it can be made very secure, privacy conscious and still usable. It works fine on all platforms, such as desktops and mobile devices. It also can sync your bookmarks and other settings between your devices. In short, you can have all the benefits of Google Chrome, without selling your soul, and your data, to Google.

!!! It should be noted that you should aim to keep your add-ons to as few as possible. The more add-ons you add, the more likely you are to become an unsuspecting victim of bad code somewhere, so add-ons should be considered carefully before installing them. I will suggest a couple of add-ons that you should install, which can help you increase your privacy.

If you have downloaded Firefox and you wish to use the multi-device sync functionality, you will probably wish to create a sync account, but this is entirely optional. If you have multiple devices and you do not wish to create an account, you can instead set up Firefox for each of your devices. I like having a sync account though, so I will assume you do too.


### General preferences

![Preferences - Content Blocking](pref_content_blocking.jpg?resize=335,411&classes=caption,caption-right,figure-right "Preferences - Content Blocking")

Next you should open up your "Preferences" and go to the "Privacy & Security" settings. Start with scrolling down to "Firefox Data Collection and Use" and uncheck all the boxes there.

Whilst you are in Privacy & Security, find the section called "Content Blocking". There are three options there: Standard, Strict and Custom. I have mine set to "Custom", with the following block settings:

- Trackers (In all windows)
- Cookies (Third-party trackers)
- Cryptominers
- Fingerprinters

I guess that is the same as the "Strict" setting, but with custom it feels easier to control with more granularity, and to set exceptions, etc.

Next, under the "Security" section of the Privacy & Security page, ensure you have all the boxes ticked.


### Search engine

Finally, under the Search button you should change your search engine from Google. I would suggest having [DuckDuckGo][5] as your default search engine. Personally I'm using a service called [YubNub][6], and have done so for years, but that is possibly the basis for another article I guess. Basically, YubNub turns your address field in your browser into a fully functional command line, which can be very handy for someone like me, but probably is overkill for most of you.

The search engine I'm using for internet searches, via YubNub, is DuckDuckGo, as it cares about your privacy. Protecting your privacy is even the unique selling point for DuckDuckGo, who will not track you or your search history in any way what so ever.


### More advanced

For the next thing on the list you need to open up a new tab in your (Firefox) browser, and type "[about:config](about:config)". It will ask you if you are totally sure. You (hopefully) are so you say yes. You will see a lengthy list of configuration settings.

Now we will disable the WebRTC, which stands for "Web Real-Time Communication". It might sound like an excellent thing, but it enables real-time with no additional plug-in. The cost of doing so is that it reveals your actual IP address, **even** if you are on a VPN which might have given you a "hidden" IP address.

In the search bar at the top, type in "media.peerconnection.enabled". It will filter down that list to **only show you one line of text**. If the "value" says "true", double click on it and it will say "false" instead. You can test this setting over here: [www.perfect-privacy.com/en/tests/webrtc-leaktest][7] to see if you've enabled it correctly. Basically, it should not detect your IP-address under that test.

### Installing Facebook Container add-on

![Facebook - Barred](facebook_barred.png?classes=caption,caption-right,figure-right "Facebook - Barred")

Firefox has an add-on called "[Facebook Container][8]", which you wish to install. This add-on does a great job in ensuring Facebook can't track you on various sites as you go about shopping, browsing or doing things that you normally don't associate with Facebook, but where those little "log-in with Facebook" or "share to Facebook" buttons are. As soon as this add-on detects links going to Facebook from a site, it will put that link behind "bars" and disable it. You can, if you wish, enable it to function normally, but if you don't, Facebook will not get notified of you even being on that site. This add-on is created by Mozilla, the creators of Firefox.

### Installing ad blockers and tracker blockers

After that you wish to install the following add-ons too:

* [Ghostery][9]
* [Privacy Badger][10]

Ghostery is created by [Cliqz][12], which has many privacy-related solutions, and Privacy Badger is created by [EFF][11], which stands for "Electronic Frontier Foundation" and is a well-known actor in the space of protecting the privacy of end-users online within a multitude of areas.

Basically, these plugins are blocking various trackers and tracker scripts, and they are pretty much just "install and forget" type of add-ons. There _can_ be instances where you notice a particular website is not working because of extensive blocking of various scripts, but in those cases it is simple enough to just click the little icon in your browser and enable that script to run. It hasn't happened a lot for me, and I probably browse more things on the internet than most. More importantly, it puts _me_ in control over when I will allow certain tracker scripts and/or images to load.

![Badges](badges.png?classes=caption,caption-right,figure-right "Badges")

Another thing you should install is a "regular" ad-blocker. I have opted to go for [uBlock Origin][13]. There are others, but some others have been criticized for collecting data, or for allowing some advertisers but not others, etc. I have, so far, not found any similar information regarding uBlock Origin, and thus has opted to use that.

By now, if you've installed all three add-ons, you will have three icons to the right of the address bar in your Firefox browser. Those little icons will highlight when they are working. On this site you will notice they say "0" in their little notification bubbles. That means this website has absolutely no tracking and absolutely no advertising on it.

!!! <b>Addition 5th May 2020:</b> In the new design of this site they will show you 4 (Privacy Badger) + 2 (Ghostery) indications. I still do not do any tracking here, but I use a web font from [Adobe Typekit](https://fonts.adobe.com). Also, I have a [Twitter feed](https://twitter.com/hellquist) in the sidebar. Both companies could deduct that this site has visitors. Their key focus is not to track users as much as they aim to track usage of their services, to enable them to scale computing resources better. Finally, there is an image being linked to [Creative Commons](https://creativecommons.org/), because of the license I have opted to use on this blog. I hope that is alright.

If you wish to see the three plugins work hard, I suggest you go to Swedish newspaper [Aftonbladet.se][14]. When you get there, those little notification bubbles with show completely different numbers. As I'm typing this, I notice my window in the background, on which I went to aftonbladet.se, currently is up to 16 scripts blocked by Privacy Badger, another 3 scripts have been blocked by Ghostery, and 34 ads have been blocked by uBlock Origin. When I scroll down that page just a little, uBlock Origin shows it has blocked 89 (!) ads, and it is still ticking up steadily. That is over 100 requests (!) to servers that has been blocked. Also, the add-ons have blocked you from being tracked, so whatever you do on the site will not follow you around when you continue surfing the web elsewhere. Quality stuff.

### Install HTTPS Everywhere

Another add-on you wish to install is [HTTPS Everywhere][15].
It does pretty much what is says on the tin: it creates all your requests to web sites over HTTPS, where the "S" in "HTTPS" stands for "Secure".

When your browser tries to access a website (for example https://wikipedia.com) your browser makes a series of "hand shakes and introductions" to various internet servers. The only way to ensure that any of those handshakes are happening with the correct server is validated by certificates that are tied to their URL and IP addresses. If your connection happens over HTTPS, you will have a much stronger guarantee that the site you think you are visiting truly is that site, and not a site that has been "impersonated" the site you are trying to reach.

HTTPS Everywhere ensures you are truly using the HTTPS protocol at all times and that the site you think you are visiting truly is that site, and that it hasn't been "[spoofed][16]".

### Install your VPN providers add-on

This is assuming you have a VPN provider, which you should. In my case I use [NordVPN][17], and they have their own add-on called [NordVPN Proxy Extension][18]. For NordVPN I also have [an app installed][19] on all my machines/devices, enabling me to connect the device as a whole to VPN (meaning _all_ connections will go through VPN), but sometimes that isn't needed (when you need to use other services on your local network for example). With the add-on I can be connected to my regular network if I wish so, and still enable Firefox to connect via NordVPN, separately.

### So far so good...

By now you have massive improvements in securing your Firefox browser compared to how it was. A bonus is also that it isn't a browser made by Google, which means “Google and its advertising network”, are in your control now as far as tracking you and showing you ads is concerned. You now will be protected from all tracking and advertising scripts. If you are on a VPN, you cannot be seen on your original IP-address either, and your browser will not be “leaking” personal data. You are now massively better off.

But don't just trust me on this, test it out for yourself! Ensure you have everything enabled, and then you [go and re-read a previous article][20] I wrote on checking how much data you might (or could) be leaking. If you did that test previously, or if you'd like to do it now in another "non-secured" browser such as Chrome to compare with, you can now marvel in wonder at how different the results will be in your Firefox.

That is it for securing your Firefox browser.

### But wait, there is more!

There are a couple of additional things you still can install that perhaps are not crucial for security/privacy, but that can be very helpful indeed when surfing around on the internet. Further to the add-ons above, I most often have the following add-ons enabled:

![F.B. Purity and Stylish](fbpurity_stylish.png?resize=513,481&classes=caption,caption-right,figure-right "F.B. Purity and Stylish")

* [F.B. Purity][30] - This add-on makes Facebook bearable, as it allows you to **completely** customize Facebook in your browser. Wish to remove ads? Done. Would you like to see your "News Feed" in chronological order instead of what Facebook has chosen for you? Simple. Wish to not see "suggested friends"? Easy. Dislike seeing stories? Check a checkbox and they're gone. Don't want the right-hand side bar? Just remove it. This add-on is so effective that Facebook has banned its link/URL to be posted on Facebook, as it disrupts their money flow too much, but Facebooks money flow is hardly my concern.
* [Burner Emails][21] - lets me quickly create a fake email address for sign-up forms when needed.
* [Cookie Quick Manager][22] - To delete cookies from a specific site easily. This can be very handy, especially when used with the add-on below, as it can remove all those _"you have viewed 2 out of your 3 free articles this month"_ messages some sites use.
* [Firefox Multi-Account Containers][23] - This extends the containers described previously in this article and will let you create your own containers for various things. For example, you could put all newspapers that use cookies to regulate amount of articles they allow you to read in a container you've named "Reading". You can then force those web sites to always open up in that container, in which you ensure you always delete cookies before closing that tab, so it is always "fresh". The "Multi-Account" part in the name also indicates another benefit: you can be logged in to the same service multiple times if you have multiple accounts for a service. This would otherwise be impossible, as your browser would "know" you are logged in with one specific account, and it's tedious to open up new "private" tabs all the time and keep logging in.
* [I don't care about cookies][24] - This will remove all the messages saying "we are about to set a cookie, do you agree?" messages. Despite the name of this add-on I care about cookies, but it is a fact that pretty much all sites (except this one) will set a cookie, and I don't need a message about it beforehand and I most definitely need not to press "agree" on it for every site. It will be set. I will delete it afterwards if I choose to (with the Cookie Quick Manager, above).
* [Greasemonkey][25] and [Stylish][26] - These two allow you to add your own custom code to any site, provided you know how to code for the web, though there are also ready-made code snippets that you can download if you don't know how to code. You can for example increase the text size of headlines on a specific site, or you can change the entire colour scheme on another. I run a Stylish script for those rare occasions I go to Facebook, which [makes Facebook dark][27]. On [another forum site][28] I run a script to highlight which posts I've read (they are dimmed) or which posts are new (they get another colour), etc.
* [LastPass][29] - LastPass keeps track of all my passwords, on all my devices and in all my browsers, so I don't have to.

### That is it

Given the time we spend in our web browsers when we are using our computers, this is well worth doing. The aim with this is to inform you on how I took control over my own data, and when I share what. It is up to you to do whatever you think is correct/right for you to do.

It can be noted that most of these add-ons (also called "extensions" in some browsers) are available for other browsers, even Google Chrome. However, as long as you are logged in to Google Chrome with your Google account, Google will always be able to follow you around, regardless of what extensions/add-ons you install. Your browser will send your browsing history to Google, along with everything you type into Google Search (which is the major reason you should be using Duck Duck Go for searching ever after).

I have all the above set-up for Vivaldi browser too, which serves as my secondary browser these days.

Also, remember to get a VPN provider (NordVPN in my case) and a Password manager (LastPass in my case), if you haven't already.

 
[1]: https://imakethingswork.com/privacy/browse-in-privacy
[2]: https://vivaldi.com/
[3]: https://threadreaderapp.com/thread/1159040790154399745.html
[4]: https://www.mozilla.org/en-US/firefox/new/?redirect_source=firefox-com
[5]: https://duckduckgo.com/
[6]: https://yubnub.org/
[7]: https://www.perfect-privacy.com/en/tests/webrtc-leaktest
[8]: https://addons.mozilla.org/en-US/firefox/addon/facebook-container/?src=search
[9]: https://addons.mozilla.org/en-US/firefox/addon/ghostery/?src=search
[10]: https://addons.mozilla.org/en-US/firefox/addon/privacy-badger17/
[11]: https://www.eff.org/
[12]: https://cliqz.com/en/
[13]: https://addons.mozilla.org/en-US/firefox/addon/ublock-origin/?src=search
[14]: https://www.aftonbladet.se/
[15]: https://addons.mozilla.org/en-US/firefox/addon/https-everywhere/
[16]: https://en.wikipedia.org/wiki/IP_address_spoofing
[17]: https://nordvpn.com/
[18]: https://addons.mozilla.org/en-US/firefox/addon/nordvpn-proxy-extension/
[19]: https://nordvpn.com/download/
[20]: https://imakethingswork.com/privacy/are-you-leaking-personal-details
[21]: https://addons.mozilla.org/en-US/firefox/addon/burner-emails/
[22]: https://addons.mozilla.org/en-US/firefox/addon/cookie-quick-manager/
[23]: https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/
[24]: https://addons.mozilla.org/en-US/firefox/addon/i-dont-care-about-cookies/
[25]: https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/
[26]: https://addons.mozilla.org/en-US/firefox/addon/stylish/
[27]: https://userstyles.org/styles/136318/clear-dark-facebook-by-book777
[28]: https://forum.audiob.us/
[29]: https://addons.mozilla.org/en-US/firefox/addon/lastpass-password-manager/
[30]: https://www.fbpurity.com/