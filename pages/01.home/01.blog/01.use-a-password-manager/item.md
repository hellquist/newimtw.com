---
title: 'Use a password manager'
media_order: 'a_c4u1usfq.jpg,lastpass.png'
date: '2017-04-21 21:25'
taxonomy:
    tag:
        - security
        - passwords
        - lastpass
hide_git_sync_repo_link: false
body_classes: header-dark
visible: false
routes:
    aliases:
        - /security/use-a-password-manager
hero_classes: 'text-light parallax overlay-dark-gradient'
hero_image: a_c4u1usfq.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: website
---

Recently a friend of mine had his iCloud account hacked. If you are unsure of what the implications of that could be I urge you to go and have a read of [this article](https://www.wired.com/2012/08/apple-amazon-mat-honan-hacking/), written by [Mat Honan](https://twitter.com/mat).

===

It should scare you into realising having your account(s) hacked is a bad thing, and that using a password manager is a good thing. Go on, read it, then come back.

Back? Right, let’s continue.

Hacks like these are usually carried out by a combination of digital know-how, combined with “[social hacking](https://en.wikipedia.org/wiki/Social_hacking)“, where the hacker can access quite a lot of your digital life if they just have a couple of details to go on. If your view of a “hacker” is the “movie version” of someone who sits in front of lots of monitors where cryptic data are floating past, think again. A lot of “hacking” happens over the phone, with people claiming to be someone they are not in an attempt to get more details about their target from the company/person they are talking to.

You get the picture: being hacked in one place can lead to the hacker acquiring more details about you, which can lead to being hacked in many more places, and that is obviously a bad thing for you.

Now, you can avoid this pretty much completely if you do two things:

  1. Enable [Two Factor Authentication](https://en.wikipedia.org/wiki/Multi-factor_authentication) whenever you are given the option to do so, on all your services you use (Facebook, Gmail, Dropbox etc)
  2. Ideally use a password manager, but most definitely don’t use “weak” passwords that you repeat on all the services you do use

The idea is simple: if a hacker “only” has your username and your password, it should still not be enough for that hacker to be able to login as you. They will also need something else, preferably a physical object you carry with you. Codes and/or verifications from another device that you own can also be used.

I shall therefore give a few helpful pointers and links over a couple of articles on how normal people, which is the majority of us, can increase the security on your various online accounts for most of your services.

I will do this without going into detail on how hackers actually hack, as that will change over time in any case, but the fact that they will try probably (most likely) will remain. You can still prepare yourself and increase the security level already now, for your accounts, which will make it a lot harder if someone tried to hack you.

## My chosen password manager – LastPass

Let’s start with bad passwords.

Using a password manager, or as I do, a cloud based password management service like [LastPass](https://www.lastpass.com/) to store all your passwords might sound counterintuitive to security, as you are placing all your eggs in one basket. The thing is, a good password manager will have tools to help you ensure you create unique and complex passwords for every service you sign up for. You only need to memorise the password for your password manager (your “last” and only password you need, hence the name of the service), and the password manager will remember all your other passwords. Good services like LastPass will even help you fill in the fields for username and password when you want to log in to your services, like Facebook, Gmail etc.

They will also have a completely encrypted database of your passwords, meaning not even the staff at the company hosting your passwords can retrieve them without you. To them it is just an encrypted storage “blob”, one of many.

The service I use, [LastPass](https://www.lastpass.com/), also have good integration with all popular web browsers as well as having apps for mobile devices. In reality this means that even though I run computers of all flavours (Mac, Linux, Windows) and various mobile devices (iPhone/iPad and Android), I can still access that exact same password vault wherever I am, and whenever I need to.

[![Link to LastPass password manager web site](lastpass.png)](https://www.lastpass.com/)

Further to that, good password managers also have integrations with various Two Factor Authentication methods which, as we learnt above, means that not even with your username/password combination alone can someone access your password “vault”.

In my case I have opted to use two separate solutions for Two Factor Authentication, one that is hardware and another that is an app for mobile devices (as I, like most, almost always carry them with me): Yubikey and Google Authenticator. I shall delve deeper into what those are in the posts to follow, as well as look at a few settings you can do in the services you quite likely already use, to increase the security and privacy level.