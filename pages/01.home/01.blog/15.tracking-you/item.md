---
title: 'Tracking you'
media_order: '5163.jpg,email.jpg,europe.jpg,manage_history.jpg,taiwan_trip.jpg,your_activity.jpg'
date: '2019-04-15 21:29'
taxonomy:
    tag:
        - privacy
        - google
        - tracking
hide_git_sync_repo_link: false
visible: false
routes:
    aliases:
        - /privacy/tracking-you
hero_classes: 'text-light parallax overlay-dark-gradient'
hero_image: 5163.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

Well timed for my latest series of privacy related things I received an e-mail the other day, from Google. They wanted to let me know that they know exactly where I am and where I have been, regardless of if I'm using their Chrome browser or not. Apparently I have settings and apps in my iPhone (which obviously is made by Apple, not Google) that automatically report this stuff back to Google.

===

![Google map showing my locations when travelling in Taiwan](taiwan_trip.jpg?resize=720,400&classes=caption,caption-right,figure-right "Overview map from my Taiwan trip")

The e-mail shows a summary over my last months movements and included my work trip to Helsinki. More importantly, it also had a link in it saying "Explore your timeline", which I had to click. Oh yes, if there was *any* doubt they knew where I had been those doubts were swept away now. With extreme precision could I back-track my recent journey to Taiwan, down to street level. I could also see both our local Helsinki office as well as the hotel I stayed at when I was in Helsinki.

![Screenshot of Google site regarding your location](manage_history.jpg?resize=400,200&classes=caption,caption-right,figure-right "After a few clicks this appear")

As I was clicking around a box appeared after a little while, with a button saying "Manage location history". The box also had an informative text saying *"Your location is reported by your mobile device and only you can see it."*. Um yeah, ok. And **you** Google. Google obviously also can see it, otherwise they wouldn't be able to show it to me.

Clicking on that "Manage location history" button leads you off to a site that where you can see what app/software/device that is reporting your data back to Google. I would suggest you go there and have a look yourself. Perhaps you are comfortable with Google keeping tabs on your every move. I'm not. Especially not after reading their privacy policy, which pretty much confirms exactly what I've said in previous posts; the data you provide can, and will, be used to be able to serve you more relevant advertising.

[![Screenshot from Googles privacy policy](your_activity.jpg?resize=400,400&classes=caption,caption-right,figure-right "Information about data Google collects from you")][2]

Among other things it says like this on their [Privacy Policy site][1]:

> _We collect information about your location when you use our services, which helps us offer features like driving directions for your weekend getaway or showtimes for movies playing near you._

>_Your location can be determined with varying degrees of accuracy by:_

>* GPS
> * IP address 
> * Sensor data from your device 
> * Information about things near your device, such as Wi-Fi access points, cell towers, and Bluetooth-enabled devices 

A bit further down that same link it says like this:

> _Depending on your settings, we may also show you personalized ads based on your interests. For example, if you search for “mountain bikes,” you may see an ad for sports equipment when you’re browsing a site that shows ads served by Google. You can control what information we use to show you ads by visiting your ad settings._

Sure thing, they **also** say that if you allow them to track your location you are helping them build better products etc, but make no mistake in that it all boils down to advertising and advertising sales. That is what they make money from, so it kind of has to lead there.

If Google knows where you live, where you work, and where your kids go to school etc, it is pretty easy to deduct patterns for if you are on a holiday trip or a business trip for example. If you meet up with someone who is also tracked by Google, which is quite likely, it will not be overly difficult to have a good guess at what you could be talking about. After all Google knows both of your marital statuses, your interests, your family set-up, your jobs etc. They probably also know if you are friends, colleagues or just meeting for business. All of these little differences obviously can, and will, affect what you would be susceptible to when it comes to advertising and purchasing of goods/services. This knowledge is what they sell to advertisers on their platform.

So no, Google is probably not selling your specific name and/or address to their advertisers, but then again, they don't have to. They can still promise a pretty detailed demographic overview of the recipients of their advertising slots they sell.

Now I'm off to turn off a bunch of apps/services I apparently have running.

[1]: https://policies.google.com/privacy
[2]: https://imakethingswork.com/user/pages/01.blog/tracking-you/your_activity.jpg