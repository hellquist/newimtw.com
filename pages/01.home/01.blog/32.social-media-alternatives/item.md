---
title: 'Social Media Alternatives'
maintitle: 'Social Media'
subtitle: '<b>Alternatives</b>'
published: true
date: '2021-02-13 23:00'
taxonomy:
    tag:
        - facebook
        - 'social media'
        - fediverse
        - twitter
hide_git_sync_repo_link: false
body_classes: header-transparent
hero_classes: 'overlay-dark-gradient text-light parallax hero-large title-h1h2'
hero_image: globe.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: globe.png
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'Stop using Twitter/Facebook. There are good alternatives.'
    image: globe.png
metadata:
    description: 'Stop using Twitter/Facebook. There are good alternatives.'
    'og:url': 'https://imakethingswork.com/blog/social-media-alternatives'
    'og:type': article
    'og:title': 'Social Media Alternatives | I Make Things Work'
    'og:description': 'Stop using Twitter/Facebook. There are good alternatives.'
    'og:image': 'https://imakethingswork.com/blog/social-media-alternatives/globe.png'
    'og:image:type': image/png
    'og:image:width': '1200'
    'og:image:height': '687'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Social Media Alternatives | I Make Things Work'
    'twitter:description': 'Stop using Twitter/Facebook. There are good alternatives.'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/social-media-alternatives/globe.png'
    'article:published_time': '2021-02-13T23:00:00+01:00'
    'article:modified_time': '2021-02-14T00:14:16+01:00'
    'article:author': 'I Make Things Work'
summary:
    enabled: '1'
    delimiter: '==='
---

You might have seen me on Facebook/Twitter saying something along the lines of that you/we/all should stop using "[Big Tech][7]" social media services, and look at start using alternative services.

===

There are [lots of reasons][12] for using alternatives to Facebook/Twitter. The reasons might differ from person to person as well. For me the main reasons are [privacy][13] and [leaking private details][15]. Also I wish to truly own my originally posted content (however mundande that content might be), as it is mine.

This also means that I don't like the theoretical and possible threat of me being banned/blocked/moderated from a service also means I will not be able to access my own content that I have posted. By using, and posting, original content on Facebook/Twitter/YouTube, that will always be a risk.

The debate about "free speech" surrounding the blocking of accounts on both Twitter and later Facebook are merely showing the symptoms of the problem with having big centralised services. For what it is worth I don't consider Twitter/Facebook to have "broken" free speech. They are private companies, and they removed elements from their "free" services that caused a lot of trouble. It is well within their rights to do so.

### Ban-hammers & your post content

But as nice as it has been to be spared the raging lunacy of certain Twitter accounts, the legislation that has governed how/what/when/where the services are "responsible" or not, is being looked in to at this very moment. The big services in turn are responding pre-emptively, by making sure that they are covered for any eventuality regarding any changes in the law.

All the big tech social media services are now going through *all* the content shared/communicated on their services. Your content. Your messages. Even your private messages. They do this to ensure *you* are not breaking rules that will put *them* in to trouble.

When the alt-right/fascist/conspiracy chase has calmed down a bit the big services will start looking at any and all content, posted by everyone. In fact, [it has already started][8], and I and some of my friends have been on the wrong end of that too many times. All kinds of content is now being caught in erratic faulty content blocking algorithms, or worse, manually checked by someone who has no clue where to find the latest information.

I can't even fault the big tech services for doing "something". They kind of have to. It is just...I don't want to be a part of it. I don't want them to have, and own, all my data, my thoughts and my conversations. I don't need them to know what I like/dislike only for their benefit to place better ads in front of my eyeballs, or to prioritise showing me posts from one user instead of another, thus building and re-inforcing opinion bubbles in their hope of increased interaction.

Those opinion bubbles are a huge part of the problem in todays society which has been massively divided, and is kept that way thanks to the algorithms of big tech social media services.

### Looking at alternatives

All of that possibly kind of makes sense to you, and you now wonder what alternatives you have? Glad you asked (you did, didn't you?), as that is the first step of breaking free, or at least attempting to regain control over your part/contribution in all of it. Unfortunately here are no quick and easy answers.

Firstly people should probably have a think about *how* and *why* they use social media today, and question themselves if they consider that to *actually* be the target when looking for new/other services. That will help you set the goalposts and make it easier to work out if you are getting closer to your goal (or further away) when you are looking at features and capabilities of alternative services.

In reality most people have probably not given this much thought at all. Yet many people on Facebook are NOT on Twitter (and vice versa) due to some quite specific-to-them reasons.

Therefore, trying to replace Facebook with a Twitter clone might not be the ideal way forward either, and can for sure set people up for disappointment.

#### Examples of goals you can set to look for in other services:

* Not have an "algorithm" for the feed, and the feed should be in chronological order
* No Advertising
* Be able to share longer posts than Twitter allow for today (which is 280 characters)
* Be able to upload photos and to find those photos again (document storage and/or photo album)
* Have interest-focused groups
* Be able to group your friends and to post to a selection of them
* Do I like the look/layout of how posts are presented?

Those are just example goals. You might have others. It is probably wise to also set a priority of importance of those things for yourself. Is any single one of the things on your list an absolute "must-have" or a direct "no-no"?

It can also be noted that the Big Tech services out there indeed are *very* good at what they do. They have spent millions making their services easy to use, and to create a glitch-free experience for their users. That is how they can reel in as many people as possible, which in turn means advertising money.

Most alternative services are often developed and run by happy amateur groups, that mainly stick together due to a shared conviction "they are doing the right thing", but without any type of pay or monetary reward.

In my search for viable alternatives I have signed up to, and used, most of the "alternative" services out there, with the main exceptions of Parler and Gab. Basically I have excluded platforms that are infamous for hosting alt-right/fascist/conspiracy people. That is not to say those people aren't spreading out over all the other services. They do, but I have wished to avoid services where they have stated and claimed/announced meeting points as theirs.

### The Fediverse

Basically my choice has fallen upon using a set of different services within what is commonly referred to as "The Fediverse". You can read [this (external) article where they answer "What is the Fediverse?"][1].

In short: The Fediverse is a collective of  several separate services, each with their own characteristics, that can talk and interact with most other services on The Fediverse.

Each of those services are "[federated][14]". This means there is no central server somewhere that manages everything. Each of the services have their own "instances" (servers) and each instance can be as generic or as specific on whatever topic and/or interest group as the administrator of that instance feels like. There are thousands of "instances", with anything from 1 user to hundreds of thousands of users for that instance. There are more than 4 million people in total on the federated services.

4 million is a far cry smaller than Facebook/Twitter, who count users in billions, but honestly, you probably don't talk to most of the people on either of those services, and I doubt very much that you talk to, or interact with, more than a few hundred *at most*.

This also means that it doesn't really matter where in the Fediverse you join, you can talk/interact with the rest of the users of any Fediverse service, regardless of which service *they* prefer to use, or where *they* are.

**You can *do you*, they can *do themselves*, and you can still interact. Great eh?**

I have tried out several of the different Fediverse services and I shall be doing a proper write-up of my thoughts and learnings later, but in short, for me right now, I'm using [Friendica][2] as a Facebook "replacement" (it isn't covering all bases, but I don't need all bases to be covered, only a few key ones) and I'm using [Akkoma][3] as a Twitter replacement. Also I have started using [Pixelfed][4] as an Instagram replacement. All three services are part of "The Fediverse", so unlike Facebook/Twitter/Instagram/YouTube etc etc, most (almost all) of the services in the Fediverse can see, and talk to, and interact with, the other services, "out of the box" so to say.

It could be noted that I am not using Mastodon, which is the absolutely largest Fediverse service (millions of users), directly myself. I don't have to, as the Fediverse services I *do* use can talk and interact directly with Mastodon and its users anyways. The main reason I'm not using Mastodon is because it requires more of the server it is hosted from, compared to Pleroma.

I host my own instances of both Friendica and Akkoma, mainly because I can and I'm interested in such things. If managing servers is not *your* "thing" I would recommend signing up on open/public instances instead. That was also how I started out exploring The Fediverse.

I recently read a [great new-user summary of the Fediverse][6], which said:

> _"1. What is the fediverse?<br>
mastodon, misskey, pleroma and others are social networks comparable to Facebook in 2010: real human beings sharing mundane things to small audiences of friends, acquaintances or friendly strangers.
Nowadays it seems that the big platforms, especially Youtube and Twitter, have turned into broadcasts where a few big influencers present the talking point of the day to massive audiences who consume those opinions, often without adding anything themselves."_

To me that statement has a lot of truth in it. Also, keywords to me would be "_2010_" and "_real human beings_".

In the Fediverse the users, which indeed are "real human beings", take responsibility, and the control, over the content they post and the content they read, themselves. If you think you'd like that, you should definitely give it a try.

You can find me here:

* [https://pixelfed.social/hellquist][9] (Pixelfed)
* [https://thias.hellqui.st/m][10] (Akkoma)
* [https://friendica.hellquist.eu/profile/mathias][11] (Friendica)




[1]: https://newatlas.com/what-is-the-fediverse/56385/
[2]: https://friendi.ca/
[3]: https://akkoma.social/
[4]: https://pixelfed.social/
[5]: https://joinmastodon.org/
[6]: https://misskey.de/notes/8i2xpqqj0w
[7]: https://en.wikipedia.org/wiki/Big_Tech
[8]: https://unherd.com/2021/02/facebooks-incompetent-censorship/
[9]: https://pixelfed.social/hellquist
[10]: https://moc.d-x-b.com/mathias
[11]: https://friendica.hellquist.eu/profile/mathias
[12]: /blog/watch-the-great-hack
[13]: /blog/tracking-you
[14]: https://en.wikipedia.org/wiki/Fediverse
[15]: /blog/are-you-leaking-personal-details