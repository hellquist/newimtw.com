---
title: 'Tightening up privacy'
maintitle: 'Tightening up <b>privacy</b>'
media_order: '248374.jpg,barbwire.jpg,escalator.png,likes.jpg'
date: '2019-03-20 21:36'
taxonomy:
    tag:
        - privacy
        - facebook
        - services
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/tightening-up-privacy
hero_classes: 'text-light parallax overlay-dark-gradient hero-fullscreen title-h1h2'
hero_image: 248374.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

After [all of this][17], what should one do? Well, I can't answer for what you can or should do, as it all depends on what you use today and what you do within those apps/services.

I shall be starting with defining what I would need fixed, as it always is easier to measure if I am reaching my goal if I actually have a goal.

===

These are some of the things I wish to accomplish:

* Make it harder for apps/services to get hold of my private data/details
* Make it harder for apps/services to connect my accounts with other services
* Don't leave as big a trail of my online activities as I've done up until today
* Adjust my privacy settings on most services, to tighten them up
* Shift my usage of some apps/services to other services that are directly/indirectly under my control

![Barbwire](barbwire.jpg)

### Removing negativity spreaders

On top of that I also have a few "soft" goals too. I am for example dead tired of all negative "political experts" who think they have a superior opinion on everything that even is more correct than trivial things such as "facts" and other mumbo-jumbo, though they will do absolutely zero/null/zilch to make anything better. They will just spend all their time complaining about things that aren't really their concern, on Facebook, in their bias bubbles who applauds them and their ideas, regardless of if they are feasible/possible at all.

I'm tired of privileged white middle-aged middle-class who spends an enormous amount of time complaining about what everyone else does. Sick of it. I wish for less of it.

I have recognized that the simplest solution to that equation is to remove myself from it, as that would solve my wish and also wouldn't force anything upon others, who apparently are happy with being their regular miserable and negative selves.

### Facebook

I will not be "quitting" Facebook. I do find it useful for a bunch of things.

I do wish to stay updated with my positive and happy friends around the globe, to see when they get married, have kids etc. Also credit should go where it is deserved, and I have made many many friends via Facebook too that I would never have met otherwise, and those are friendships that I value and don't wish to say goodbye to.

Also I find Facebook to be really useful when it comes to special interest groups, where ideas, inspiration and discussions can be exchanged. In my case this is mainly related to create/compose music, and to computer gaming, but hey, I do value it and the members in the groups I'm actively participating in. It is basically my only usage of Facebook anyways these days, apart from saying "Happy birthday" to people.

I have already uninstalled the iOS Facebook apps on all my devices. It is a memory hog anyways, so that was more of a gain than a loss. I can access [the mobile browser version of Facebook][11] via my mobile web browser(s) when I wish to do so.

I have tightened up my profile to extreme lengths though (it actually already was like that). You can do the same by going to "Settings" in Facebook and have a look at "[Privacy][14]", which basically dictates what other people can see about you, and "[Ads][15]" where you have the settings based on your "advertising profile". You can turn on/off things, pick what types of ads you wish to see (or more importantly, not see) etc.

![Too many likes](likes.jpg)

### No more "like" from me - it isn't personal

I shall resume my "no like" programme I did before, a year or so ago. At the time it did wonders to the quality of my timeline. Basically if you don't "like" (i.e. press the "thumbs up", "heart" or whatever) you are giving away less to Facebook about what you actually think about things. This in turn will upset their formulas a bit as they try to pair you up with the most "relevant" people and content, and they will try harder, and search further out, to see if there is something else you might like. The net result, unless they have changed their algorithm, will be that the quality of my Facebook timeline will improve, and the negativity will be mixed up with new/fresh material. As I keep on not-liking posts from negative people they will start to fade away.

Previously the negative people could post something good now and then, and if I clicked "like" on that I would continue to get posts, most often negative, from them again, as Facebook interpreted my like for the positive post as "I'm really interested in everything this person has to say". If I don't like *anything* that won't happen.

I am also looking into alternatives for social media sharing. Currently [Mastodon][1] is the strongest contender for general sharing. It is decentralised so there is no main hub/server. It is ad-free. It is open-source so no one can sneak in anything weird into it. I could host it myself if I wish to, but currently I think it is enough to use it as a hosted service.

As only a handful of my current Facebook/Twitter friends are on Mastodon I will possibly share things on Mastodon, and then re-share my Mastodon-post on Facebook (which basically will take people to my Mastodon post), and then do less and less of that re-sharing. For those that miss me or my regular posts they can [follow me there][13] instead.

### Chats and conversations

When it comes to [Messenger][8] I just have to give in and admit there is no way I can make people that I currently have in groups/discussions move to another service. Otherwise there actually are alternatives, such as [Slack][2], [Discord][3], [Line][4], [Telegram][5] and [Signal][6]. The two latter, Telegram and Signal, pride themselves on their privacy and encryption, but as with all communication services it is only useful if I'm not alone in using it. I'm obviously trying to avoid adding WhatsApp to that list, as they are owned by Facebook.

Signal is by the way the service [Edward Snowden][12] recommends for ultimate encryption and privacy. Telegram is the service that [gained 3 million new users during Facebooks latest take-out][16].

Slack is perhaps more for work groups, and it is being critisized quite a bit lately as it is turning into a time hog if you have too many groups in there. Discord is much the same as Slack but with the added functionality of voice chats as well, and it used quite a bit by gamers but it has taken on with other interest groups as well because of its text chat capabilities. Line is huge in Asia in general, and Taiwan in particular, and thus is what we are using for chats with friends over there.

![Photo sharing](escalator.png?resize=800,600&classes=caption,figure "Photo sharing")

### Photo sharing

Like with Messenger it will be very difficult to not use [Instagram][9], which is another Facebook service these days. I have to admit I have cut down quite a bit on my sharing on Instagram already (bar the amazing Taiwan trip I made in December). The main reason for that is to preserve the privacy of my kids. I basically figured my kids might not appreciate every minute of their life being documented and uploaded to Internet of when they were kids, when they become adults.

These days I ask my kids before I upload/share any photos of them, if they think it is alright. I don't think they understand the implications of it all just yet, but I do think it is important to teach them their online presence should be on their terms.

I already do use [Flickr][10], and Mastodon has quite nice photo sharing capabilities too, so I think I can keep myself to only doing occasional posting on Instagram in any case, and for the rest just be a "consumer" of others photos, to keep myself updated on what my friends are up to.

Next up will be Google services like Chrome, Gmail etc...

[1]: https://mastodon.social/
[2]: https://slack.com/
[3]: https://discordapp.com/
[4]: https://line.me/en/
[5]: https://telegram.org/
[6]: https://www.signal.org/
[7]: https://www.whatsapp.com/
[8]: https://www.messenger.com/
[9]: https://www.instagram.com/
[10]: https://www.flickr.com/
[11]: https://m.facebook.com/
[12]: https://en.wikipedia.org/wiki/Edward_Snowden
[13]: https://mastodon.social/@hellquist
[14]: https://www.facebook.com/settings?tab=privacy
[15]: https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen
[16]: https://www.theverge.com/2019/3/14/18265231/telegram-new-users-facebook-outage-3-million
[17]: /privacy/putting-the-giants-on-a-diet