---
title: 'Is Climate Change a hoax?'
maintitle: 'Is <b>Climate Change</b>'
subtitle: '<b>a hoax</b>'
media_order: 99853.jpg
date: '2018-10-24 23:14'
taxonomy:
    tag:
        - climate
        - facts
        - argue
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /climate/is-climate-change-a-hoax
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: 99853.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

In my day job I often have to solve things, most often by listening to two separate parties who are at different ends of a conflict and then I/we decide on a path to take us forward. This means I have experience in figuring out **why** *this side claims this, that and the other*, but also why the **other** side claim things from their perspective.

===

Often one side isn't entirely correct. Often the actual solution is somewhere in between the two different sides. Sometimes the solution is to just pick a side and go with their proposal because it makes the most sense in the larger landscape of surrounding problems/stake-holders. This means I'm used to building logical paths, to understand underlying causes. "This" happens because of "that", which in turn is affecting "that thing, over there". Pretty clear. Pretty logical in most cases. One might not agree with all of the possible logical paths, but still, I can see them and understand how they can be important to someone else.

If I now zoom out from work and instead look at something that *apparently* is debatable, like Climate Change, I clearly see the reasons behind one side of the argument. It is a side supported by thousands of scientists, that each get paid by various independent sources. They have concluded the current Climate Change is caused by humans. Many of those scientists are in a global organisation called [IPCC](http://www.ipcc.ch/). As you might have seen lately in [various news sources](https://www.bbc.com/news/science-environment-45784892) IPCC in turn has suggested routes, or actions, mankind should take to prevent further damage to the planet we all live on. That is one side of the Climate Change "debate" that I see these days.

There is also [another side](https://en.wikipedia.org/wiki/List_of_scientists_who_disagree_with_the_scientific_consensus_on_global_warming). That other side claim that many of the scientists in general, and IPCC in particular, are wrong. There doesn't seem to be much argument around the fact that the well documented and well researched current Climate Change is caused by humans or not, even the opponents of IPCC mostly agrees on that. Where they differ in opinion is what it can possibly lead to, and thus, what actions, if any, that need to be taken, today. Climate Change "alarms", they say, is all a hoax, at least the part regarding what we need to do about it.

The above description was on purpose overly simplified and, as stated, zoomed out. I'm sure there are more layers to the onion here, but in general I think the above information should be enough to be able to draw some basic initial conclusions on *"why does this side say this?"* and *"why does that side claim something else?"*. In either case, who stands to benefit of the actions (or non-actions) of either option, because there is usually someone who is intended to benefit when people argue intensely, otherwise there wouldn't be anything to argue about.

In all conflicts and arguments I normally try to give both views (or opponents) a fair chance to explain **why** they claim something. Which brings me to my dilemma now: The side of the sceptics/deniers. I don't see it. I just don't see it. I need help by someone, or many, from the "climate change sceptics" to explain to me not only **how** it can be a hoax, but more importantly:

### Why?
Pretty simple, huh? **Why** is there a hoax?

* What are the reasons for the hoax?
* Who stands to benefit from people caring more about the environment?
  * Is that a problem?
  * Is it a bad thing?
* In what way does whoever orchestrating this very elaborate hoax stand to gain from the hoax?
  * Who stands to benefit if we do something?
  * Who stands to benefit if we do nothing?
* What are the risks if we try to implement solutions to put a brake on man-made global warming?
  * What are the benefits of not doing it?
* What hidden evil global unified goal will be reached by taking actions against (global) Climate Change?

**I just don't see it.**

I clearly need some help on this. If you read this and you are a Climate Change sceptic, or even denier, feel free to contact me and in a clear (preferably non-insulting) manner explain to me what I'm missing. I won't even reveal your name, to **anyone**, I promise. Even if I quote you I will do so anonymously (unless you ask me to include your name). However, if you reply in such a manner that I can't identify you myself, I might leave your signature in, as it won't matter much in any case. 

-"Yes". I know there are "alarmists" (on both sides), most often headed by an eager press that wishes to get headline news. That is not what I'm talking about. I'm not buying their stories blindly either.

Do **not** bother with sending me messages like *"if you can't see it you will never understand"* or *"it's out there if you know where to look"*. I've had both, and more, when I've asked regarding other conspiracies/hoaxes (hi flat earthers) in other forums previously, and I have truly tried to search for the answer to "why?" in this, but either I'm looking in the wrong place(s) or it is right there in front of me and I still don't see it. I'm asking for real, proper links, preferably with multiple scientifically supported evidence, that proves your standpoint is correct, not sensationalist news headlines or opinion pieces. Please give me your knowledge (and not just biased un-proven theories).

### To emphasize: I'm not after arguments to prove or dis-prove Climate Change. I'm looking for the answer to the **reasons** why Climate Change would be a hoax.

If I understand the root cause of either the hoax, or the reverse, I will also be able to decide what I think seems reasonable, for me.

As a side-note: things like *"IPCC is only looking at causes for man-made Climate Change"* you don't need to bother me with either, unless it leads to some better understanding of where you want your argument to go (which ideally answers the questions above). Of course IPCC does that. To me that is like complaining about forensic scientists only finding proof, or lack thereof, regarding crimes, or that doctors only solve things related to illness or medical conditions. That is what they do. That is what they are paid to do. Is that a problem?

Contact me, either via the form on this site (anonymous, though it has a spam filter), Facebook, Twitter, Mastodon, Instagram or wherever you are active, it is quite likely I have an account there too. I repeat, I will not publish or reveal your identity in any way (unless you tell me to). I read English and Swedish. Anything else sadly have to be translated into either English or Swedish.

Thank you.