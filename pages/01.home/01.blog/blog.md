---
title: Blog
media_order: 'site_cover.png,autumn_field.jpg,rocky_coast.jpg,mountain.jpg'
hide_git_sync_repo_link: false
body_classes: 'header-dark header-transparent'
child_type: item
aura:
    pagetype: website
    description: 'A blog written by Mathias Hellquist, covering various topics such as privacy, security, innovation, opinions and various world observations.'
    metadata:
        'twitter:image': 'https://imakethingswork.com/blog/site_cover.png'
hero_classes: 'text-light title-h1h2 overlay-dark-gradient parallax hero-large'
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: '0'
feed:
    limit: 10
    description: 'I MakeThings Work - Sane shit, different mane'
content:
    items:
        - '@self.children'
    limit: 10
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
bricklayer_layout: true
display_post_summary:
    enabled: false
metadata:
    description: 'A blog written by Mathias Hellquist, covering various topics such as privacy, security, innovation, opinions and various world observations.'
    'og:url': 'https://imakethingswork.com/home/blog'
    'og:type': website
    'og:title': 'Blog | I Make Things Work'
    'og:description': 'A blog written by Mathias Hellquist, covering various topics such as privacy, security, innovation, opinions and various world observations.'
    'og:image': 'https://imakethingswork.com/home/blog/site_cover.png'
    'og:image:type': image/png
    'og:image:width': '1280'
    'og:image:height': '817'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Blog | I Make Things Work'
    'twitter:description': 'A blog written by Mathias Hellquist, covering various topics such as privacy, security, innovation, opinions and various world observations.'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/site_cover.png'
    'article:published_time': '2020-11-09T23:42:39+01:00'
    'article:modified_time': '2020-11-09T23:42:39+01:00'
    'article:author': 'I Make Things Work'
sitemap:
    changefreq: monthly
modular_content:
    items: '@self.modular'
    order:
        by: folder
        dir: dsc
pagination: true
---

# I Make **Things** Work
## Sane shit, different mane