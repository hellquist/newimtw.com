---
title: 'You should install Signal'
media_order: 248703.jpg
date: '2019-08-28 20:25'
taxonomy:
    tag:
        - privacy
        - facebook
        - telegram
        - messenger
        - signal
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/you-should-install-signal
aura:
    pagetype: article
    image: 248703.jpg
hero_classes: 'text-light overlay-dark-gradient parallax title-h1h2'
hero_image: 248703.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
maintitle: 'You should <b>install Signal</b>'
metadata:
    'og:url': 'https://imakethingswork.com/blog/you-should-install-signal'
    'og:type': article
    'og:title': 'You should install Signal | I Make Things Work'
    'og:image': 'https://imakethingswork.com/blog/you-should-install-signal/248703.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '853'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'You should install Signal | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/you-should-install-signal/248703.jpg'
    'article:published_time': '2019-08-28T20:25:00+02:00'
    'article:modified_time': '2020-05-05T09:23:29+02:00'
    'article:author': 'I Make Things Work'
---

Messaging. Security. Privacy. Three words that rarely can be combined these days. Unless, of course, your chosen messaging app is [Signal][1] or [Telegram][2]. However, for the sake of simplicity, I decided on one messaging app to rule them all. I chose Signal. This is why...

===

Every time you and your friends use [Facebook Messenger][3] you are topping up a pool of keywords assigned to you by [Facebook][4] with even more keywords. Those are keywords that help Facebook to do some very clever placement of advertisements and "messages" to you and your friends. Those messages are personally tailored, for you, to make you purchase as much as possible. Those messages are also known to have [been used in experiments on you][11], to control your feelings (and thereby thoughts). Yes, you dear reader.

This is also a part of the overall formula to why Facebook can be perceived to be eerily accurate on placing ads they "shouldn't even know you had an interest in". Well, they do. If you continue to feed the beast with lots of things it likes, the beast will grow bigger, not smaller. What it likes is your words and conversations, your interactions, your likes and other reactions. It can also work out that if you like (or interact a lot with) a specific person, and _that_ person likes a lot of specific things, it is probably not useless to target you based on your friends preferences too. If nothing else you will have seen something they know your friend is interested in.

Basically we have fed Facebook so much information, in so many channels, that they now can select what you should see and thereby feel, via formula. Have you been feeling sad lately? Angry? Happy? Have you been hanging on social media in general and Facebook in particular? Just saying...as long as you keep feeding Facebook and/or other social media with keywords that they can assign to you, the more likely you are to be in their hands when it comes to decision making.

That is the price of _"free"_, which should probably be considered _"free - unless your privacy counts..."_, which it does to me these days. It is time to break the chains.

### Enter Signal

Signal is, unlike Facebook, a not-for-profit company. They have created a chat/message app that has all the functionality of the popular services. You can obviously send direct text messages. You can also send pictures/photos, videos, files, voice messages, "disappearing messages" and you can share other documents, without the fees for SMS/MMS. Of course if you use data packages on a mobile connection it _will_ use data (as it is impossible to send for example a video without moving data somehow, so this is true for Facebook Messenger too).

Further to that you can also use Signal for making calls. Again, distances doesn't count here as we are talking about moving (voice-) data over [IP-networks][5], much like Skype (which, by the way, have hired Signal to help them out, which is a good thing), not broadcasting audio to the nearest telephone mast/satellite, but also there are no roaming fees and similar.

The biggest difference however is that it is **all** [encrypted][6]. Everything. Totally encrypted. End-to-end. This means that not even Signal, the company, can intercept your messages/calls and decode them. They simply can't. No one can. This in turn means whatever you say, send or share, it will not affect your advertising formula somewhere. It will not land you in jail. It will not be used by the service provider in weird experiments to make you feel sad. No one can snoop in on your messages/calls. The only one(s) that can get to your messages are the ones you choose to message/talk with.

Do you need more convincing? Do you remember [Edward Snowden][7]? The US "whistleblower" who worked for CIA/NSA and revealed some nasty secrets and had to get political asylum in Russia? I think you can't point to another person alive who is more cautious of their digital footprint. Edward Snowden uses Signal. I'm thinking I'm not about to do any whistleblowing, I mainly just don't want to feel watched by big services any more. I'm also thinking if Signal is good enough for Edward Snowden it is quite likely good enough for me.

### ...and groups of course

Did I mention you can also create groups in Signal? I'm in chat groups on Facebook for all kinds of things with my friends, but I would these days much prefer if you/they all just downloaded Signal and we re-created the groups we already have. The difference would not be noticed as far as chat functionality goes.

It works great and is mainly replacing Facebook Messenger when I message my wife, and it replaces [Snapchat][8], [Whatsapp][9], [Tiktok][10] and a few others when I need to get hold of my kids. It does this whilst keeping the conversations completely encrypted, even in our family group, where we (well I...) have validated our accounts towards each other.

Signal even has a desktop app for your computer. Given that all conversations are encrypted it does take one extra step to verify each device that you own truly is yours and belong to your account, but it is well worth it in the end, and it is really quite convenient to be able to type messages with a real keyboard on a real screen when I'm working, and still be able to have the same access to it all via my mobile device(s).

I'm also slowly convincing more and more of my friends to download Signal and start using it. Signal is especially appreciated by my friends who are _not_ on Facebook (yeah, I have quite a few of those friends too, believe it or not). They don't feel they have to crawl into the giant beast just to send messages any longer. If you haven't already, I think you should give it a go. Add me if you like. :)

<p class="alert alert-error"><b>Edit:</b> It has been brought to my attention that it might be difficult to add me, or someone/anyone, on Signal that you don't already know the mobile phone number of. I'm not overly eager to publish my phone number here, but if you have me on Facebook already I have temporarily made it visible for my friends. If you <i>don't</i> have me on Facebook already you can instead <script>
<!--
var s="=b!isfg>#nbjmup;nbuijbtAifmmrvjtu/fv#?tfoe!nf!bo!fnbjm=0b?";
m=""; for (i=0; i<s.length; i++) m+=String.fromCharCode(s.charCodeAt(i)-1); document.write(m);
//-->
</script>
<noscript>
&#60&#97&#32&#104&#114&#101&#102&#61&#34&#109&#97&#105&#108&#116&#111&#58&#109&#97&#116&#104&#105&#97&#115&#64&#104&#101&#108&#108&#113&#117&#105&#115&#116&#46&#101&#117&#34&#62&#115&#101&#110&#100&#32&#109&#101&#32&#97&#110&#32&#101&#109&#97&#105&#108&#60&#47&#97&#62&#13&#10&#13&#10&#13&#10&#13&#10
</noscript>
 with your phone number and I'll add you first, send you a message and you can add me after that.<br>Alternatively you could contact me in a DM on Twitter, or via Mastodon (links on top of the page).<br><br>Yes, it is ironic that I share my phone number (albeit temporarily) on Facebook, but that is how it is these days: most people <b>do</b> have Facebook, so they are kind of my target audience for swapping to Signal in the first instance.
</p>

### Why pick Signal over Telegram?

For me it came down to a couple of things when it came to deciding between the two, and for a full disclaimer: I obviously have both. I have a few friends who prefer Telegram and aren't on other services. For the main reason of being able to "set up one app, only" with the family I had to pick just one, and after a brief investigation my choice fell on Signal in the comparison with Telegram, for the following reasons:

* Most of my security concious mates said so. :)
* Telegram isn't _fully_ open-source and as such is isn't totally possible to guarantee what happens in the background. Having said that, they too have good reputation and are considered one out of two main secure apps, where Signal is the other.
* I found the default configuration of Signal to be simpler/easier/quicker.
* I like the "less is more" approach Signal has to its design.
* As stated above, if Signal is secure enough for Snowden it is most likely both good and secure enough for me and my needs.

So, there you have it. Go download Signal. Message/chat away. Add me, especially if we know each other already. Whatever you will say to me will not be possible to intercept for anyone else.

[1]: https://signal.org/
[2]: https://telegram.org/
[3]: https://www.messenger.com/
[4]: https://www.facebook.com/
[5]: https://en.wikipedia.org/wiki/Internet_protocol_suite
[6]: https://en.wikipedia.org/wiki/Encryption
[7]: https://en.wikipedia.org/wiki/Edward_Snowden
[8]: https://www.snapchat.com/l/en-gb/
[9]: https://www.whatsapp.com/
[10]: https://www.tiktok.com/en/
[11]: https://slate.com/technology/2014/06/facebook-unethical-experiment-it-made-news-feeds-happier-or-sadder-to-manipulate-peoples-emotions.html