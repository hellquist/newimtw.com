---
title: 'Using Google Authenticator'
media_order: 'first.jpg,ga.jpeg'
date: '2017-04-28 21:11'
taxonomy:
    tag:
        - 2fa
        - security
        - google
hide_git_sync_repo_link: false
body_classes: 'photo header-dark header-transparent'
visible: false
routes:
    aliases:
        - /security/using-google-authenticator
aura:
    pagetype: article
speakerdeck_title: ''
display_speakerdeck_title: '0'
speakerdeck_id: ''
speakerdeck_slide: ''
hero_classes: 'text-light parallax overlay-dark-gradient'
hero_image: first.jpg
header_image_alt_text: ''
header_image_credit: ''
header_image_creditlink: ''
blog_url: /blog
show_sidebar: '1'
show_breadcrumbs: '1'
show_pagination: '1'
post_icon: ''
hide_from_post_list: '0'
textsize:
    scale: ''
    modifier: '1'
style:
    header-font-family: ''
    header-color: ''
    block-font-family: ''
    block-color: ''
    background-color: ''
    background-image: ''
    background-size: ''
    background-repeat: ''
    justify-content: ''
    align-items: ''
hide_page_title: '0'
content:
    items: '@self.modular'
    order:
        by: ''
        dir: ''
class: ''
footer: ''
hide_title: '0'
display_custom_links: '0'
presentation:
    content: ''
    parser: ''
    styles: ''
feed:
    limit: '10'
    description: ''
metadata:
    'og:url': 'https://imakethingswork.com/blog/using-google-authenticator'
    'og:type': article
    'og:title': 'Using Google Authenticator | I Make Things Work'
    'og:image': 'https://imakethingswork.com/blog/using-google-authenticator/first.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1600'
    'og:image:height': '1067'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Using Google Authenticator | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/using-google-authenticator/first.jpg'
    'article:published_time': '2017-04-28T21:11:00+02:00'
    'article:modified_time': '2020-11-08T15:53:45+01:00'
    'article:author': 'I Make Things Work'
---

In my first article regarding increasing online security for normal people, I [covered LastPass](../use-a-password-manager/). Then in my second article I covered how to [use a hardware dongle](../using-yubikey/) for Two Factor Authentication (2FA). I also use Google Authenticator for 2FA, which is an app that generates time sensitive security codes.

===
<div class="twentytwenty-container">
 <!-- The before image is first -->
 <img src="https://imakethingswork.com/user/pages/01.blog/03.using-google-authenticator/_DSC8755-copy.jpg" />
 <!-- The after image is last -->
 <img src="https://imakethingswork.com/user/pages/01.blog/03.using-google-authenticator/_DSC8755-Edit-copy.jpg" />
</div>
<p class="figcaption">You can find this <a href="https://flic.kr/p/2jZiQqg"> image on Flickr</a></p>


!! **Update 2019-08-19:** Even though the functionality of Google Authenticator still is as described, and as such good, I have since writing this article encountered another software called [Authy](https://authy.com/), which does very much the same thing. Authy however has a bunch of more benefits and today is the app I recommend to use instead of Google Authenticator. It is however better to use Google Authenticator than nothing.

Google Authenticator is a very nice solution indeed, as it can help you generate security codes for any app/site that supports it. One of the main benefits with it is that they have sort of defined a standard that others can join, which many have.

![Google Authenticator on a phone](ga.jpeg?classes=caption,caption-right,figure-right "Google Authenticator on a phone")

For you as a user that means you can actually use the same app for many services, which in turn means you don’t have to install many and various solutions just to get your Two Factor Authentication going. Basically Google Authenticator generates a time limited code for each of your connected services. When the time for the current code is up it generates a new code, which invalidates the previous one. Even though it isn’t rocket science to crack 6 numbers, doing it within the correct minute, whilst it is valid and before it becomes invalid is almost impossible.

Only with the combination of your username, your password and the correct (for that specific time) timecode will you be able to login. As the Google Authenticator app itself lives on your smartphone, which you hopefully are the only one who has access to, it is a good and secure system, and in my humble opinion it is a lot more convenient that those RSA dongles some work places insist on using, which basically does the same thing, but for one specific use only.

If you would like to know how to set it all up I guess a [link to Googles instructions](https://support.google.com/accounts/answer/1066447?hl=en) on the topic is in order. There they walk through how you can set it up on your device.

### Complexity of security

For those of you who have never heard about tools like these previously, and still are suspiciously wondering how they work in practice, you are right: it canactually turn into a small little dance when logging in to a service from a new location, at least the first time on an unknown device/system for your service. For example LastPass, will require you to use your Yubikey, only to retrieve your really complex Gmail password, which then in turn will require your timed code from Google Authenticator. You’ll be doing the Two Factor Authentication twice. Those cases are rare though and still…

Consider the alternative. Have you ever e-mailed a password (for anything, to anyone)? Account details? Do you have things in your inbox/service you’d rather not anyone else could see? Do you have work stuff in your private e-mail inbox? Most people have done one, or many, of those things, even though they really shouldn’t have. If you have, and you are being hacked, the hackers know full and well what to search for in your inbox to find the juicy stuff, and it will only take a couple of seconds and your life can have become a whole lot harder. It will have become a whole lot easier for the hackers to continue on to your next service though…if they get in, in the first place.

Better to be safe than sorry.