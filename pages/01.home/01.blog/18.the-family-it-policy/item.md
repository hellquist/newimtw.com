---
title: 'The family IT policy'
maintitle: 'The <b>family</b> IT policy'
media_order: '183707.jpg,calendar.png,Inkorg _ SOGo_b.png'
date: '2019-08-27 20:54'
taxonomy:
    tag:
        - email
        - calendar
        - groupware
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/the-family-it-policy
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: 183707.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

Right, so I took a couple of days off my summer vacation to finish off what I had started when it comes to sorting out self-hosted email & calendars for the entire family. This is what I've done.

===

Basically I wanted to achieve the following:

* Have email that isn't hosted (read stored and "scanned") by any of the big players such as Google, Microsoft, Apple etc.
* Have spam/virus checking of the emails already on the server (i.e. before it reaches any device).
* Ensure that our sent emails from my domain(s) don't classify as spam with Google/Microsoft et al.
* That the entire family would be able to check email on desktops, laptops as well as mobile devices. This meant I have to take height for Windows, macOS, iOS and Android.
* Ensure the entire family could sync calendars on their devices too.
* Establish a shared calendar that all of us could see and input content for.
* Sync contacts/addressbook for the family too, especially the kids.
* Sync tasks and to-do's in the family.
* All of the above should ideally be possible to manage with built-in apps/software and/or free software in whatever device that person prefers

I was also looking for a good and secure way to send messages within the family, as I was quite fed up with the confusion on what app the kids were using that day (WhatsApp, Snapchat, TikTok etc).

Further to that I was also looking at doing yet another overhaul of how I should sort out my workflow in getting things done, both for work related things as well as private projects, that would work on all platforms. I usually tend to keep work/private completely separated, but that doesn't mean I want multiple workflows to keep track of. I used to have this sorted quite well when I only had to cater for using a Mac, but that is no longer the case, and so it has to work on PC's too.

But first things first:

### 1. Server with Ubuntu on Digital Ocean

I have my web hosting in "the cloud" and mainly on [Digital Ocean][1]. Therefore I decided to try it all out with another server on Digital Ocean. Any virtual server by any of the cloud providers would have been alright though, the main thing is that I trust Digital Ocean to not dig through my server for content, and even if they did they wouldn't be able to sell that data without causing a major scandal. The "where to host it" is otherwise the weakest point in the whole set-up, where the safest option would be owning a physical server hosted somewhere in a building you can control yourself, but that is taking it a bit too far for my needs.

The server I'm using for this costs me $20/month, which is alright. It has 4GB ram, a decently sized harddrive for the system and a bunch of extra storage mounted as well for storing the actual email/calendar database. It also has generous back-up options enabled.

I opted for [Ubuntu][2] (server, obviously) simply because I know it and how to configure it to be secure. I could have picked some other Linux distribution too, but that would have made it possible for me to make simple mistakes in the configuration of it all as I then would have to learn it first. Better to go with what I know, and that I know works.

I installed it with all the regular stuff such as firewalls, [Fail2Ban][3], custom ports, only login via certificate and no passwords etc.

### 2. iRedmail & SOGo Groupware

![Screenshot of SOGo inbox](Inkorg%20_%20SOGo_b.png?resize=600,400&classes=caption,caption-right,figure-right "The SOGo Inbox, via web interface")

After that it was time to sort out the email/calendar functionality. I had been looking around quite a bit, with a vague idea of my final plan, and had come to the conclusion [iRedmail][4] would suit my needs, especially given the fact that you can use [SOGo Groupware][5] in conjunction with iRedmail. SOGo is a key component in the entire set-up.

Reading some of the guides I learnt that I had to learn quite a few things when it comes to _how_ mail **actually** works, and more specifically, what is needed when it comes to DNS records for a functioning email server. Apart from that (which actually was a pain) the rest of the installation was more about following a step-by-step tutorial on how to install it, [such as this one][6].


### 3. iCal & Carddav

The rest was quite straight forward, and consisted more of sorting out all the devices in the family that needed access to the services. As the wife + kids aren't overly interested in the technicalities of how things work they therefore have a limited attentionspan on how they should configure things. I basically took all their devices and sorted them out, and handed them back with functioning services.

This whole process was also helped by iCal calendars I could import, such as school schedules/events etc, though I learnt my daughters school has better (but uglier) IT services than my sons school (which has a really shiny quite incompetent system).

I also learnt that Samsungs flavor of Android, which is what my wife has on her phone, is nigh-on incomprehensible in its lack of logical locations for adding/tweaking things. I finally got it all to work though.

I configured my laptops/desktops along with mine and the kids iOS devices really simply/quickly though.

Now we all had working calendars, both personal ones and a shared family calendar, along with visibility of everyones schedules. We also had shared contacts from the server, which didn't interfere with existing contacts, where synced contacts only add, not remove, to the existing address book (my kids don't need my work contacts/colleagues for example, and I don't need those of my wife either).

![Screenshot of SOGo Calendar](calendar.png?resize=600,400&classes=caption,caption-right,figure-right "The SOGo Calendar, via web interface")

It looks like this when it all is "on", but normally I switch the others "off" unless I need to see them. I turn them "on" once each morning to get a view of what is happening for this day/week, then "off", so I can focus on my own calendar. More importantly, when viewing the calendars in a calendar app (for example macOS Calendar) the different calendars get different colour coding and it becomes easy to quickly work out who's calendar I'm watching right then. In SOGo's web interface all the calendars are grey, which obviously can confuse things. Then again, the web interface is merely a bonus to me, as I manage mail, calendars and to-do's in separate apps.

### 4. Spark

I installed [Spark][7] on everyones mobile devices. Previously I have found it _too_ simple, basically because I have around 15 email addresses I need to check regularly, and I didn't like how Spark plonked all of that into one single inbox. I have up until recently used [Airmail][8], both on my mobile devices and on my private desktop/laptops. Airmail has unfortunately backstabbed their entire userbase and decided to go subscription only, even for us that have paid for that functionality in the first place, so I was looking for a new alternative anyways for my private email handling.

<p class="alert alert-info">Regarding Airmail: It could be noted I don't object to paying for software, especially good stuff. I probably purchase more software/apps than most. I also value the work of developers in general. I do think however that changing what is included in the agreement after it has been made and paid for isn't good business practice. If someone has bought something for an agreed upon price, changing what is included in that isn't something anyone would normally get away with. Imagine buying a car, paying it off, driving around in it happily for months. Then one morning you find your car without a steering wheel and a note from the car dealer you bought it from originally saying you can borrow the wheel (you have already bought) if you start paying them on a monthly basis. I am thinking that would annoy most people. Anyways...</p>

Also, the kids don't really use email (that is for old people, right?) but they still had email addresses for their iTunes accounts etc, and now they were given a new email address from me. I figured they are too young (12 & 9) to keep track of these things yet. The main thing we were after here was the calendar, but that came attached with an email address, so it would make sense if they also _could_ check those emails. My son is also coming to an age (13 this year) where he signs up for things and has to click confirmation links in emails, which up until now has gone to me.

Enter Spark. In Spark everything ends up in one inbox for all your email addresses. If you choose to reply it will default to the email address you received the message on, and it is quite clear in its overall design. It is simple, but simple enough. It is also advanced enough to be competent.

### Summary

This all means that I have achieved what I set out to get at the top of the page. It also means that everything is syncing with the native apps in my computers/devices, which in turn means that I am not bound to use any particular app/software, but instead can choose whatever I like the most. In theory that also means the different family members could use their own favourite app/software for that functionality.

I was thinking I would include task management and chat/message capabilities in this post, but it is already dragging out on time so I'll create a separate post for that.


[1]: https://www.digitalocean.com/
[2]: https://ubuntu.com/download/server
[3]: https://www.fail2ban.org/wiki/index.php/Main_Page
[4]: https://iredmail.org/
[5]: https://sogo.nu/
[6]: https://kifarunix.com/how-to-setup-iredmail-mail-server-on-ubuntu-18-04-lts/
[7]: https://sparkmailapp.com/
[8]: https://airmailapp.com/