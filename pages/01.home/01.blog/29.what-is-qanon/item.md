---
title: 'What is QAnon?'
media_order: qanon_follower.jpg
published: true
date: '2020-11-13 01:00'
taxonomy:
    tag:
        - qanon
        - conspiracy
        - gamification
    author:
        - 'Mathias Hellquist'
hide_git_sync_repo_link: false
body_classes: header-transparent
aura:
    pagetype: article
    description: 'I came across an interesting article about gamification of the real world, in particular on the topic of QAnon, which I realised I knew nothing about. Here are some links I found.'
    image: qanon_follower.jpg
hero_classes: 'text-light parallax overlay-dark-gradient hero-fullscreen'
hero_image: qanon_follower.jpg
header_image_alt_text: 'QAnon follower in red shirt'
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: qanon_follower.jpg
summary:
    enabled: '1'
    delimiter: '==='
hide_from_post_list: false
feed:
    limit: 10
maintitle: 'What is <b>QAnon?</b>'
subtitle: ''
metadata:
    description: 'I came across an interesting article about gamification of the real world, in particular on the topic of QAnon, which I realised I knew nothing about. Here are some links I found.'
    'og:url': 'https://imakethingswork.com/blog/what-is-qanon'
    'og:type': article
    'og:title': 'What is QAnon? | I Make Things Work'
    'og:description': 'I came across an interesting article about gamification of the real world, in particular on the topic of QAnon, which I realised I knew nothing about. Here are some links I found.'
    'og:image': 'https://imakethingswork.com/blog/what-is-qanon/qanon_follower.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '757'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'What is QAnon? | I Make Things Work'
    'twitter:description': 'I came across an interesting article about gamification of the real world, in particular on the topic of QAnon, which I realised I knew nothing about. Here are some links I found.'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/what-is-qanon/qanon_follower.jpg'
    'article:published_time': '2020-11-13T01:00:00+01:00'
    'article:modified_time': '2020-11-13T16:04:33+01:00'
    'article:author': 'I Make Things Work'
---

I came across an interesting article about gamification of the real world, in particular on the topic of QAnon, which I realised I knew nothing about. Here are some links that I found.

===

The other day I came across an article posted by a friend on Facebook. He normally posts interesting things and the title appealed to me, so I clicked it. The article (which I will link to further down) is a game designers view on the conspiracy group called QAnon.

I finished the article with several "Aha!" moments, not because I have been contemplating QAnon in particular, but that the article and what it describes, is applicable to many other conspiracy theories too. Many of those, it turns out, are also somewhat interlaced/overlapping with QAnon, who doesn't appear to discriminate conspiracy theories in the slightest, but more acts as a collector of them all.

I realised I knew little to nothing about QAnon, though I had wondered what the Q signs/teeshirts stood for when I watched various news broadcasts or read articles covering Trump before the election. Me being me I had to find out more, especially after having read that article.

### But first some background

This might be the time where I should back-pedal a bit for your understanding: I have "friends" on various social media from literally all paths in life. All the skin colours. All the religions. All political views. All the sexual orientations. All the genders, including non-binary and trans-genders.

As my collection of mostly-beautiful people/friends on social media usually comes from one interest path in my life or another, where things like religion, sexual orientation or political views does not matter in the slightest, we most often have solved it by not talking about those things whilst we instead _are_ talking about the topic that brought us together, be it music creation, music listening, work, gaming, photography or books.

Like most I also have friends that have been collected over time. Some are childhood friends. Some are more recent. Some I _knew_ but they might have developed in to people I no longer actually know or socialise with. Some new ones I think I know, but honestly, I only know them from one area of interest and/or work, and I don't have the full picture of their entire life.

That is how social media works, right? If you are like me you have "friends" on social media platforms that often not truly are your friends. Some you've never even met. Some you have met and wish you hadn't, but you keep them anyways on the social media platform.

It should be noted here that I have also made lots of "real" friends on social media, or other online activities including gaming, often that I most likely wouldn't have met otherwise. I've even started companies with such friends, and I've been travelling to weddings where the actual wedding is the first time I met the people in person, even though we've known each other for years, online.

### Unknown side of people

However, after becoming "friends" with some "outside" the interest group that brought us together, I have learnt that I also have friends that believe in various conspiracy theories. This has surprised me, especially when the friend is similar to myself in aspects that relate to however we got to know each other in the first place.

As I am one of those that have been online since "the beginning" (early -90's), and take pride in my ability to find _any_ information online if I focus on finding it, I have therefore investigated quite a few of the conspiracy theories quite closely. To me this has been for two reasons:

1. Telling a conspiracy theorist that they are a conspiracy theorist does nothing good to him/her nor me. If anything it reinforces their belief that they are correct.
2. I'm a curious bastard. I figure the only way to properly be able to argue with, or convince someone, is to understand how they think. Not to agree, but to understand, and to understand how they reason (however flawed that may be).

It should be noted that I have learnt it is literally impossible to convince someone who believes in conspiracy theories that they are wrong though. You can't. Doesn't matter if you attack or "play along". It just isn't possible.

If you have read any of my older posts on this blog you also know that I often talk about finding un-biased news, to ensure you have done your fact check and verified sources (and that I'm extremely concious of security and privacy, but that isn't necessarily related to this post).

### Due diligence

Therefore I have, over the years, browsed and also signed up to various messageboards on a wide array of what most normal people would call dubious topics. This is probably where I should hasten to clarify that none of the topics I've "investigated" is sex related.

I do know more than I wish to know about conspiracy theories regarding Flat Earth, Holocaust-denial, 9/11 theories, moon landings, right-wing extremists etc. I've covered them all, but also need to stress that saying I did "investigate" or "research" in the proper (to me) meaning of the words, would be to glorify what I've done. I've mainly browsed and read and followed trails, mostly on the open "regular" Internet, nothing more.

I've not signed up _as_ myself, mind you, no one does on those places and everyone are anonymous. If someone would use a real name/photo people would assume it is either fake and/or a trap, and/or a spy. Generally, they think most are spies, especially in the groups where they discuss various topics that relate to "the deep state" etc.

On those places you will not get access to all areas either, until you've somehow proven that you are real, active and consistent in your own messages on that same topic.

Anyways, I haven't done much of the above in a while (I would probably have to start over again on most, and I can't be bothered really), and QAnon has cropped up whilst I was looking the other way, so I realised I knew little to nothing about them, which got me curious, especially after reading the mentioned article above.

### Back to QAnon

If you, like me the other day, don't know what QAnon actually is, here's a couple of primers from "regular" news sources: [What is QAnon?][1] (NY Times) and [an article on Washington Post][2] which also includes a nice 5-minute video that summarize it on a zoomed out level. Go read them and watch that video, then come back here. It should be noted that New York Times and Washington Post probably are "the enemy" in the eyes of QAnon.

So the above articles teaches us that there is an actual person (or a group of people) posting things on an [anonymous messageboard][3] and that posts under the pseudonym Q. It all started in October 2017. At first it was apparently "Q Anonymous" and later, just "QAnon". Often the posts are including "code", acronyms or shortforms that you have to be read up on to understand at all. On [qanon.pub][4] there is apparently a collection of posts (I got that link from the video in the article above, it was written on one of the signs of a Q-follower).

I'm not alone in not being clued up, others have been interested too, so instead of typing out stuff myself I shall be linking to it instead.

* Daniel Morrison posted "[The First Q][5]" on August 4. It is a re-post of the first message on what it all is about.
* Rabbit Rabbit posted the article that got me curious (mentioned above) on Sept 30, which is called "[A Game Designer’s Analysis Of QAnon][6]", and is an extremely good/interesting read (IMHO). Also, it covers a lot more than "just" QAnon, so if you are interested in the "how" and the "why" regarding other conspiracy theories you'll probably find it interesting too.
* Daniel Morrison posted "[The Greatest Trick the Devil Ever Pulled][7]" on Sept 11. It digs deeper in to how human minds function in general, and regarding conspiracy theories and QAnon in particular.
* Tom Anderson posted "[A Layperson’s Guide To Understanding QAnon; Or Why Did Everyone Lose Their Minds?][8]" on Sept 1. It is also about the gamification of it all, and what "rules" there might be around "the game of QAnon".

The main one, to me, is still [Rabbit Rabbits post][6], but the reason I didn't give you that link first is because I think you will appreciate that article even more if you have a basic understanding of what QAnon is first. After you've read that article the rest of the articles are more for deepening your knowledge in case you are interested.

Some of the QAnon theories described in that collection of articles are truly mind-blowing. It is hard to believe anyone would actually believe any single one of them, but many believe in the vast majority of the collection of them. It is everything from the "regular" mumbo-jumbo that [George Soros][9] controls literally everything and everyone to occult cannibalistic sex pacts involving famous people.

To me, not being an expert and not sitting on any inside information, this all sounds like the continuation of what Cambridge Analytica did. It wouldn't surprise me in the slightest if a similar company is behind it all. Plant seeds. Seeds of doubt. Seeds of clues. Make people connect dots on a trail they have created. All the dots are "just there", no one knows where they came from, but now they are there, so they must be true, right?

This will make people "solve the riddle", something that most people actually love to do, and something they take pride in having accomplished, especially if others haven't. They feel superior and start defending and/or market it slowly. Grassroot opinion-shaping that leads a life of its own, as long as you feed it nuggets (Q messages) now and then.

I have often wondered why some of my "friends" online say things like "climate-change is a hoax, listen to this educated guy telling you the truth" (just an example) and then you check up on that educated guy and realise he has made his fortune working for the oil/car industry. All his friends are most likely in the oil/car industry. Would he ever propagate for not using oil/cars? Not very likely. Can't they see it? Apparently not.

In the same way they are somehow totally clued in on that Soros, the rich "democrat supporter" from Hungary, is "behind it all" whilst completely ignoring that [Robert Mercer][10], the billionaire that founded both Breitbart and pumped money in to [Cambridge Analytica][15], which in turn managed to overturn democracy in at least 14 countries ([according to the documentary][14]) and leaving a trail of destruction behind them and Mercers money.

Actually, given that Mercer sold Breitbart in Nov 2017, about the same time as QAnon was created, he might have found another way to influence lots of people with a leaning to supporting his ideals and ideology in general, and Trump in particular...(X-Files Theme in the background)



Image credit: [Marc Nozell][12] took the [original photo][11], which I turned in to a "watercolor painting" (in Photoshop). The photo is shared under [Creative Commons - Attribution 2.0][13]

[1]: https://www.nytimes.com/article/what-is-qanon.html
[2]: https://www.washingtonpost.com/technology/2020/11/10/qanon-identity-crisis/
[3]: https://www.4chan.org/
[4]: https://qanon.pub/
[5]: https://medium.com/@daniel.ed.morrison/the-first-q-e2b8f389d533
[6]: https://medium.com/curiouserinstitute/a-game-designers-analysis-of-qanon-580972548be5
[7]: https://medium.com/@daniel.ed.morrison/the-greatest-trick-the-devil-ever-pulled-10caa1cec8b5
[8]: https://medium.com/@toand8715/a-laypersons-guide-to-understanding-qanon-or-why-did-everyone-lose-their-minds-9b5a93468be7
[9]: https://en.wikipedia.org/wiki/George_Soros
[10]: https://en.wikipedia.org/wiki/Robert_Mercer
[11]: https://commons.wikimedia.org/wiki/File:QAnon_in_red_shirt_(48555421111).jpg
[12]: https://www.flickr.com/people/37996583933@N01
[13]: https://creativecommons.org/licenses/by/2.0/deed.en
[14]: /blog/watch-the-great-hack
[15]: https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal
