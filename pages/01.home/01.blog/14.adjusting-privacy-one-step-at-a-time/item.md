---
title: 'Adjusting privacy one step at a time'
media_order: '98385.jpg,homeowner.jpg,mountain.jpg,email.jpg'
date: '2019-03-28 23:32'
taxonomy:
    tag:
        - privacy
        - 2fa
        - email
        - servers
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/adjusting-privacy-one-step-at-a-time
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2 hero-fullscreen'
hero_image: 98385.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
maintitle: 'Adjusting <b>Privacy</b><br>one <b>step</b> at a time'
---

I've been keeping myself rather busy over the last week's evenings with various things as I am wrapping my head around exactly *how* deeply entrenched I have been, and in some cases still am, into some services that devour my personal details. It is eye opening to say the least. Eye watering in some cases.

===

Quite scary too how freely I have shared things, and to what amount. Also, I know that I am more clued up on quite a few of these things compared to other people I know. *They* are quite literally giving away everything, everywhere.

When I put everything on top of each other I can easily say that during my previous years I have entrusted complete strangers and companies with a lot more personal/private information than I would willingly tell pretty much most people except for possibly my closest friends/family. What the companies have in common though is that they are not my friends at all and they do not wish me, in particular, well and secondly that they wish to make as much money as possible. I have made it a lot easier for them.

Swapping out web browsers was simple, and it is also quite fun. Rediscovering some old (browser) "friends" whilst making some new (due to my job tasks in previous jobs I used to have most browsers installed, but not so much these days...until now).

Did you know that [Vivaldi browser][2] have integration with [Philips Hue][3], which I happen to have all over my house? I had no idea. Also in Vivaldi I can set "themes" based on the hour of the day, which means I can keep my dark/night themes for evenings and still use something lighter during the day when it is bright. Nice touch.

![Photo of a snail with a shell](homeowner.jpg)

### Sidetrack - Passwords and 2FA

I have also swapped out [Google Authenticator][4] (which is an app [I wrote about previously][1]) to instead use [Authy][5], which seems to be a lot better IMHO. Google Authenticator is strictly bound to one phone. I have previously gone through the pain of having *that* phone crash so hard that I had to send it for repairs, during which time I couldn't actually log in to any of the regular services I used Google Authenticator on, only to realise that they swapped out the entire phone for a new one. I had to contact every single service that used 2FA via Google Authenticator and explain the situation, and that if I could only please-please-please turn off the 2FA so I could get back into my services, I would solve it differently etc. Painful to say the least.

Authy doesn't have that problem. You can have it on multiple devices, even your desktop. It syncs with your other devices too, and you can make it so you have to log in separately in Authy compared to your device/desktop. If you can log in you will then find that they are all in sync with each other. Very handy. Not that a big compromise on security, though I wouldn't install Authy on **every** device I have, only a few key ones.

I have also taken the opportunity to update some old/weak passwords for various services. In my case I use [LastPass][6] for password storage, and again, I have to use a [Yubikey][7] to even unlock the database for LastPass, so even if you sit with my password database in front of you, it will be of no use to you unless you also have my username, my password and my physical key. Still, I had been sloppy in some instances with repeated passwords, or passwords from services I knew have had breaches but I hadn't bothered updating the passwords as I often had 2FA (two-factor authentication) switched on anyways.

That is now fixed and amended. I have now reached a stage where I couldn't possibly leak the passwords myself; they are quite simply too complex and long. I have no idea what they are, I only know where they are. LastPass keeps track of them for me now.

![Illustration of E-mail bouncing back and forth](email.jpg)

### Setting up e-mail server(s)

But the most time I have spent with taking back control over e-mail, which in my case means no longer having my private domains on a shared hosting, where the e-mail part of the domain often was handled by Google Apps, or [GSuite][8] as they named it, which essentially meant "Gmail but for my other addresses".

I have now set up my own e-mail server to handle all the e-mails of 10 of my domains (at least so far). Doing the actual set-up and installation of an e-mail server isn't complete rocket science, though it obviously helps that I have previously created/started up a couple of thousand cloud servers and know my way around how to configure them both for security and functionality. It is probably not something "anyone/everyone" can, or even should, do though.

Setting up the [DNS][9] for the e-mails, and conforming the e-mail server headings etc correctly was quite a pain though. It isn't helped by the fact that the big guys like Gmail, Outlook etc have so many users already, and that in turn means you have to set up your own e-mail server quite specifically and correctly, to avoid automatically ending up in the spam folder of users of said services. For the customers of Gmail/Outlook this is actually a good thing: not just anyone can quickly set-up a server and start spamming people.

It is one thing if I am trying to avoid Gmail/Outlook, but I can't really force, or even exclude, the possibility of that a recipient of an e-mail from a user on my domains aren't using one of the big free services, so in short any e-mail server need to be set-up and configured properly, to avoid being thrown directly into the spam folder of Gmail/Outlook users.

This was also a process where you have to do a lot of trial-and-error. Unfortunately, Google has a 48 hour window for every single little changle you do to your DNS settings. If you make a typo? Wait 48 hours. Oh, there should be a ";" over in that DNS record? Wait another 48 hours. Ah, bugger, that record should be a txt record, not a DKIM record. Wait another 48 hours, etc. Combined with that [most e-mail testing services][10] also have a "maximum of 3 tests/day" and similar rules, one should not be in a rush when setting up e-mail servers, that's for sure.

![Mountain](mountain.jpg)

Now, finally, it is working though. Working well even. I now have a perfect e-mail server set-up, correctly configured that sends e-mails without ending up in any spam folder automatically. Also I can still use my [e-mail clients][11] as previously, both in my mobile devices and on my (various) desktops. I even have really nice web interfaces for both just the e-mail part ([Roundcube][12]) but better yet, for both e-mail/calendar in the form of [SOGo][13], which seems quite slick so far.

I have also started to de-coupling myself from my Gmail address within the various services I use and/or have accounts on. I don't see a good reason to why Google should know who my friends on [Flickr][14] are, nor should they know when I have booked flights or gig tickets. It is annoying enough that Microsoft knows who my contacts on [LinkedIn][15] are, without me having to feed that information straight into Google as well.

So yeah, that has been the evening of my week since last update. There is still lots of things to sort out, but I'll update here afterwards when I know what I've done.

[1]: https://imakethingswork.com/security/using-google-authenticator
[2]: https://vivaldi.com/
[3]: https://www2.meethue.com/en-us
[4]: https://en.wikipedia.org/wiki/Google_Authenticator
[5]: https://authy.com/
[6]: https://www.lastpass.com/
[7]: https://www.yubico.com/
[8]: https://gsuite.google.com/
[9]: https://en.wikipedia.org/wiki/Domain_Name_System
[10]: https://www.mail-tester.com/
[11]: https://airmailapp.com/
[12]: https://roundcube.net/
[13]: https://sogo.nu/
[14]: https://www.flickr.com/photos/hellquist
[15]: https://www.linkedin.com/in/mathiashellquist/ 