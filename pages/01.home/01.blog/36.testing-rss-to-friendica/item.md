---
title: 'Testing RSS to Friendica'
date: '2023-04-02 23:35'
taxonomy:
    category:
        - blog
    tag:
        - fediverse
        - rss
        - testing
    author:
        - 'Mathias Hellquist'
hide_git_sync_repo_link: false
media_order: 'Blue hills.jpg'
body_classes: header-fixed
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
bricklayer_layout: true
display_post_summary:
    enabled: false
feed:
    limit: 10
hero_classes: 'overlay-dark-gradient text-light parallax hero-fullscreen title-h1h2'
hero_image: 'Blue hills.jpg'
hide_from_post_list: false
published: true
---

This is the executive summary of the post where I am testing RSS to Friendica.

===

Right, this is just a test to see if I can post on the blog, which then should update the RSS feed, which in turn should be picked up by my Friendica instance, and thus being able to re-share it from my Akkoma account (which is my main account). Here's for testing. There is nothing to see here.

Actually, I am testing one more thing: I normally create posts directly on my desktop, sync to git and that updates the blog. This post however I am typing in an admin interface. I never do that, so I had to test that too. :)

<a href="https://brid.gy/publish/mastodon"></a>
