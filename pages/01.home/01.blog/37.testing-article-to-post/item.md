---
title: 'Testing article to post'
taxonomy:
    category:
        - blog
    tag:
        - fediverse
        - testing
    author:
        - 'Mathias Hellquist'
hide_git_sync_repo_link: false
media_order: ''
body_classes: header-fixed
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
bricklayer_layout: true
display_post_summary:
    enabled: false
feed:
    limit: 10
hero_classes: 'overlay-dark-gradient text-light parallax hero-fullscreen title-h1h2'
hero_image: ''
hide_from_post_list: false
published: true
---

This is just a quick test of adding an article to the blog to see if it is autoshared to other services.

===

Feel free to ignore.