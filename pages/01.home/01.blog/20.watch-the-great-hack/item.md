---
title: 'Watch The Great Hack'
media_order: map.png
date: '2019-08-29 20:35'
taxonomy:
    tag:
        - privacy
        - facebook
        - data
hide_git_sync_repo_link: false
visible: false
routes:
    aliases:
        - /privacy/watch-the-great-hack
hero_classes: 'text-dark parallax overlay-light title-h1h2'
hero_image: map.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
maintitle: 'Watch <b>The Great Hack</b>'
---

If you have wondered _why_ you should cut down usage of the big company services such as Facebook, Google, Microsoft, Apple etc you should probably watch the movie [The Great Hack][1].

===

[plugin:youtube](https://www.youtube.com/watch?v=iX8GxLP1FHo)

The movie, which is a documentary, can be viewed on [Netflix][2] (and also elsewhere online I noticed when searching for it) and it takes quite a deep look at the involvement of [Cambridge Analytica][3]'s role in the 2016 US presidential election, and also looks to a degree at Brexit and how it too was influenced by Cambridge Analytica.

Actually the movie highlights quite a few regions that have had political tension/problems, then someone hired Cambridge Analytica, who started to work on information-intelligence-warfare, often for "the other side", to make that other side do, or not do, something specific.

There are basically two things you can try to influence in elections: either what people should do OR what they shouldn't do.

The movie also explains how it came to be that Cambridge Analytica had _"5 000 data access points of every voter in the USA"_. This is the data that partially came from Facebook, various Facebook quizes, scraping profiles and friends-of-friends profiles etc.

Later in the movie they also explain how they had swung an election in Trinidad-Tobago, which apparently is divided into an Indian side and a Black side, by making young (black) people **not** go to vote. This in turn swung the entire election result to the Indian party that had hired Cambridge Analytica. Basically Cambridge Analytica appeared to work for the black youth community in uniting them towards a common goal of doing something cool, a political rebellion against politicians. The black community felt strongly in their conviction of not doing as the politicians said, and "rebelled" by not voting. As we all know though, elections are won by political parties that get the most votes, and to get the most votes you need voters. As the Indian kids did as their parents told them they went to the ballots to cast their votes, when the black kids didn't. The Indian side won. Mission accomplished. As pointed out, Cambridge Analytica did this without focusing on actual politics.

![Screenshot from CA's sales presentation](map.png?classes=caption,figure "Screenshot from CA's sales presentation")

Other elections mentioned in the movie where Cambridge Analytica or SCL, another company in the same sphere, played a major impact on the final result:

* Argentina 2015
* Trinidad & Tobago 2009
* Thailand 1997
* India 2010
* Malaysia 2013
* Italy 2012
* Kenya 2013
* Colombia 2011
* South Africa 1994
* Ukraine 2004
* Antigua 2013
* Indonesia 1998
* many more (the movie scrolls faster and faster)

Facebook obviously claims that it was its, at the time, "regretfully lax policies" that enabled Cambridge Analytica to scrape this data/information from Facebook, and not Facebook willingly giving it to Cambridge Analytica. However, we do know that [Facebook have conducted their own "tests"][4] in trying to affect how people feel, literally. They have placed messages in such a way that they wanted to measure if they could make people feel happy, or sad, which then in reality will affect what option they will choose when the options are limited (and controlled).

The Great Hack also highlights what a major impact Facebook-owned app/company [WhatsApp][5] played in the latest election in Brazil, where a populist right-wing politician now is the president.

A quote from the movie that stuck with me, and came from a sequence regarding "Crooked Hillary", filmed by a hidden camera at a restaurant: _-"We just put information into the bloodstream to the internet and then watched it grow, and this stuff then infiltrates the online communities, and it expands, but it is un-attributable, un-trackable..."_

Basically planting seeds of doubt. Plant a rumour here, another there. Like the childrens game "[Chinese Whispers][11]" ("Chinese Telephone" for our US friends), the message/doubt/rumour will take on a life of its own, and after a while it doesn't even matter what it was in the beginning.

So...I'm back at it again: if we, the users, don't give away our data willingly/freely (as we so far apparently have) the power of those big companies will be more limited. The "data intelligence" only works because we let it be just that: intelligent. If we don't reveal **all** our interactions, locations, thoughts and conversations to the same service, it will not be able to profile us like this.

It won't go away for sure, but it will be massively limited. There will probably always be a (or many) Cambridge Analytica-like company out there, and The Great Hack highlights that four former employees of Cambride Analytica currently works in the Trump team in preparation for the 2020 election...we are probably being subjected to their handywork already now, as this type of campaigns is all about seeding mis-information and letting it grow and take on a life of its own, so that weak minds have been properly confused come 2020.

These are parts of the reason why I wish to move away from, or massively limit, my interaction with the big companies. If they don't have the beans they can't spill them. If they don't have my data they can't leak my data.

Back to The Great Hack, though this isn't a review (I wouldn't know how/when/where to write a proper review), I did find it a bit one-sided. Some of the "whistleblowers" seems to be completely lacking moral compass, and became "whistleblowers" mainly for their own purposes of not getting blamed themselves. I do appreciate them helping out in highlighting this now, but I couldn't really shake the feeling of "right..._now_ you think it is a good time to do _the right thing_...but before...not so much?"

The movie also briefly touches on the fact that one of the main characters we follow in the movie, also worked on the Obama campaign in the previous election. Just saying, although Cambridge Analytica and its mother company SCL, are/were owned by [Robert Mercer][6] (billionaire, previous owner of [Breitbart News][7], and the largest funder of the Trump campaign), and that [Steve Bannon][8] (who was a Trump advisor) was the chairman of Cambridge Analytica, I would not exclude that similar tactics will be commonplace in the political landscape going forward, from either side of any political spectrum. It does however highlight that the, in this case, relevant right-wing supporters appear to be very susceptible to falsified facts, and doesn't feel a need to do their own fact checking. They blindly just believe whatever, if it is said by any "non-liberal".

It should also be noted that the Cambridge Analytica scandal, and [Facebooks involvement in it][9], led to Facebook being fined, to the tune of 5 billion USD. When that was announced the [Facebook shares went up with 6 billion][10]...as the fine should have been (and was expected to be) much larger to count at all as a punishment for a company like Facebook. 5-6 billion USD this way or that is all in a days work for companies like Facebook.


[1]: https://www.imdb.com/title/tt9358204/?ref_=fn_al_tt_1
[2]: https://www.netflix.com
[3]: https://en.wikipedia.org/wiki/Facebook–Cambridge_Analytica_data_scandal
[4]: https://slate.com/technology/2014/06/facebook-unethical-experiment-it-made-news-feeds-happier-or-sadder-to-manipulate-peoples-emotions.html
[5]: https://www.whatsapp.com/
[6]: https://en.wikipedia.org/wiki/Robert_Mercer
[7]: https://en.wikipedia.org/wiki/Breitbart_News
[8]: https://en.wikipedia.org/wiki/Steve_Bannon
[9]: https://www.theguardian.com/technology/2019/mar/17/the-cambridge-analytica-scandal-changed-the-world-but-it-didnt-change-facebook
[10]: https://www.youtube.com/watch?v=LZOXRN_qaFY&t=6s
[11]: https://en.m.wikipedia.org/wiki/Chinese_whispers