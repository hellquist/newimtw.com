---
title: 'Unbiased News'
media_order: 'allsides_big2.png,allsides_front2.png'
date: '2020-04-19 18:28'
taxonomy:
    tag:
        - media
        - bias
    author:
        - 'Mathias Hellquist'
hide_git_sync_repo_link: false
visible: false
routes:
    aliases:
        - /article/unbiased-news
hero_classes: 'overlay-dark text-light parallax title-h1h2'
hero_image: allsides_big2.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: allsides_front2.jpg
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'Finding news sources that aren''t biased can be hard. Here''s some tools and tips on how to find them.'
    image: allsides_big2.png
maintitle: '<b>Unbiased</b> News'
---

In this day and age finding good and trustworthy news seems to become a skill at least if you are looking for news based on facts, and not overly skewed from a political, or bias, perspective. But lets start from the beginning. What is "bias" anyway?

===

![Allsides](allsides_front2.png?classes=caption,figure "Screenshot of Allsides front page")

This is how [Wikipedia defines bias][1]:

> _"Bias is a disproportionate weight in favor of or against an idea or thing, usually in a way that is closed-minded, prejudicial, or unfair. Biases can be innate or learned. People may develop biases for or against an individual, a group, or a belief. In science and engineering, a bias is a systematic error. Statistical bias results from an unfair sampling of a population, or from an estimation process that does not give accurate results on average."_

Basically, what that in reality says is that people often only take in sources that confirm their previous theory/opinion on that matter. They will probably end up on the same opinion again, i.e. not changing views, and after having read/listened to the new article/web post/podcast or video, they probably even more enforced in their previous biased view now.

That sounds fine and well, however it is missing one critical thing: the ability (or willingness) to re-calculate outcome based on additional facts which should change the calculation.

### Bias is almost everywhere

There are lots of articles on the various bias types out there, and many funny (or scary) images/presentations on web sites/searches that you probably will find eerily accurate even for yourself and your own biases, because pretty much everyone of us have biases towards things, whether we know it. Sometimes it is on the least expected topics, with explaining the various bias types. I leave it to you to investigate them further. The Wikipedia link above lists most of them for this article.

As mentioned, most people have a bias, or a likely "affinity" towards certain ideas or political views. In laymen's terms this means your bias on a particular topic is like a hammer, and now you are looking for nails (whilst you at the same time are discarding screws or bolts).

This is also true for journalists. You should note that most journalists are aware of that they work on a news source that is leaning in a more general direction towards "left" or "right" as the world has called the political bias almost regardless of where in the world you live.

Most news organisations are open with what political target audience they are aiming for, and it will be clear even before you read, that all the news presented from them will enforce that political view, if nothing else to keep their target audience happy (and thus keep the advertising money or other funding coming in etc). This is true for the left. This is true for the right.

That is all fine and well for some things like “opinion pieces” etc, which often are marked as such. That way you know “this might not be investigative factual journalism, this is the opinion of this writer” and you should not confuse it with “facts”, which you in case of doubt should find elsewhere.

However, for reporting actual news, or factual news, a biased organisation (and they almost all are) will ask questions particularly to confirm that bias. When they get replies/responses to questions, they are likely going to not include the questions/answers where the response wasn't ideal for their bias target audience, and they are free to publish whatever they like from the material they have collected. This also means they are free to not publish some of their work too. They will compile an article/video etc in a way that confirms their target audience bias and keep their core readers happy. They will also add words that are more emotionally loaded and reactions based on feelings more than they are about facts. In short: They will do whatever it takes to make their heroes look more heroic and they will publish whatever it takes to make their opponents look more like villains.


!!! Note I'm not saying journalists or news organisations are doing a poor job, or misquoting or claiming that someone has said something they haven't said. I'm not saying they are outright lying (though, actually, some of them are, in particular the further out on the fringes of the far-left or the far-right ends of the political spectrum). <br><br>They are merely doing their jobs by selecting from their collected material, of which the journalist's own words are a huge ingredient, and how to present what has been said/found. Also, to not present a particular fact/question/answer can be a good/bad way in enforcing a particular view.

### So what can you do?

Well, there are tools and sources out there that can help you navigate this biased fast-moving world we live in. Also, there are a handful of news sources that mostly are reporting news from a factual point of view, without adding emotional triggers for any particular view/side/opinion. To tell you about them is why I have posted this.

First, let me just say that these tools/sources are based on English-speaking news and news sources. Sadly, I haven't found something similar for Swedish and if there are similar tools like the ones described below in any other language, let me know and I can link them too. For this article, however, we will have to concentrate on English as the primary language.

Also, most tools/services that "classify" news sources on a left/right spectrum, usually have the following labels, or similar: hard-left, leaning-left, center, leaning-right, hard-right.

Some news sources are "mixed", and can publish articles that are leaning both to the left and to the right, depending on the topic in question. The Economist, for example, [have adopted both left and right views over time][15], depending on the topics. Some, like Reuters, AP etc, are centric (not left nor right) and usually [just stick to the facts of events][16], or just quoting/citing people literally on what they said at a given time (which has to be considered a factual event).

! It should also be noted that you will have no problems finding people claiming all news sources that aren't on the same extreme fringe as they themselves are, will claim all other news sources are biased, as the other news sources aren't reporting the same things as their favourite "news" source is.

### All Sides

![Allsides - Example](allsides_big2.png?resize=500,500&classes=caption,caption-right,figure-right "Allsides - Example")

First out is allsides.com. Allsides presents news visually in a way that you easily can see the same news topic headline, and then you can see what the left-leaning news sources say about that topic at the same time as you also can what the right-leaning news sources say about the same topic. Also, there is a non-biased headline for that same article. Go there and have a look. Look at how differently they tilt the headlines to evoke the maximum amount of feelings for either side. Compare it to the "center" view, which only focuses on what happened, leaving it to the reader/viewer to form their own emotion based on the factual event.

It should be noted that allsides is very clear on two things:

1. They are basing their left/center/right based on American views. This means that the "actual" political spectrum can differ compared to where you live. For example: I live in Sweden. I would consider pretty much all Swedish political parties "left" to a smaller/higher degree in the US, regardless of if we in Sweden call them "left" or "right". In Sweden the right-most political parties are at most "centrist" in the US. In Italy it might be different. In Turkey it might be different again.

2. Allsides.com only rate the online sources of a news outlet. This is wise as they can differ from the printed/televised/radio version. Usually they have original teams working in the various publishing channels anyway, so we should consider them on their own merits.

Also, if we take the examples of CNN or Fox News:

* CNN, the online news, is leaning towards "left".
* CNN, the opinion pieces, are firmly rooted to the "left".
* Fox News, the online version, is leaning towards "right".
* Fox News, the opinion pieces, are firmly rooted on the "right".

### So how do you use this?

I would suggest that before you click "share" on the next link you have been triggered by, go to allsides.com and search for that headline. Chances are they have picked it up and can present that topic from published news from three or more sources with a different bias. Read all three articles, fully. Take in what they are saying. Do you still think it is worthy of sharing? Or had something perhaps been left out of the original article that triggered you, but you now know actually could be of importance for a collected view of the full picture?

Also think about the reasons to **why** you would like to share it. Is it to spread knowledge? As in, really? Or is it to sway opinion towards your own biased view of something you in actuality really don't know that much about? Do you wish to be "first with the latest"? That can make you look like a fool in a couple of days/weeks when it turns out it was actually a lie, just so you know.

### More tools

Another site that rates news sources based on bias is [Media Bias Factcheck][3]. [Here is their summary of Reuters][4] for example. You can see *how* they have reached the conclusion of why they have rated Reuters in exactly that way.

Compare their conclusion of [Reuters][4] with for example their conclusion of [Breitbart][5] or [Liberal Speak][6]. Or why not go all-in and have a look at their rating of [The Daily Conspiracy][7].

I know which of those four sources I'd rather get my news from: Reuters (just to make that clear). Also, I would never dream of "sharing" posts from the other three sources on social media. It is more likely that my cat accidentally types something share-worthy.

[Ad Fontes Media Bias Chart][8] is another tool that visualises the bias of various news sources. Obviously to these, there are people who will claim that such-and-such news source is not rated correctly, or they claim that the chart(s) are all wrong because their preferred biased news source is coming off less flattering than they would like to. That is how it is. Use these tools as you like, but as a guide at most.

[The Thread Weekly][9] is an e-mail list with weekly mailings, and which will give factual viewpoints of opposing opinions to a multitude of topics. It doesn't summarise it all to a conclusion, but leaves the conclusion to you, the user to make for yourself.

[Kialo][10] is for you who would like to argue your points on a specific topic yourself. I mention it here because it also draws views/opinions from two sides of any argument, and you can either add your own point to the discussion, or you can support a viewpoint you agree with. This will probably not lead to a conclusion that everyone agrees upon, but rather than sharing your biased views on Facebook or Twitter, Kialo might help you ventilate your thoughts without destroying your social media profiles.

[Living Room Conversations][11] is a similar service that allows you to chip in and both hear/read topics discussed from both sides of opinions, intending to bridge the gap and create a better understanding of your opponent's arguments.

[The Flip Side][12] is an e-mail newsletter, just like The Thread Weekly (above) that you can sign up for, and which will give you both sides of a story in just 5 minutes. For example, here is their presentation of the "[WHO funding][13]" topic, which is a hot potato right now at the time of this writing. You get to see arguments from both left/right at the same time as you scroll down.

### The Art of Arguing

Finally, before you continue to share your biased links from not-so-trustworthy sources and end up in internet arguments with strangers, remember there is [an entire field of expertise on carrying an argument][14] and being in a debate.

For those that have been in "debate/argument teams" in school, or for any tabletop role player (that applies to me, yes) you know that the best way to have a fruitful debate/argumentation most often comes from understanding your opponents view and arguments, and being able to poke holes in their balloon by presenting facts that support your argument, whilst removing the arguments of the opposition.

This can only happen if you truly have tried to understand *why* they have their opinion, which then is the reason to *why* they argue their points in the way they do. To truly understand someone's view on a topic you have to listen. Put yourself in their shoes. Work out *their why* so you can counter it properly with **your** why. Everything else is just moving hot air around.



[1]: https://en.wikipedia.org/wiki/Bias
[2]: https://www.allsides.com/blog/5-tools-avoid-media-manipulation-election-year?utm_source=homepage&utm_medium=BlogPost&utm_campaign=ToolManipulation
[3]: https://mediabiasfactcheck.com/
[4]: https://mediabiasfactcheck.com/reuters/
[5]: https://mediabiasfactcheck.com/breitbart/
[6]: https://mediabiasfactcheck.com/liberal-speak/
[7]: https://mediabiasfactcheck.com/the-daily-conspiracy/
[8]: https://www.adfontesmedia.com/interactive-media-bias-chart/?v=402f03a963ba
[9]: https://www.thethreadweekly.com/
[10]: https://www.kialo.com
[11]: https://www.livingroomconversations.org/
[12]: https://www.theflipside.io/
[13]: https://www.theflipside.io/archives/who-funding
[14]: https://thebestschools.org/magazine/15-logical-fallacies-know/
[15]: https://www.quora.com/Is-The-Economist-left-or-right
[16]: https://www.quora.com/Do-NPR-Reuters-AP-and-other-major-news-wires-lean-left-or-right-politically?share=1