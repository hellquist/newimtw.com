---
alert_color: blue
title: 'Important Notice'
published: false
hide_git_sync_repo_link: false
visible: false
---

I got bored and fed up with my old site, so I'm currently in the process of re-jigging it all in this new design.

As far as possible I try ensure the old URL's are still forwarding to the new URL's, though I honestly don't care that much.
