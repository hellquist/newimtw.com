---
title: 'Bye Bye Feedly'
maintitle: 'Bye Bye Feedly'
published: true
date: '2021-07-29 16:28'
taxonomy:
    tag:
        - rss
        - feedreader
        - feedly
        - feedbin
hide_git_sync_repo_link: false
body_classes: header-transparent
hero_classes: 'overlay-dark text-light parallax'
hero_image: arhino2.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: arhino2.jpg
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'Time to move on from Feedly and I need to find another good RSS Feed reader.'
    image: arhino2.jpg
summary:
    enabled: '1'
    delimiter: '==='
metadata:
    description: 'Time to move on from Feedly and I need to find another good RSS Feed reader.'
    'og:url': 'https://imakethingswork.com/blog/bye-bye-feedly'
    'og:type': article
    'og:title': 'Bye Bye Feedly | I Make Things Work'
    'og:description': 'Time to move on from Feedly and I need to find another good RSS Feed reader.'
    'og:image': 'https://imakethingswork.com/user/pages/01.home/01.blog/34.bye-bye-feedly/arhino2.jpg'
    'og:image:type': image/jpeg
    'og:image:width': 1280
    'og:image:height': 800
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Bye Bye Feedly | I Make Things Work'
    'twitter:description': 'Time to move on from Feedly and I need to find another good RSS Feed reader.'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/user/pages/01.home/01.blog/34.bye-bye-feedly/arhino2.jpg'
    'article:published_time': '2021-07-29T16:28:00+02:00'
    'article:modified_time': '2021-07-29T16:58:28+02:00'
    'article:author': 'I Make Things Work'
---

Time to move on from Feedly and I need to find another good RSS Feed reader.

===

I have been a [paying "Pro" user of Feedly][1] since 2014. In general it has been good and I've not had much to complain about when it comes to the actual service. Being a "Pro" user also has meant that I could "Power Search" for feeds and topics and add those as feeds. To be honest I had no idea search was a paid-for feature as I've been paying for it for so long.

![](search.jpg?classes=caption,figure "Feedly search")

Therefore I was a bit surprised when I tried to search for "Infosec" (and/or "Infosecurity") as a topic and as the first alternative got presented with something that looked great. Selecting that option leads on to the next page, which weirdly has an overlay where only two things are clickable: a "cancel search" or a sign-up button.

![](overlay.jpg?classes=caption,caption-right,figure-right "Feedly overlay")

I figured "*right, that is apparently what I have to do...*" and clicked through and ended up on a series of questions about my company/employer, my title, work e-mail address etc. I started feeling uncomfortable about it now, because I was thinking "*surely you know all the things you need to know about me...I'm a paying customer, right?*"

The entire string of questions ended in a time-slot booking form that was impossible to exit out of, bar closing the entire desktop app. So I did. And felt like a right-old gullible fool. By now I had already parted with my work email address (which is different from my Feedly user email address) and lo and behold, pretty soon there was a bling from my email inbox that I had received an email from Feedly.

### Email and presentation

In the email I was asked if I wished to book a time-slot (as I hadn't finished that part of it all), and I was also provided with a link to a presentation describing what the product actually included, because this apparently was a product.

Let's pause here for a moment and for reference tell you that my Feedly Pro account costs me $45/year. Not a lot, but also not nothing.

Back in the presentation, at the end of it, I was informed of that what I was about to sign up for would cost me $1200 for the "Standard Edition", or I could get the "Advanced Edition" for $1600. Per month.

![](presentation.jpg?classes=caption,figure "Feedly presentation")

For friends of maths, that is $14400 - $19200 over a year. I think it is fair to say that is quite **a lot** more than the $45 I am already paying.

### Conclusion for me

The takeaway in all of this is a massive bugbear I have with many services online: why can't they be transparent about the cost up front? Why can't they be transparent about the feature-set of the service, up front?

A few simple lines of text information would have saved both me and Feedly from a lot of trouble, and I would have gone "*oh, right, this is not something I need or even want, besides the price-point being in the realms of pure fiction and fantasy*". Have you mistaken me for someone who flies to near-space just for the fun of it?

I'm sure the service/feature itself is fine. It looks quite impressive. Not a "please-here-have-my-firstborn-in-exchange-for-this-service" fine, but probably a decent service all in all.

Anyways...due to all of this, and due to me thinking I should not support companies that do bad UX and sneaky sales, but instead are honest up front regarding what they are about and how much it will cost me, I'm in the market for a new RSS Feed reader service. Currently [Feedbin][2] (thanks [Phil Sheard][3]) looks like it is the winner for me.

Feedbin has functionality for following social accounts like Twitter and automatically combine "threads" into long posts. It also has functionality for parsing newsletters, which would be a nice reprieve to my bacon-filled mail inbox. They are also abundantly clear on what they charge and what I will get for it. I  can even search as a part of the deal. I think it might be worth checking out, if nothing else as they have a 14-day trial too. If I like it I'll do a write-up of it.

If anyone have opinions and/or tips, please contact me.

[1]: https://feedly.com/i/pro/welcome
[2]: https://feedbin.com/home
[3]: https://twitter.com/philsheard