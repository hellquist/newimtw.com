---
title: 'Evidence of Conspiracy'
published: true
date: '2020-05-09 15:30'
taxonomy:
    tag:
        - conspiracies
        - proof
        - evidence
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: 271244.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'There are many conspiracy theories flying around, but there is very little evidence to support them...'
    image: 271244.jpg
maintitle: 'Evidence of <b>Conspiracy</b>'
subtitle: 'It doesn''t exist, does it?'
---

Did you hear _"Bill Gates will make billions of $USD on a vaccine as he owns the patents for such a vaccine"_? Or perhaps did you hear _"viruses spread via 5G towers"_? Or what about _"the vaccine will contain digital trackers to control us all"_?

Yeah, I saw them too. What I haven't seen was any actual evidence to support a single one of those claims though...

===

I find it interesting but not amazing that in times like these, the people who normally blurt out their conspiracy theories have added some fresh ones to their arsenal. What I *do* find surprising, and also very weird, is how "regular people" allow these stories to spread in their social media streams, either by re-sharing them actively themselves, or by not objecting to their friends sharing them. I understand people might not be keen on confronting their friends online, though given the times and the trends, arguing with strangers online seem to be a favourite hobby for some quarantined people.

Now, I understand there are many factors across a wide range of topics that feel new, unknown, and some of them are scary. Still, that shouldn't stop us from seeking truth, and to further our knowledge. When I say ”further our knowledge” I mean we should be seeking facts.

This is where it goes wrong though: people apparently have confused "published online" with actual "knowledge". Therefore I would like to highlight a basic fact for you:

Almost anyone can, and does, publish almost whatever they like on the Internet. It is a great freedom we have, and it is an essential part of what many countries have as a basis for their legal system: [freedom of speech][1]. Some countries do not even have this. Therefore, those of us who have it should value it. Carefully weigh it. Appreciate it, and ideally, use it for good. Freedom of speech does not mean freedom of consequences though.

### Freedom of Speech

Freedom of speech means that pretty much anyone can say what they feel like, without fear of being censored by the government(s), and what better medium/platform for that than the Internet, right? We could probably spend a separate article only on covering Freedom Of Speech, but that is not my target here, I merely wanted to point out it is very easy to publish things online, for anyone. You are for example reading this self-published text on this very blog. I wrote this. You don’t have to agree with me, but it is still my right to express my opinion here on this topic. It is your right to agree or disagree with me, or to ignore me completely. People have died for us both to have these rights.

But I hope that you consider what I write critically and still with an open mind: just because I have published it online doesn't make my words true, it merely means that I have a platform to publish my thoughts on. You have the same possibility to do the same. Almost everyone I know does. And, you know, there’s always Facebook…

Now, here comes the crux of the matter: because I _know_ how easy it is to publish things online, I *read* things online with extreme caution, bordering on suspicion. If I find, or read something online that makes me stare in disbelief and think _“really?”_, I need to investigate further, and to find facts from more sources on the same topic. I have even spent some of my time trying to help people to [find non-biased news sources][2], to ensure they stay with the facts, and not following someone's political or commercial agenda, which may or may not be “hidden”, but still often is there.

### The lure of videos

The same goes for all conspiracy believers favourite media channel, [YouTube][3]. Here's a tipoff to the rest of you: if someone breaks into a fruitful, interesting  discussion you and your friends are having on whatever topic that might be under debate and claiming it is all wrong, and then leaves a 1-hour YouTube video as their “evidence”: block and/or ignore. These days you can nigh on measure how much a conspiracy believer is close to being a proper “nutcase” by looking at “minutes of YouTube videos shared”, and I haven’t even mentioned checking the validity of the creators to said videos.

Again, literally anyone with a smartphone can upload videos to YouTube. The combination of "free speech" along with an ever increasing population means that a larger and larger number of people, that probably shouldn't, now can upload whatever they like on a global video distribution platform. 

A good video can definitely help digest some boring reports of statistical data, or it can help to support scientific areas by distilling it down to the easily presentable facts. If someone were to tell me about the genome structure and how a certain illness could affect it in detail, via written text or via speech only, I would probably start off nicely, but slowly doze off more and more unless they were excellent presenters. Show me a video of the same genome structure and how it looks before/after that illness probably would illustrate the same thing, literally in minutes, and make it quicker for non-professionals like myself to take in. It is easy to see the lure of a well produced video.

Also, people seem to enjoy watching movies, as an entertainment channel video it is not bad at all. However, if we are talking about scientific facts, the video should *support* the written evidence related to it and that it also ideally links to, as that is the _actual_ evidence. This is where pretty much all the conspiracies go downhill quickly. They can't produce the facts. They can't produce evidence, and they especially can't produce evidence that can be replicated, either by themselves (not as important) or _anyone_ else, which is *very* important if you are trying to prove something that goes against verified scientific theories/facts.

### The word "theory"

Speaking of the word "theory", that is another favourite word from the conspiracy people. They have completely missed that there is a difference in actual meaning between a speculative “theory” (i.e. _-“I think it will rain tomorrow“_ or _-“I think Rey is Anakin Skywalker’s cousin“_) with the meaning of a scientific “theory”. Somehow they seem to think the word "theory" in the term "Conspiracy Theory" is of equal weight to the "theory" in "Scientific Theory". It is not. They are not even related, and it is a shame they are spelt the same.

Once again will I lean on Wikipedia to tell you about [Scientific Theory][4]:

> A scientific theory is an explanation of an aspect of the natural world that can be repeatedly tested and verified in accordance with the scientific method, using accepted protocols of observation, measurement, and evaluation of results. Where possible, theories are tested under controlled conditions in an experiment. In circumstances not amenable to experimental testing, theories are evaluated through principles of abductive reasoning. Established scientific theories have withstood rigorous scrutiny and embody scientific knowledge.

But it also goes on by saying this (scroll down on the link above), and as it is important, I feel I need to include it as well:

> A common misconception is that scientific theories are rudimentary ideas that will eventually graduate into scientific laws when enough data and evidence have been accumulated. A theory does not change into a scientific law with the accumulation of new or better evidence. A theory will always remain a theory; a law will always remain a law. Both theories and laws could potentially be falsified by countervailing evidence.

There aren't many things similar between a speculative conspiracy theory (which are guesses) and a scientific theory, which can be proven repeatedly often earlier than they actually can be scientifically explained.

However, if you read those passages again, or better yet, read the source links over at Wikipedia or wherever you get your facts from, you notice that it all boils down to a key concept: evidence. You know, the thing that conspiracy theories lack in 100% of their cases.


### Presenting actual evidence

Speaking of evidence, there appears to be some confusion on how the “burden of proof” works, so let's have a look at that too. This is what Wikipedia says regarding [“burden of proof” regarding law][5] (as in legislation):

> The burden of proof is always on the person who brings a claim in a dispute. It is often associated with the Latin maxim semper necessitas probandi incumbit ei qui agit, a translation of which in this context is: "the necessity of proof always lies with the person who lays charges."

That is both pretty clear and also very logical. If you claim that a previously proven fact is incorrect, you would need to prove how, when and where it is actually incorrect by submitting new/superior facts that make your point obvious. Makes sense? Fair? However, Wikipedia continues like this too, on the entry for [Burden of proof (philosophy)][6]:

> In a legal dispute, one party is initially presumed to be correct and gets the benefit of the doubt, while the other side bears the burden of proof. When a party bearing the burden of proof meets their burden, the burden of proof switches to the other side.

Apparently that definition is too straight-forward for conspiracy nutters to understand, as they keep failing on that very specific point. Instead, they appear to have adopted a stance which appears to go like _“I refute your scientific proof based on that I don't like it, and it is now your task to change my mind”_.

No. No no no. That is not how it works. That is not how _anything_ works.

The even more common thing I see circulating right now is the demand from the conspiracy nutters to prove a negative, and a negative that they themselves have made up no less. I read a very [interesting article on “evidence”][7] yesterday (thanks Sean for posting it). Go read it.

### Impossible to prove a negative

Also, I read an article earlier today regarding negative proof today which I can't find right now (if you know where I read it please send me the link), but which summarises the absurdity of proving a negative quite well:

>-"I heard the president ordered 17 hookers dead!"<br>-" I don't believe that is true."<br>-"Then you must prove me wrong."

For obvious reasons it is impossible to prove a negative, but _exactly_ that pattern is what the conspiracy people use time and time again, along with calling anyone who doesn’t nod along to their tune or perhaps even raises an eyebrow at what they say (probably out of concern for the mental health of the conspiracy spreader) with _"sheeple!"_ or _"don't you see the truth!?"_ etc.

When you ask them for basic things like _"sources?"_ or _"proof?"_, they simply dismiss it with _"if you don't know where to find it you are doomed anyway"_ or _"it is out there if you know where to look"_ or, of course _"here's 6 YouTube videos"_ (each clocking in at 2 stiff confused hours, and you wonder what the actual question was again after 5 minutes in to one of them, because you decided to give it a chance, and after 15 minutes you've given up hope for humanity to actually make it altogether) or something along those similar lines.

Right, so that is what I think on the topic. But I still can't understand why otherwise somewhat logical people re-share illogical hearsay and obvious conspiracy theories, without proof or evidence. Stop it. Or as [Kat Montgomery so elegantly put it in a Facebook post][8] when reaching the topic of conspiracy theories being removed from Facebook, YouTube etc:

> Lastly, private companies removing false information from their platforms does not represent repression or promotion of propaganda. It’s helping to promote the spread of sound scientific information. If you think lies should be permitted to circulate freely alongside the truth with the intention of reaching people who won’t be able to tell the difference, you are part of the problem.

[1]: https://en.wikipedia.org/wiki/Freedom_of_speech
[2]: /blog/unbiased-news
[3]: https://youtube.com
[4]: https://en.wikipedia.org/wiki/Scientific_theory
[5]: https://en.wikipedia.org/wiki/Burden_of_proof_(law)
[6]: https://en.wikipedia.org/wiki/Burden_of_proof_(philosophy)
[7]: https://eu.freep.com/story/opinion/contributors/2020/05/05/coronavirus-evaluating-evidence/3083768001/
[8]: https://www.facebook.com/kathleen.weber.montgomery/posts/10113287265749743