---
title: 'Non-tracking alternatives'
maintitle: '<b>Non-tracking</b> alternatives'
media_order: 155772.jpg
date: '2018-03-21 23:04'
taxonomy:
    tag:
        - privacy
        - facebook
        - google
        - mastodon
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/non-tracking-alternatives
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: 155772.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

The last couple of weeks a story about [Cambridge Analytica][1] accessing [Facebook][2] data to ensure [Brexit][3], Trump winning etc has been making the rounds. This has in turn led to lots of people killing off their Facebook accounts, often with an accompanying note saying "Facebook is unsafe, you can find me elsewhere like Twitter, Instagram, mail etc".

===

Now, leaving Facebook today due to what happened in 2014-2015 doesn't actually solve anything. However, the act of leaving the big guys like Facebook, Google, Yahoo etc behind isn't all new, and have many other good reasons. It should be noted I'm a heavy user of all these services myself, though I have been contemplating limiting my involvement with them.

> As a sidenote: if you say you are leaving Facebook and instead can be found on Instagram, you haven't actually truly left Facebook, as Facebook owns Instagram. There.

There are lots that can and will be said about this latest scandal, however, this is not that article. This article is here mainly to point out that there also are other alternatives to most of the "big" services out there. The problem with the "competition" to the giants is, of course, that not enough people are using them, which can mean they feel empty and lonely at times. There are lots of benefits though. Most often the alternatives are free of advertising. Also, if we are talking abbout federated open-source solutions, they are often also void of data collection activities and "[algorithms][4]" that does clever things to/for you.

## Moving from Facebook / Twitter

### Mastodon

If you are looking for alternatives to Facebook and/or Twitter, you might want to check out [Mastodon][5]. It is much like twitter in that it has timelines that you can configure and share things on. However, you can write longer posts on Mastodon than you can on Twitter, though they have a smaller size than the full-on articles you can write on Facebook. Mastodon is free to use, and you can even host it yourself as it is a "[federated][6]" network. If you don't understand the wikipedia article: basically there isn't a central server storage, but many connected nodes that interact instead.

Those nodes can have different rules and can also be about various specific topics, such as "photography" or "music" etc, or be in specific languages. It is up to the owner of the node what rules that go for those who register/use that particular node. However, and this is the main difference, the different nodes can talk to each other. You can even host a node yourself on your own server if you would like to.

Mastodon also has a bunch of various apps for your device. I use [Amaroq][8] for iOS myself, and I find it to be quite good, but there are lots of [alternatives][9] for most platforms and browsers.

### Diaspora

A site that has been around for a while, and was launched as an alternative to Facebook, only "owned by the users", but which never really kicked off big time is [Diaspora][11]. I had even forgotten I already have an account on it, but clicking around in it today it indeed looks quite nice and scaled back. It looks like Facebook without the ads, groups and sidebar stuff. And no blue. If there are apps for your devices to interact with Diaspora I couldn't find them in (iOS) AppStore when searching for Diaspora.

Just like Mastodon you can host Diaspora on your own servers if you would like to, though I've read you probably need some programming skills to do so.

### Ello

[Ello][13] was launched 2012 as a Facebook alternative, but "without the targeted advertising". For those keen on grammar you quickly realised that promise could include advertising, it just wasn't targeted. Also, as pointed out by some experts, Ello had taken VC money. That normally means that they would have had a plan on monetising something at some point (as VC's aren't famous for giving money away for nothing in return), especially seeing as the service was/is "free" for the users. It all became clearer quite recently:

The last 1-2 years Ello has rebranded itself as **The Creators Network**, and is a *"community of artists"*. This means you can actually use Ello to sell your creations; right next to the button saying "upload image" there is another button saying "$ Sell". Currently I have nothing to post or sell, so I can't try out the functionality, but I'm guessing Ello cut a slice in between whatever price the artist sets and what is actually being paid by the buyer.

### micro.blog

There is also [micro.blog][7], which I literally learnt about today, and therefore I haven't tried it out properly (though I got myself an account now). It isn't totally free but it is free of advertising. When I say it isn't free, you actually get 10 days trial of the "full thing". After that it defaults to actual free, but which also limits the functionality. They then have 2 paid tiers:

* $2/month allows you to do cross-posting to Facebook/Twitter from your micro.blog site
* $5/month will give you your subdomain for your existing site, or hosting with the service under a unique URL (like hellquist.micro.blog). It also includes the cross-posting functionality as well as ability to publish from the web, iOS and Mac. Furthermore you also get access to various "themes" and you can add "pages" with static content, such as an "About me" page etc.

As the old saying goes, and which has been proven by Facebook lately: *"if the service is free you are not the consumer, you are the product"*. Therefore it can make sense for some to actually pay for a service, putting themselves into the customer seat again.

Micro.blog can act as a subdomain for your own existing web site. You cannot host it directly yourself.

## Moving from Instagram

### Vero

A service that has been around since 2015 is [Vero][10]. They got renewed interest last month and in fact so many new people signed up on it that it completely crashed. Vero had to abandon their previous offer, which was to give a free-forever account to the first 1 million people who signed up (like myself) but couldn't login or upload things, that they now have a an offer that gives everyone a free account, currently "forever".

**Vero only works on your smart phone**. Furthermore, you need to provide a functional phone number to be able to register. There isn't a web interface for the service, only for brochure information about the service. On Vero you can, in a really quite slick app, quickly share the following types of content:

* Camera
* Link
* Music (that you listen to)
* Movie/TV (that you are watching)
* Book (that you are reading)
* Place

Yes, that is right, there is no "text" or "post" or "tweet" that can be shared, though you can add some commentary text to accompany your shared/recommended thing, and your followers can comment on your shared thing.

What you can't do is add whiskers, oh-so-funny-noses, horns or glasses etc to your photos, like on Snapchat etc. In my book that is a good thing, and these days quite refreshing.

Vero is owned by an arabic oil sheik prince worth billions in Euros.

### Eyeem

Another service I had forgotten about, but found now when I was looking around for "alternatives" to "the big ones" was [Eyeem][12], which I also found I have an account with. Redownloading their app I notice their strapline now says "make money with your photos", which also hints at their business model: basically you, as a creator of photos, can sell your photos with the "market" in the Eyeem app. If one of your photos is sold you get 50% in commission (which actually is quite bad as far as commissions go, but hey, to some better than nothing I guess).

Eyeem also have quite detailed explanations on their web site regarding privacy policies, user data etc. I do not know if they have taken VC money or what the owner structure is like though.

### The services above

Obviously you can share photos on the services listed higher up on the page too, however Instagram doesn't let you post on their service without also adding a photo, which means Vero is the one that is most like it in that sense. All those other services lets you add text as the main content of your post.

Also, as those with a keen eye can notice, I have not listed QQ, Wechat, Renren or Weibo (all Chinese) or the Russian ones like VKontakte, mainly because I don't think moving from "The Big Ones" should mean "swap for a new unknown but possibly as dangerous" service, and given that most readers of this site are not in those areas I have excluded them on purpose.

## Swapping out the rest

I just realised that I know most of my visitors for this site are either Scandinavian or otherwise English speaking much due to Google Analytics. Now, I have actually been thinking I should try to move away from all the Google services as well, so I might actually do a follow-up article from a web site owners point of view, where I might have worked out how to swap out Google Analytics for Piwik/Matomo and mail to...hmm...I shall have a look at the best mail solution too.

## Final words

When trying to list all the alterntive services I not only found that I have accounts with most of them, but also, ironically, that I have used Facebook to create the accounts on the other services. This also highlights that that moving away from a service, or adopting a new service, can be done cleverly and almost always should be done carefully, with an exit strategy. I didn't/haven't and therefore I have quite a few things I need to edit/change before I can move away from a service.

[1]: https://en.wikipedia.org/wiki/Cambridge_Analytica
[2]: https://facebook.com
[3]: https://en.wikipedia.org/wiki/Brexit
[4]: https://neilpatel.com/blog/facebooks-algorithm-revealed-how-to-remain-visible-in-the-cluttered-facebook-news-feed/
[5]: https://mastodon.social/web/accounts/7877
[6]: https://en.wikipedia.org/wiki/Federated_portal_network
[7]: https://micro.blog/
[8]: https://itunes.apple.com/us/app/amaroq-for-mastodon/id1214116200?mt=8
[9]: https://github.com/tootsuite/documentation/blob/master/Using-Mastodon/Apps.md
[10]: https://www.vero.co/
[11]: https://joindiaspora.com
[12]: https://www.eyeem.com
[13]: https://ello.co