---
title: 'Dependency Confusion'
maintitle: Dependency
subtitle: '<b>Confusion</b>'
published: false
date: '2021-02-12 14:28'
taxonomy:
    tag:
        - code
        - frameworks
hide_git_sync_repo_link: false
body_classes: header-transparent
hero_classes: 'overlay-dark text-light parallax'
hero_image: code_soup.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: code_soup.png
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'Installing one framework usually leads to installing a string of interdependent frameworks'
    image: code_soup.png
metadata:
    description: 'Installing one framework usually leads to installing a string of interdependent frameworks'
    'og:url': 'https://imakethingswork.com/blog/dependency-confusion'
    'og:type': article
    'og:title': 'Dependency Confusion | I Make Things Work'
    'og:description': 'Installing one framework usually leads to installing a string of interdependent frameworks'
    'og:image': 'https://imakethingswork.com/blog/dependency-confusion/code_soup.png'
    'og:image:type': image/png
    'og:image:width': '1200'
    'og:image:height': '692'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Dependency Confusion | I Make Things Work'
    'twitter:description': 'Installing one framework usually leads to installing a string of interdependent frameworks'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/dependency-confusion/code_soup.png'
summary:
    enabled: '1'
    delimiter: '==='
---

Snappy summary of what this is about

===

The main bulk of the content


<!-- 
Image variables
![Allsides - Example](allsides_big2.png?resize=500,500&classes=caption,caption-right,figure-right "Allsides - Example")
-->
<!--
Links
[1]: 
[2]: 
[3]: 
[4]: 
[5]: 
[6]: 
[7]: 
[8]: 
[9]: 
[10]: 

-->