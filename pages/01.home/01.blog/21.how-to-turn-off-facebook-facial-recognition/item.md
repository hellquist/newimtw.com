---
title: 'How to turn off Facebook facial recognition'
media_order: '97892.jpg,settings.jpg,settings2.jpg'
date: '2019-09-06 20:07'
taxonomy:
    tag:
        - privacy
        - facebook
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/how-to-turn-off-facebook-facial-recognition
hero_classes: 'text-light overlay-dark-gradient title-h1h2'
hero_image: 97892.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
maintitle: 'How to <b>turn off</b>'
subtitle: '<b>Facebook</b> facial recognition'
---

The latest improvement for your online privacy from Facebook is to enable you to turn off Facebooks automated facial recognition. You should. This is how you do it.

===

Facebook can do all kinds of tracking of its users, i.e. you and your friends, and it can draw even more kinds of conclusions based on what you’ve said or how you’ve interacted. However, as they are now also closely monitored by the world (due to Cambridge Analytica etc) they have taken a few steps in letting you control **what** of your personal data they can use, and also **when** and **where**. The latest changes is regarding your face and Facebooks facial recognition algorithms.

Turning off the automated facial recognition will do the following for you and your privacy:

* Facebook will no longer automatically tag you in images (your own images or images uploaded by your friends)
* It will also stop the “photo review” feature, which searches any/all images and looks for you, even if you are tagged by Facebook in that image or not

[![Facebook settings](settings.jpg?resize=698,418&classes=caption,caption-right,figure-right "Facebook settings")](settings.jpg)

More importantly, until now Facebook has, behind the scenes, created a “template” of your face, which is set of numbers based on a bunch of criteria (distance, orientation etc), which it uses to compare your face (or actually your string of numbers) to other number strings extracted from images on their site. Turning off the facial recognition will delete your template. It will also mean Facebook can’t create a new template for your face as long as you keep that setting disabled. If you later turn it on again Facebook will be able to create a new template of your face.

[![Facebook settings](settings2.jpg?resize=698,418&classes=caption,caption-right,figure-right "Facebook settings for facial recognition")](settings2.jpg)

To disable the setting for facial recognition you need to go to your [profile settings][1] and then click on "Face recognition" on the left hand side (on the desktop) or scroll to it in the Facebook app.

Then you check the value of "Face recognition". If it says anything other than "no", click "edit" on the far right of the screen, and change your value to "no".

That is it! If you have done that you have increased your privacy on Facebook a little bit. If you, unlike me, plan on continue to use Facebook regularly, there are a bunch of more settings you probably should have a look at, to verify you are happy with what they are set to.

### More extreme ways to avoid facial recognition

As I was researching for this topic I came across a couple of other links that can help you in trying to avoid facial recognition. We live in interesting times indeed when these things are actually done by some.

* [Hide from facial recognition via jewelry][2]
* [Jewelry and hair-do to confuse facial recognition][6]
* [Confuse surveillance cameras with fashion tricks][4]
* [...and even more fashion with textile patterns][5]
* [The hardcore way to avoid facial recognition][3]


[1]: https://www.facebook.com/settings
[2]: https://mymodernmet.com/ewa-nowak-avoid-facial-recognition/
[3]: https://www.survivopedia.com/6-ways-to-defeat-facial-recognition/
[4]: https://www.theguardian.com/world/2019/aug/13/the-fashion-line-designed-to-trick-surveillance-cameras
[5]: https://www.theguardian.com/technology/2017/jan/04/anti-surveillance-clothing-facial-recognition-hyperface
[6]: https://cvdazzle.com/