---
title: 'Using Evernote Productively'
published: true
date: '2020-05-11 20:14'
taxonomy:
    tag:
        - productivity
        - gtd
        - evernote
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: evernote_bar.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
summary:
    enabled: '1'
    delimiter: '==='
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'Set up Evernote for automated morning routines to kickstart your day'
    image: evernote_bar.jpg
maintitle: 'Using <b>Evernote</b> Productively'
---

Keeping productivity up can be a challenge, especially when working from home. It has made me do a re-jig of the tools and routines I use. I am now back at using [Evernote][11] for my "input", to help me sort out the rest of my day. This is how I have it set up.

===

During my eternal search for the holy productivity grail I've learnt that it doesn't exist, at least not as described by others, for me. Maybe it is me. It probably is. I've tried _all_ the tools. I've tried _all_ the methodologies. Still, I haven't been fully content with any of them.

Therefore, I am now, and have been for a couple of months back at using Evernote as my primary input tool or "work handler". It is now a part of a larger Frankenstein system I use (which I can describe later, but which works for me), but now I'm "only" using it for sifting through tasks/to-do's, prioritising them and keeping track of them being actioned.

The major difference now compared to earlier is all about mind-set: I tried to use Evernote for "everything". That included "content input", "as a database" etc, though I don't really like it for those purposes. For example, it doesn't handle Markdown (hello?) and it has a very clunky template system that doesn't handle images in any logical fashion at all. As a "database" it lacks some very important features too.

Still though, it _is_ cross-platform with a solid sync between devices, and I like the "search text within PDF's" functionality which is very handy when I use the companion app [Scannable][4] for my travel expenses and receipts, etc. I can easily forward items to Evernote from other apps/software, and with a little hack I can even add files that are put in folders automatically to Evernote (this isn't a big thing for PC people, but weirdly this functionality has never made it macOS/iOS).

### My new world order

My current productivity workflow in Evernote goes like this:

The morning starts with IFTTT creating a new note in my Evernote at 07.00
It has a basic list of tasks to set me off on the correct course for the day. Currently, it looks like this on IFTTT:

![](ifttt.png?classes=caption,caption-right,figure-right "IFTTT settings")

```html
<b>Today's List</b><br><br>
Review Calendar<br>
Mail Inbox Zero at Work<br>
Mail Inbox Zero at Home<br>
Review EN Inbox<br>
Review EN Reminders Pending<br>
Review EN previous day/week<br>
Take medication<br>
Take vitamins<br>
Eat Breakfast<br>
Eat Lunch<br>
Exercise<br>
Write (at least) 500 words for article<br>
Make New Daily list<br>
Summarise today’s progress
```

I open up that note, put down the cursor in the title field and press CMD+SHIFT+D which adds today's date after the weekday name it already has in the title.

I select all the text lines in the note and press CMD+SHIFT+T which turns each line into a task with a checkbox in front of it.

I didn't invent this myself; I got this excellent tip from [http://www.torgersons.com/detailed-routine-using-evernote-for-tasks-and-tracking/][1] but I have found it to be useful for me to actually using Evernote at all. For me it is about removing barriers, otherwise I might skip doing things, and skipping things is a slippery slope towards chaos, I know that from experience.

Some tasks on that list are there because my brain needs them there, otherwise I would simply forget. Weird, but hey, apparently that is how my brain works. I move this list to a notebook called "Action Pending" and I ensure I have tagged it with "1-today" (it actually comes automatically tagged from IFTTT with this tag). This way it will be visible in several views that are easily accessible from all my target platforms, especially mobile devices that don't have as many settings as the desktop version of Evernote.


### Going through email inboxes and calendars

Anyway, now I have a blueprint for the morning, because this list will get longer in just a couple of minutes. Even though that list isn't in priority/time order it still starts off in the correct order, and so I chip things off from the top.

If I find something in my calendar that I need to clarify, prepare or otherwise take note of, I create such a note in Evernote now, even if the contents is just the headline of the calendar entry, like the meeting name.

Then I move on to my inboxes, both for work and my private emails (which are spread out over 9 different accounts, all with different purposes). Here I delete a lot (and I try to remember to unsubscribe to both spam and [bacon][9] as well, to make my inbox leaner and more manage-able tomorrow).

Mails that are just confirmations or "for info", I archive right there and then. What I'm looking for are emails that require further action or will be needed for future important reference. Here I also adopt the GTD view: if it'll take me less than 2 minutes to respond to the email, I do it right away. If it will require more of my time and/or brain power than 2 minutes, I forward the email to Evernote. Evernote will turn the mail into a note with the subject as the note title.

When I'm done, I have reached "Inbox Zero", which I have realised now works really well for me (see notes below). The theory being if it is in your email inbox you haven't processed it and decided on an action for it. Everything else you will have an action plan for, at least in a couple of minutes (right now you have just done rough sorting). I should add that now I also close my e-mail clients to avoid being distracted by notifications and/or sound alerts, etc.

### Decide what actions to take

The astute reader will have noticed I now merely have moved all my email inboxes to my Evernote inbox. That is correct, and also therefore the next action on my to-do list for planning my day. In Evernote I open up my Inbox and start deciding on when and where I shall act on the items in there. I tag up every item accordingly. As the tags already exist in Evernote, this is quick, and usually involves typing the first letter of the tag and then Enter.

Some things are just for reference, so I have a notebook named exactly that: “Reference”. I also have folders for “Receipts”, “Research” etc. Most things are “tasks” that I need to follow-up and/or act on, so I move them to a “Backlog” folder. I also tag them with a relevant time frame when I expect to action the item, for example “2-thisweek” or “3-thismonth” etc.

I also add relevant helper tags to make finding the notes from various views as simple as possible, for example, "@work" or "@home" or "@shop" and also, if applicable, project name.

![](today_list.jpg?resize=301,338&classes=caption,caption-right,figure-right "Today's list")

When I've gone through all the items in the backlog, they have all been tagged as correctly as possible. I then move down to the tag view for "1-today". It will list every note that has that tag and thus will be my tasks for today. Also, this is where I can find my master task list for the day that IFTTT created for me this morning. Here's another nifty trick from [Torgerson][1]: select all notes (but de-select the master list) and select: "Create Table of Contents Note".

That will create a new note, with each of your selected notes titles on a new line, linked up nicely. Select the contents of that list, click the “list button” to remove the numbering, and tick the checkbox button to put check boxes in front of each line. Then copy that list to your daily master list at the bottom. This is now your to-do list for today, along with whatever tasks you have left from your default template that IFTTT sent you earlier.

This process described above probably takes longer to read about in this post than it takes to actually do, especially when you get in to it.

### Taking notes in the notes

Many items in your master list for the day will also have a link to that specific note where you can add further details/information. This is perfect for meetings notes, or for adding development notes to an email that you needed to follow up, etc. You get both the rough overview of headlines of the tasks, but you can still add as much/little details as you like to each note, without cluttering up the master list.

I think the key here in this workflow is that I have cherry-picked a bunch of tips and tricks from various methodologies, where neither worked perfectly for me on their own, to something that fits my daily life and, perhaps more importantly, my brain.

The key inspiration for the above Evernote workflow comes from previously failing in doing the following routines/methodologies:

* [http://www.torgersons.com/detailed-routine-using-evernote-for-tasks-and-tracking/][1]
* [https://thesecretweapon.org/][2]
* [https://gettingthingsdone.com/][3]
* Inbox Zero

Especially Inbox Zero should be explained just a little: for years I “faked” Inbox Zero. Having it at zero became more important than getting things done. I checked my inbox every 5 minutes. As I was sloppy in doing it “correctly” (i.e. decide if an action is needed etc) and I had various confused ways of acting on it, it usually ended in me not doing the things, but still deleting/hiding/archiving in the inbox. You can get away with that for some time, but for me reality caught up, and I needed to sort it out.

I think the most important thing with any of these productivity tips you read online is: you have to find what works for you, not because someone else says so, but because you know yourself. As [Merlin Mann][10] (a.k.a "Mr Inbox Zero") so perfectly put it:

> “It’s about how to reclaim your email, your atten­tion, and your life. That “zero?” It’s not how many mes­sages are in your inbox–it’s how much of your own brain is in that inbox. Especially when you don’t want it to be. That’s it.” – Merlin Mann

Finally, using services like [IFTTT][5], [Zapier][6], [Automator][7] (macOS) and [Shortcuts][8] (iOS) can do wonders in automating boring tasks that would only eat up your time further if you were to do them manually. Use those tools where you can, so you can free up time to getting things done.

As mentioned above, this is one part of my overall workflow, but it is a very important part as it keeps the rest of it together. I will do a write-up of the other bits and pieces too, if nothing else for the purpose of documenting it for myself.


[1]: http://www.torgersons.com/detailed-routine-using-evernote-for-tasks-and-tracking/
[2]: https://thesecretweapon.org/
[3]: https://gettingthingsdone.com/
[4]: https://evernote.com/products/scannable/
[5]: https://ifttt.com/
[6]: https://zapier.com/home
[7]: http://www.documentsnap.com/evernote-mac-import-folder/
[8]: https://support.apple.com/en-us/HT208309
[9]: https://www.urbandictionary.com/define.php?term=bacon&page=7
[10]: http://www.43folders.com/izero
[11]: https://evernote.com/