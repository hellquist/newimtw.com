---
title: 'Using Yubikey'
media_order: 'a_szmit85cv84.jpg,yubikey.png'
date: '2017-04-25 21:06'
taxonomy:
    tag:
        - security
        - yubikey
        - 2fa
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /security/using-yubikey
hero_classes: 'text-light parallax overlay-dark-gradient'
hero_image: a_szmit85cv84.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: website
---

Following the [article on LastPass password manager](../use-a-password-manager/) I mentioned I am using Two Factor Authentication in a couple of ways. One of those ways is via a physical device called [Yubikey](https://www.yubico.com/product/yubikey-4-series/#yubikey-4), which I have on my physical key chain at all times.

===

[![Yubikey web site](yubikey.png?classes=caption,caption-right,figure-right "Yubikey web site")](https://www.yubico.com/product/yubikey-4-series/#yubikey-4)

Yubikey is a hardware USB “dongle” that I plug in to my computer on an empty USB slot. It is really quite simple: I have paired it with my LastPass account and the Yubikey contains an encrypted string. It does not need to be installed on your computer. It just works.

You will only be allowed to log in to LastPass as me if you have the combination of my username, my password and the encrypted string contained on my physical Yubikey. As the only way to provide the encrypted string is to get it off the Yubikey, my login to LastPass is now protected not only by my excellent username, my amazing and complex password, but you’d also need a piece of hardware, the Yubikey which, hopefully, only I have access to.

## Yubikey is useless by itself

However, and this is important, the Yubikey by itself is useless. I could give it away, or lose it in a taxi, forget it in a café etc, and I will still be safe. It is only the combination of the three things, at the same time, that enables anyone to log in as me. As you can imagine, if I were to lose my Yubikey I would be even more tight lipped about my LastPass password and username than I already am, whilst I order another Yubikey and remove the old one from LastPass.

But don’t just take my word for it. Notable Yubikey users include companies like Facebook, Google, SalesForce, CERN etc. Like all really clever solutions and ideas Yubikey is really simple and…it just works. With proper Two Factor Authentication like this you will have massively decreased the risk of being hacked, or that your passwords end up in the wrong hands.

Even if LastPass would have a security breach, the hackers can do nothing with your data, unless you also give them your username, your password and the encrypted string on your Yubikey, as all three of those are required to decrypt your data stored with LastPass. Having said that, it is always wise to change the password for a service when you hear about a security breach. If you have followed my advice from the previous article you have let LastPass randomise a password for you, so you just let it randomise a new one and you can sleep well again at night, knowing your data/details are safe.

I got myself two keys whilst I was at it, and I keep them on my two separate key chains (work and private) to ensure I never end up in a situation where I can’t log in, which would be rather annoying.