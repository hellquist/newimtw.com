---
title: 'Alternatives to Lightroom and Photoshop'
media_order: autumn_field.jpg
published: true
date: '2020-11-09 21:00'
taxonomy:
    category:
        - photography
    tag:
        - photoshop
        - lightroom
        - photography
hide_git_sync_repo_link: false
body_classes: header-transparent
aura:
    pagetype: article
    description: 'I was looking for alternatives to Photoshop and Lightroom. I didn''t find any.'
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: autumn_field.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: autumn_field.jpg
summary:
    enabled: '1'
    delimiter: '==='
hide_from_post_list: false
feed:
    limit: 10
maintitle: 'Alternatives to'
subtitle: '<b>Lightroom</b> and <b>Photoshop</b>'
metadata:
    description: 'I was looking for alternatives to Photoshop and Lightroom. I didn''t find any.'
    'og:url': 'https://imakethingswork.com/blog/alternatives-to-lightroom-and-photoshop'
    'og:type': article
    'og:title': 'Alternatives to Lightroom and Photoshop | I Make Things Work'
    'og:description': 'I was looking for alternatives to Photoshop and Lightroom. I didn''t find any.'
    'og:image': 'https://imakethingswork.com/blog/alternatives-to-lightroom-and-photoshop/autumn_field.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '850'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Alternatives to Lightroom and Photoshop | I Make Things Work'
    'twitter:description': 'I was looking for alternatives to Photoshop and Lightroom. I didn''t find any.'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/alternatives-to-lightroom-and-photoshop/autumn_field.jpg'
    'article:published_time': '2020-11-09T21:00:00+01:00'
    'article:modified_time': '2020-11-09T21:35:37+01:00'
    'article:author': 'I Make Things Work'
---

Right, so this will be long, and it will all be about photography/photo-editing software. If that is not your thing, feel free to skip. :)

===

In an autumn clean-up of various service/software subscriptions, where I try to clean out the stuff I pay for but don't actually use, I went onwards and had a look at subscriptions I _do_ pay for and actually use. The [Adobe Photography package][15] is such a subscription. It is actually quite aggressively priced in that you get both Lightroom and Photoshop for about $10/month (I guess there are slight local variations).

However, turning that on its head, that is also $120/year. I've paid for it since 2013 by the looks of my billing history. That is $120 x 7 years = $840 so far. I got curious on what the possible alternatives are today. At the time there wasn't any serious competitor to Photoshop, and I got Lightroom "for free".

For those who don't know, Photoshop and Lightroom solve different problems. I do need access to both their sets of capabilities. I don't open Photoshop every time I edit my photos, but when I do open it, it is because I truly need that fine-grained detailed control that it gives, or the ability to work in layers. Doing slight dodge/burn of eyes for example would be a pain to try to do in Lightroom, but is comparatively easy in Photoshop as long as you have the time.

On the other hand, Lightroom handles my photo collection. I can tag things and keep them in order, search for specific tags, and quickly sift through photos after a shoot to quickly discard the duds and to mark the good ones for further editing (probably in Photoshop).

7 years is an eternity in software though, so I figured there might be other/new tools out there these days, but I realised I didn't know what they were and if they were any good. Being me, I obviously had to try as many of them as possible. If I were to find a replacement for **both** Lightroom and Photoshop, I could possibly ditch my subscription and move on to other tools, if nothing else because new shiny toys are...umm...shiny.

Just replacing one of them wouldn't be good enough, as that would still leave me with the current subscription, only that I would have bought more software and have gotten myself even further dependent on whichever of Photoshop/Lightroom that I hadn't managed to replace. So how did I do?

Not very well.

I am now deeper entrenched in Adobes Photography suite than I've ever been before. However, I love it more than I have ever done before. Also, I can honestly say that I gave the competition a run for it, having deep-tested most of the other software out there for their full trial periods, trying to solve real and actual photo-editing problems I had, without "cheating" by doing it in Photoshop instead.

I found a couple of new tools to add to my arsenal of photo editing as well. There is however no point in having several pieces of software doing the exact same thing, and as no combination I tested could topple the Photoshop/Lightroom combo I have only added new tools that enhance them.

Perhaps most importantly: I got to brush up my old Photoshop skills (I've been using it on/off since Photoshop 3 back in the -90's, when they introduced layers, and I swapped from Paint Shop Pro) as well as learn a whole host of new tricks. I should perhaps stress "new to me", as some of the things I've learnt over the last couple of months are old-school photography techniques that "real" photographers have used for years. Some I've been vaguely aware of, and the theory behind them. Some were completely new to me.

One of the reasons I wanted to "skill up" my Photoshop skills was also to learn which plugins I would actually need and benefit from, and which ones that I could actually get the same results if I took the time to learn how it _should_ be done. Also, Photoshop really has added some great new features since I last took a deep-dive in it. This made it possible for me to compare various techniques for solving the same problem, and to be able to pick the best one.

In my case the end result meant staying with Photoshop/Lightroom, though adding two plugins and a couple of "panels", mainly for the speed/convenience of them.

### Trying out the competition

Some of the software I've tested lately includes (but is not limited to, as I probably am forgetting something): [DxO Photolab][4] (mainly v3 though v4 was released in the middle of my testing), [Luminar 4][13] (affiliate link actually, my first ever!), [Capture NX][7], [Exposure][8], all the [Topaz plugins][1] (most notably DeNoise AI, Sharpen AI, Mask AI and Studio 2), [DxO Nik Collection][4], [Affinity Photo][5], [Raw Therepee][9] and...umm, that might be it!

Apart from owning Affinity Photo on iOS already (and it's awesome there too), and having tried Topaz Mask 5 (or something) some time ago, all of these were new to me. I also read about [ON1][2] and [Capture One][3] before deciding to wait a bit with them as they would be _more_ expensive, all things considered, than what I already have, not less.

On that note though, I noticed that many of the software developers these days in reality are doing "subscription-like" pricing (with the obvious/notable exception of RawTherapee, which is free regardless of how you measure "free", and Capture NX also is free though I guess it requires a Nikon camera, which I have). Basically, the trend seem to be you pay between $40-150/year to get updates after the first year, counted from your purchase. Also, many of them have various types of "memberships" (that usually cost another $50-ish/year) which gives access to downloadable, often for a price of course, content such as "looks" or "skies" or "presets".

I guess the economy of software and living off that initial and only sale has caught up with many of these companies as they start to realise the market, after all, isn't eternal, but the other way around, quite limited and with lots of competition.

Me, already being on the Adobe subscription which, as a reminder, is $120/year and I get all the latest versions automatically, I can totally understand that the other companies wish to do something similar, for business reasons. Also, for users, if the only way a company can make money is by selling one piece of software once to each user, said companys incentive to keep developing it, refining it and adding more features to it, becomes more and more limited, all whilst their source of income will slowly decline when their target audience have already purchased it once.

If you on the other hand know you have a recurring set of users that actively use (and pay for) the software it makes sense to do improvements, and to pace development and re-investment according to how many are subscribing to it, as that will set your available budget, from which you have to pay the actual developers, the rents and ideally also have enough for some profit.

Adobes aggressive pricing means it is hard to compete with though. Most software subscriptions and/or fees from their competition simply can't deliver the same amount of value for such a low price.

### Quick thoughts of "the rest"

So, this is not a full-on review on any of these tools, but I can give a couple of fly-by comments on them:

* Most impressive file handling came in Exposure. It is ultrafast and has all the tagging capabilities that Lightroom has, without having to import all your photos into a "collection" or "library". It basically just reads your files from your harddrive. Sadly Exposure don't trigger external software with whatever Photoshop triggers them with, so even though it is completely possible to open external/other apps from within Exposure, they most often start off in the state where you left them, and not necessarily with the image you were working with in Exposure, which is a bummer and slows down workflow a lot.

* Affinity Photo is amazing. It is the one piece of software that could rival Photoshop in a feature-by-feature comparison. Sadly plug-ins only work in theory. In reality, not so much. Also, the best way of adding plug-ins to Affinity Photo is by pointing it to the plug-in folder of Photoshop, which kind of negates the possibility of uninstalling Photoshop to use Affinity Photo instead, and if I already have both installed I will quite likely pick the one with working plug-ins and that I've used for years on end.

* The file handling of Luminar, Photolab, Capture NX and RawTherapee can not compete with the file handling in Lightroom. This is my subjective view of the general feel of it, but also it is down to ability to tag etc, apart from most of them feeling sluggish to use. They all look good on the product pages, but in reality they didn't "feel" fast/responsive/reliable. Only Exposure did this, and it (to my surprise) did it even better/faster than Lightroom.

* The biggest positive surprise to me came from Luminar 4 and its ability, used as a plug-in, to add to pretty much any and all photos I passed through it. At first it felt too...umm...toy-ish (it is all what at first appears to be simple sliders), but it actually is really well done. I ended up purchasing it, and all the links to it in this post are indeed affiliate links, which means I'll get a kick-back if you end up purchasing it too when coming from a link here.

* I was also quite (surprised and) impressed by Topaz Studio 2, which I had downloaded to test mainly because it was right next to the plug-ins I _actually_ was there for (DeNoise AI and Sharpen AI). Studio 2 has "light" versions of several of the Topaz plugins, but it is also really easy/quick to create your own settings, save them and recall them later. I often ended up using as a "Photoshop light" from within Lightroom.

* I was surprised to see that Capture NX and DxO Photolab appear to be sharing a (software) design paradigm/GUI for doing detailed spot changes. Both have the same annoying flaw though: I can only pull out the area as a perfect circle on a given spot. Big circle. Small circle. But no other shape/form. Making an oval would have been nice for example, as faces most often are oval, and adding some exposure to a face in an otherwise too dark image can improve it quickly...provided I can apply that exposure addition on the face, only, not as a halo around the head.

* DeNoise AI got a lot of use. That was expected as it was one of the embryonic sub-reasons I started off these tests in the first place: I had a bunch of grainy/noisy images that I wished to fix. I ended up purchasing it too (no affiliate links though).

* Sharpen AI was also surprisingly good. Even though I have learnt firstly to sharpen in Photoshop "native", as well as having a couple of new Photoshop panels that help with sharpening, I used it now and then anyways, to compare results. It often came out really well. Topaz have "bundles", which can be a good way to save some money. They even have a "complete my bundle" function, which perhaps can be useful to some. Annoyingly they appear to have compiled said bundles "wrong" though, in my humble opinion. For me, owning DeNoise AI and wishing to own Sharpen AI and Studio 2, there is absolutely no sensible way of doing that via bundles, even though the actual prices of the bundled software could be swapped 1:1 and be a match in price. I guess they wish to increase sales of the software that doesn't sell otherwise, and they get added to bundles to get them out there, at all...?


As mentioned above, doing all of this testing I also managed to brush up my old Photoshop skills, and also add a plethora of new Photoshop skills to my arsenal. This meant learning the "how" of sharpening, noise reductions, focal points, de-haloing, de-fringing etc without any other tools than Photoshop. I needed to do that to see where a plugin actually is adding value to my tools, or where they are not needed (and I therefore can spend my money elsewhere). It was great fun, and very educational. As I had hoped, it also made it possible for me to discard a handful of plugins that I no longer need, though plugins still can be a nice/quick way of achieving results without having to spend hours on a photo.

This left me with the following conclusion (and software):

### Conclusion

* Lightroom stays. I didn't find anything to replace it with. No, really.
* Photoshop stays. Though Affinity photo was close it falls off when it comes to my general workflow and also (as outlined below) for plugins/panels.
* I bought [Luminar 4][13] (affiliate link again). Great piece of software. I don't "need" it, but it does good things, and it does it really well, to almost all my photos. Used as a plugin from Photoshop.
* I bought [Topaz Denoise AI][16]. Even though I've learnt ways of doing noise reduction without plugins, they were all quite cumbersome and time consuming. On the other hand Denoise AI is really good, and manages to denoise photos a lot quicker than I do manually. Used as a plugin from both Photoshop and Lightroom (depending on where I need it, which depends on what else I wish to do to the photo).
* The absolute biggest change, and one that I haven't mentioned in the slightest above, is the addition of [TK7 panel][14], to Photoshop. During all my years of using Photoshop I have had its GUI laid out pretty much the same. Never before have I installed/used something that makes me change my entire Photoshop GUI...until TK7 panel. It has edged itself in there on importance level right with "Layers" and "Channels" and the general toolbar as always-open-windows. It is basically a luminocity panel, where you easily can select and mask various parts of the image based on luminocity (brighness) levels, colours and a whole host of other things. It does it so well that it has changed how I use Photoshop completely. I will probably detail it further in a separate article here somewhere.
* Speaking of "detail", I also got the following (Photoshop) panels: [Double USM][11], [Detailizer][12] and [ALCE][10]. They all provide help in bringing contrast/clarity/details in to photos. You never use more than one of them, but having the 3 different ones you can usually find the best tool for that particular photo, which might not be the best one for the next photo etc. Also, they give more control than regular sharpening filters (which in my case includes doing unsharp masks etc). Combined with TK7 masking in particular, you can quickly get some great and extremely targeted results.
* Also, props to Adobe for giving Photoshop new/great features since I last had a deep-dive. Not only has many of the built-in filters been given great updates, some new ones have been added over the years etc, but another feature that is very powerful indeed is BlendIf on the layers. Amazingly good for introducing adjustments in a controlled way from the layer above/below.

So, there you have it. I didn't manage to replace **both** Photoshop and Lightroom, which was what I set out to investigate if I could, and that has meant I have dug in to Adoble-land even further than ever before, but am happy with knowing I have the best tools available.




<!-- 
Image variables
![Allsides - Example](allsides_big2.png?resize=500,500&classes=caption,caption-right,figure-right "Allsides - Example")
-->

[1]: https://topazlabs.com/
[2]: https://www.on1.com/
[3]: https://www.captureone.com
[4]: https://www.dxo.com/
[5]: https://affinity.serif.com/en-us/photo/
[6]: https://skylum.com/about
[7]: https://downloadcenter.nikonimglib.com/en/download/sw/175.html
[8]: https://exposure.software/
[9]: https://rawtherapee.com/
[10]: https://cc-extensions.com/products/alce/
[11]: https://cc-extensions.com/products/doubleusm/
[12]: https://cc-extensions.com/products/detailizer3/
[13]: https://skylum.grsm.io/ntdbajsubx9914
[14]: https://goodlight.us/writing/actionspanelv7/tk7.html
[15]: https://www.adobe.com/products/photoshop-lightroom/compare-plans.html#
[16]: https://topazlabs.com/denoise-ai-2/