---
title: 'Browse in privacy'
maintitle: 'Browse in <b>privacy</b>'
media_order: '111.jpg,dance.jpg,google.jpg,price.jpg,spider.jpg,vpn.jpg'
date: '2019-03-21 20:47'
taxonomy:
    tag:
        - privacy
        - browser
        - search
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/browse-in-privacy
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: 111.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

If you are browsing the Internet, as you currently are (unless you consume this content via RSS), you will be using a web browser, either in your mobile device or in your laptop/desktop. Most often that web browser has come with the operating system.

===

This in turn means that for PC's Internet Explorer, or [Edge][1] as they called the new version, is your default browser. If you are on a Mac you will have [Safari][2] somewhere on your system.

Perhaps you have reasons to not use either of them and instead have opted to use [Google Chrome][3]. Regardless, the mentioned browsers (so far) are also of course made by Microsoft, Apple and Google respectively, who all have, or try to have, their fingers in pretty much all your pies.

A web browser can report back to its maker, and/or the operating system you are using. The web browser also keeps track of pretty much anything and everything you do online within that browser. Browsers like Chrome also have a login so you can connect your Google account to that specific instance of your browser, which can be very convenient. For example will that allow you to synchronize all kinds of things such as add-ons/plug-ins, browsing history, bookmarks and even passwords.

![No price tag](price.jpg)

### Convenience without visible price tag

To be able to synchronize all those things it obviously has to send that information back to the mothership first, so it is available when you open your Chrome browser on for example your iPad or your Android phone. In the meantime Google can, and will, scan it. It will add it to your profile they have on you, which also contains your complete search history on [Google search][4] (their very first product and the by far most used service online to this day). It will also help Google to work out what you are up to, based on what you have done, and quite likely add a sprinkling of its other knowledge about you, especially if you are using [YouTube][5] or [Gmail][6] etc already, where they already scan your content/history etc.

Also consider that a large chunk of Googles income is based on their web tracking service [Google Analytics][7], which basically serve web site owners with statistics over what users that visit their sites. Quite handy if you are a web site owner, but even more handy for Google, who get people to voluntarily put tracking codes into their web sites, to report data back to Google. It will show the web site owners the statistics alright, but it will *also* teach Google "who" goes "where", "when" and "why".

![You can dance](dance.jpg)

### Profiling for ads

Your profile will then be assigned to various "demographics" which form the basis of how Googles advertising work. All the data above is part of the reason why Google can help those who pay them to target *"17-year olds in suburbs in middle europe, still living at home in a middle-income family where the father drives a BMW and the mother drives a Nissan"* or why not *"Divorced males between 35-50 who live in Arizona and work in manufacturing and doesn't own their own house, have shared custody of their kids, and also visits various dating sites quite regularly"*. They know this type of stuff. No, truly, they do.

As they also will be able to deduct where people live and what they work with it isn't overly complicated for them to work out if someone is going on a holiday, or actually haven't been away on a holiday for a while but would like to go on one. They also know where people work. If you are heading out at 19.00 (that is 7PM for my US friends) Google probably can give a pretty decent guess at where you are headed, either based on previous patterns or the lack thereof...because that is an often forgotten part of "intelligence" (not the RPG stat, but in the "we know stuff" kinda way), non-patterns can be predictable in themselves, especially if you can see the fuller picture/profile. This is only tricky if the person in question isn't an active user of all their services, because if they are they could probably just look at keywords in e-mails saying *"table booking confirmation"* or *"tickets for hockey game"* etc to know exactly where you will be heading.

By knowing where people are and/or where they are heading, based on what they've previously done, they can then adapt advertising messages in the services you use, to become more relevant and, hopefully to them, more enticing to you. You'll buy stuff, influenced by their cleverly placed ads, as it all of a sudden makes sense to you. They could also figure out if you always go to the same place on Fridays by 20.00 (that'd be 8PM), that it might be one of your routines of course, and they probably have a pretty good idea of what that routine is.

![Google everywhere](google.jpg)

### So what can we do about it?

Do you value the convenience of cross-platform sync more than you value your privacy? I know that the Google suite of services, with search, mail, calendars, browsers etc can be very convenient indeed, I have used all of it myself for a long time. I am now looking at what alternatives I have.

The simplest thing is to install extensions/add-ons/plug-ins to your existing browser. That might still miss the important parts though. The second simplest thing is to swap out the web browser(s) and search engine. There are lots of privacy concious alternatives out there, for pretty much all the platforms.

### Other alternatives

One of the "good ol' trustys" is of course [Firefox][8]. It isn't owned/manufactured by any of the mentioned companies, so there is no particular love from them to the operating system. Firefox also has settings for turning on/off various things that will all help you tighten up your privacy when you are browsing around on the Internet. Unfortunately most of those settings have a default to "off", which means you have to [find them yourself][9] and turn them "on".

On the mobile platform Firefox has recently released a privacy focused version of their browser called [Firefox Focus][10]. Be aware that it too (as many other browsers) use Google Search as the default search engine though, but it has a setting that lets you use [DuckDuckGo][11] (I'll get to that later) as the default instead. When you close your Firefox Focus it will delete all browsing history too, so when you start it again it will start "a-fresh".

On the topic of [DuckDuckGo][11]: it is basically a search engine that isn't made by any of the big companies. They also have this as their stated goal: *"The search engine that doesn't track you."* and they mean it. It has become a favourite tool amongst people who are concerned about privacy, data leakage and traceability. One important thing to remember with various browsers "incognito" modes: the search engines will *still* track you when you are incognito. Except for DuckDuckGo.

For your desktop browsers it is possible to add (and replace) your current search engine with DuckDuckGo. For mobile devices though DuckDuckGo has released their own web browser, which has all the privacy options turned "on" by default, and also of course uses DuckDuckGo as the default search engine.

Another alternative could be to use [Brave browser][12]. It is a fairly new company but was started by previous Firefox employees. Basically it has all the security/privacy options turned "on" as a default, so in that one you will have to turn them "off" if you think they are hindering the functionality of a particular web site you regularly use (which can happen as some of the privacy options means adjusting how the browser handles javascript, and from what sources). One downside, to some, is that Brave doesn't have capability to install extensions/add-ons/plug-ins. From a privacy/security perspective that is however a good thing, as the user can't introduce new "flaws" in to the system. Also I suspect Brave will add extensions in due time.

!!! **EDIT 2019-08-28:** It has been brought to my attention that Brave browser has some weird funding funnels going towards extremist groups. Also, some of the former Firefox employees that now are associated with Brave browser have <a href="https://threadreaderapp.com/thread/1159040790154399745.html">been in the press</a> for not-so-flattering issues. Your call on if you wish to support it or not. I will be using other alternatives.

![Working towards the same goal](spider.jpg)

### Add-ons

Yet another alternative is [Ghostery][13], which both has addons to your current desktop browser(s) and mobile versions of their own browser. They also have their own privacy focused search functionality.

Speaking of add-ons to your existing browser, if that browser is Safari you can install [Better Blocker][14] which adds Ghostery type of functionality to Safari. Better Blocker is made by [ind.ie][15] which is a small company that is fighting the battle of open-source, privacy focused, distributed/federated products and design in many/all areas. I have seen both founders of ind.ie at several conferences over the years, especially during the 11 years I lived in London, and I can tell you they are really passionate about what they do, which ultimately is to change the world for the better. A lot of my [information regarding Mastodon][16] comes from them too. More on that later.

Anyways, on my mobile devices I now use a combination of [Brave Browser][12], [DuckDuckGo][11] and [Firefox Focus][10]. On my desktop I am mainly using [Vivaldi][17] browser.

Vivaldi is a company founded by the old founder of [Opera browser][18]. Vivaldi has all the security/privacy options in it too. Some people dislike that it isn't completely open-source and some others dislike that a/any user will get an ID that Vivaldi is using to count how many unique users they have. Vivaldi have promised it isn't used for anything else.

Regardless, Vivaldi isn't made by any of the giants and it is a really nice browser that also runs on the same "rendering engine" as Chrome, just without any of the Google bits for tracking etc. It also gives me the convenience to sync bookmarks and tab content between my various desktops, which I still find extremely handy (as I use all operating systems on many machines), whilst also giving me the ability to install extensions/add-ons/plug-ins.

![VPN](vpn.jpg)

### ...and there is even more

One shouldn't talk about privacy, browsers and Internet without mentioning two other possible solutions as well, as they aren't mutually exclusive, but indeed can be added to any of the above: [VPN][19]'s and the [Tor network][20]. I will have to cover them in another post though, this is already dragging on a bit. I should possibly also remind people of [YubNub][21] which is an excellent "command line for the web" as they call it, and which I use in conjunction with DuckDuckGo all the time. But that is for another day.


[1]: https://www.microsoft.com/en-us/windows/microsoft-edge
[2]: https://www.apple.com/safari/
[3]: https://www.google.com/chrome/
[4]: https://www.google.com/
[5]: https://www.youtube.com/
[6]: https://gmail.com
[7]: https://www.google.com/analytics
[8]: https://www.mozilla.org/en-US/firefox/new/
[9]: https://www.howtogeek.com/102032/how-to-optimize-mozilla-firefox-for-maximum-privacy/
[10]: https://www.mozilla.org/en-US/firefox/mobile/
[11]: https://duckduckgo.com/
[12]: https://brave.com/
[13]: https://www.ghostery.com/
[14]: https://better.fyi/
[15]: https://ind.ie/
[16]: https://laurakalbag.com/what-is-mastodon-and-why-should-i-use-it/
[17]: https://vivaldi.com/
[18]: https://www.opera.com/
[19]: https://en.wikipedia.org/wiki/Virtual_private_network
[20]: https://en.wikipedia.org/wiki/Tor_(anonymity_network)
[21]: https://yubnub.org/