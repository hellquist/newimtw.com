---
title: 'Non-tracking alternatives'
maintitle: 'Non-tracking alternatives'
subtitle: '<b>- Initial thoughts</b>'
media_order: photo.jpg
date: '2018-03-22 22:59'
taxonomy:
    tag:
        - privacy
        - mastodon
        - diaspora
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/non-tracking-alternatives-thoughts
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: photo.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

This is more of a follow-up post to the post where I outlined some alternatives to the big social media services, like Facebook, Instagram, Twitter etc. As I obviously had to refresh my view on them, and have been testing them more now, these are some of my initial thoughts.

===

### micro.blog

I quite liked the concept of it, and I also thought it wasn't a bad thing they had a payment plan (from $2-5/month) which meant you would be a consumer instead of being the actual product. However, I also noticed that there is no simple way to find friends. There is a tab saying "discover", which basically is a stream of everyones posts. You can of course search for friends by being able to guess their username. Then again, perhaps you guessed wrong and your friends could be forever next to you without you seeing them.

Speaking of friends, for me, being a new user testing things out, it is **pretty** lonely in there. Sure, when I press "discover" I see some others posts, and some of those guys I even recognize from work related events etc though I wouldn't call them friends (they would hardly know who I am for example).

As for actual usage there is a Mac app (which I've downloaded but haven't yet tested) and an iOS app (which I have downloaded and tested). Firstly, the functionality called "cross-post" can post your stuff from micro.blog to Facebook or Twitter. If you enable it, which you can during your 10 day free trial, it will post everything to the connected services. I would probably have preferred being able to pick and choose which post to share to what service, not "everything to everywhere". Perhaps that is just me.

Secondly, I have been spoilt by Flickr and Instagram, in that I can upload photos in whatever format my phone provide them in, including horisontal/portrait modes. However, micro.blog will only post a square. If your image is a larger non-square, it can and will crop the images. I honestly thought we were past the square image malarkey of old Instagram.

### diaspora

I actually only logged in to it, but didn't do anything more. Part of the reason for that is that there is no mobile app, which is funny as I noticed previously that I was just a tad annoyed by Vero not having a web interface, only an app, but true enough, when out and about, I want to be able to check things in an app. To their defence I haven't actually tried opening up diaspora in a browser on my phone, perhaps an app isn't needed then, and they have thought/executed it all awesomely well. Right now I just don't know.

### Eyeem

So I'm doing a 365 this year, which means I take one image each day and share it somewhere (but not necessarily everywhere). It is mainly a creative challenge, especially on days when you feel absolutely no creativity. Todays image was alright, so I had to upload to all the new services that I have now connected, and the old ones I haven't migrated away from just yet.

That also gave me opportunity to add an image to Eyeem, to try out their market. After uploading my image (which is in horizontal format) I clicked the button "sell" and was presented with a form where I had to fill in my name and my home address. No idea what happens after that, but I was quite impressed by the auto-tagging they provided, which was spot on.

Eyeem also has a button each for Twitter/Facebook for the event I would like to share my photo to those services too.

### Vero

I uploaded my daily picture to Vero as well. It went well, and it took the image in its horizontal mode. For everything you share in Vero you also have to pick which group you would like to share with: Acquantances, Friends, Family or Private (pretty much exactly like Flickr). I picked "everyone" as Vero, like a few of the other options here, although good, feels quite empty. I have like 4 friends on there. The last update from any of them was weeks ago. As slick and nice as Vero is, it simply feels lonely.

### Mastodon

This is the alternative service that "*feels*" the best for me, which is a *very* subjective opinion. I have mainly used it via the Amaroq iOS app, but most things are there: working photo upload, ability to write longer (than 140 chars) posts etc, you can search for users by their username OR their real name (if they have provided it of course). The only thing that is lacking to make it a raging success is...**drum roll**...users. More of them. Well, at least more of **my** friends, there seems to be random-other-people there enough. I guess it is about spreading the word more.

On practical usage notes: I can't share my post to Twitter/Facebook natively, which means if I would like to transition over to Mastodon I have to double post to multiple services for quite a while. I can of course share the post via the built-in share functionality of my phone though. However, better integration with especially Twitter would be great for Mastodon. Things like showing tweets from Twitter in their full glory when you paste in the link would help people do the transition a lot quicker.

In Amaroq I am logged in, but if I want to see/edit things that are related to my Mastodon account (but not related to the app) it basically presents me with a new login web window where I have to log in again to manage my account.

### Other noteworthy points

None of these services are, today, possible to connect to services like ifttt.com etc, which otherwise could help out bridge some gaps in functionality. Already today I use ifttt recipes when I upload images to Instagram, ever since they (instagram) removed the integration to Flickr, so now I let ifttt take my Instagram uploads and add them to Flickr as well, fully automated process. I say this as I today have had to upload my daily photo to 5 different services. Ideally I should have uploaded it once, to one place, and been able to pick which service I would like to share it to **as well**.

The above is obviously true for more things too, and a thing that has annoyed me at work of late too: we basically have information overload, where it is unclear which of my colleagues that use which service. Some are on Skype (only), others are on Slack (almost only), a few are on Yammer, some are trying the capabilities of Office 365 which has just been rolled out. Sharing files and having discussions is now really quite difficult. On a private level I also have several of those services along with a couple of forums and a handful Facebook groups to keep track of. And mail of course. 15 mail accounts in my case.

I am longing for a future vision described by François Téchené in his article "[Design report #4: symbiotic applications][1]", where each task (photo, message, mail, to-do, calendar etc) has one input area/app/software that then can integrate with all others. If you get a message you know what app to use to read it, and not like today where you accidentally unlock your phone (merely by holding it in front of your face, or touching it) and you have no idea if that message you managed to catch a glimpse of was an alert from the SMS app, Skype, Skype for Business, Facebook, Instagram, Facebook Messenger, Signal, Line, Slack, WhatsApp, Hangouts or an actual email (all these are example from my own phone, and I've actually left some out), so you have to open them all just to ensure you haven't missed anything. Two hours later you can go back to what you were doing before then, if you can remember what that was.

### Final thoughts

So, typing this article is also part of the experiment. Now we shall see if it gets picked up by micro.blog, and if that in turn posts a link to it on Facebook and Twitter. :)

[1]: https://puri.sm/posts/librem5-progress-report-8/