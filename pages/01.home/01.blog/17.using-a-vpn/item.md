---
title: 'Using a VPN'
media_order: 102017_b.jpg
date: '2019-04-26 21:16'
taxonomy:
    tag:
        - privacy
        - vpn
        - tor
hide_git_sync_repo_link: false
visible: false
routes:
    aliases:
        - /privacy/using-a-vpn
hero_classes: 'text-light parallax overlay-dark-gradient'
hero_image: 102017_b.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

In a [previous post][1] I mentioned I would get back to [VPN][5]'s and also [Tor][2]. Though this post will not cover Tor or the "[Onion network][3]" to any length I have now decided on trying out a new VPN provider for my computers and mobile devices.

===

It is a quick thing to write (I just did), but anyone who has tried to work out what VPN service that is the best one in general and the best one for you in particular, will know that there's a whole djungle out there with various VPN providers all claiming to be the "best", "fastest", "most secure" etc.

!!! Please note that this, and most other articles on this site, contains links. If they have a little arrow next to them it indicates that the link is external, i.e the contents of the destination site is out of my control. Also rest assured that **none of the links are "affiliate" links**. I do not get paid by anyone to link to anything in particular. I do like to add links though so you can see/investigate things for yourself, to make up your own mind.

### Hide My Ass

I have been using a couple of VPN services previously. I started off with [HMA][6] (which stands for Hide My Ass...no, seriously...check the URL) but I stopped using them after a couple of incidents (abroad) where they had handed over logfiles and "evidence" to the US government. That you might think is all fine and well, but for me that means they also *keep* logfiles and "evidence" in the first place. If there were to be hacked you could, in theory, be traced even when connected via their VPN service. It should be noted the drive-by-mentioned incident occured 2014, so it might well have changed by now, but I decided to not renew my subscription and instead I went with [PureVPN][7] a couple of years ago.

### PureVPN

PureVPN, at the time, had a great sign-up deal which was really cheap, and this is another learning for anyone who investigates this segment: there are *always* "*extra special deals*" or "*right now, massive discount on long-term deals!*". Always. If you are shopping for VPN providers, give it a couple of weeks and what might have costed you an arm and a leg last month might well go for $3/month tomorrow. In fact, all of them currently seem to have an offer that lands at around $3/month if you sign up for 3-years.

PureVPN was great and they had a nice mobile iOS app too, which meant it was rather quick/simple/easy to connect to my VPN when I was out-and-about. It had a period of "can't connect, sorry" but they sorted it out and it was pretty stable and decently fast after that. Somehow though I managed to miss the reminders to renew the subscription, and I only noticed it had expired when it refused to connect. Looking through their offers I couldn't see an equivalent of the offer/priceplan I originally had signed up for (it was now significantly more expensive) so I figured it was time to shop around again for another VPN provider.

In all honesty PureVPN approached me after I had logged in and stared at my "please renew your subscription" on my account page on their site, and they asked me (via e-mail) what the reason was that I had left. I told them I honestly didn't know I had left until it was too late, but that I now couldn't find an offer that was as tempting as the one I had signed up for. They actually promised me I would get a very similar deal again, but I had already decided (mentally) on that it was time to try another provider, just for the sake of it.

### NordVPN

After almost going dizzy surfing around for reviews and opinions in my attempts to try to work out which VPN providers that were good today, 2019, I finally decided on trying out one that consistently over the last few years have gotten great reviews: [NordVPN][4]. They seemed to tick all the boxes when it comes to security, privacy, speed and also not keeping logs, so there isn't anything to hand over to any government anywhere. Further to that they also have functions for both shutting down the connection completely if the VPN is disturbed (for whatever reason) as well as for [connecting your VPN through the Onion Network][8]. Furthermore, they have a nice iOS app for my devices and it works on all platforms I use (which basically includes Windows, MacOS, Linux and iOS).

I shall be giving it a try (well, over the next 3 years, as that is what I signed up for) and I'll get back with reports on if I didn't like it for some reason.


[1]: /privacy/browse-in-privacy
[2]: https://www.torproject.org/
[3]: https://en.wikipedia.org/wiki/Tor_(anonymity_network)
[4]: https://nordvpn.com
[5]: https://en.wikipedia.org/wiki/Virtual_private_network
[6]: https://www.hidemyass.com
[7]: https://www.purevpn.com/
[8]: https://nordvpn.com/features/onion-over-vpn/