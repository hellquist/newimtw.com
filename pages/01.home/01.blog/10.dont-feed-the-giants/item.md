---
title: 'Don''t feed the giants'
maintitle: 'Don''t <b>feed</b> the giants'
media_order: '108496.png,fb_image.jpg,fb_image2.jpg'
date: '2019-03-18 23:19'
taxonomy:
    tag:
        - privacy
        - facebook
        - advertising
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/dont-feed-the-giants
hero_classes: 'text-light parallax overlay-dark-gradient hero-fullscreen title-h1h2'
hero_image: 108496.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
---

In a previous set of articles I wrote about how you can increase the security within your most used apps such as Facebook, Google, iCloud etc, to avoid getting hacked or having your account taken over by someone who shouldn't be posting as you, or have access to your personal details.

===

Therefore this post might come as a surprise to you, as it basically ends in *"do yourself a favour and please uninstall all those apps"*. But let's build up to that conclusion and not rush towards it as you might be confused by now.

### Facebook advertising

It has long been known that Facebook alters the advertising based on what you post, comment or like (i.e pressing the "thumbs up" icon). One early example of this for me was when a Facebook friend of mine (we are also friends in real life, but this took place on Facebook) was discussing, in Swedish, the ultimate timing for creating the perfectly boiled egg with a sous-vide, with a couple of his friends (who are not on my friends list as...umm...I don't know them).

After reading their comments on his original post, where timings such as *"I think 41 minutes creates the best egg for me, provided it is suspended perfectly"* etc, which I kind of found hilarious though fascinating, I entered the discussion with something along the lines of *"OR you could boil the egg on the stove, in water, spend a maximum of 10 minutes on it, and you will have gained about 31 minutes of your life that you could do something else on...?"* in my usual "hilarious" way.

A few more comments were added in an informative friendly banter way and I didn't think much more about it that day. I hadn't liked his original post, I had only commented on it, in Swedish.

The following weeks my Facebook advertising was 90% consisting of ads for either egg boiling machines or sous-vides. Facebook had watched. It had learnt that I apparently was surrounded by people who like their eggs and their sous-vide's, I had participated in the discussion and hence I would probably be likely to purchase such a machine myself. I noted the spooky correlation but I didn't do anything about it.

### There is more...

Fast forward a couple of weeks and I came across a [very interesting set of photos][1] on Facebook (and yes, sadly you will need to be logged in on Facebook to see that link), which basically was started off with a painting of Spider-woman that was extremely revealing, in a sexual pose. She was basically fully naked (though no pink colour was used). The set of photos, which contained screenshots of a discussion, however was highly critical to how the pose was arranged, how clothes **actually** work, who the drawing was for as well as a good coverage on how differently men and women depict women in cartoons. It was not applauding the sexual pose, it was critizising it, and explaining how it probably came to be in the first place. Facebook however missed that huge difference.

What Facebook assumed, by me re-sharing it, was that I wished for naked/sexual cartoons to not be less in my feed, but more. Now I appreciate beauty as much as the next guy, and I love watching beautiful people. I have never ever been into cartoon sex though. I haven't surfed it. I haven't searched for it. I have never installed apps that has it, etc. Yeah, some might find it incredible and unlikely, but hey, I haven't. If you like it, go for it, I'm not writing this to judge, I'm writing this because of what followed, which was...

![](fb_image.jpg?resize=220,480&classes=caption,caption-right,figure-right "Facebook advertising started to appear")

...yep you guessed it. My feed now quickly started filling up with with advertising for apps, games and web sites that contained nothing but cartoon ladies wishing to satisfy my every cartoon need. Kind of the opposite of what the original article had slammed down on, which I admittedly hadn't written myself, only re-shared.

After that I got advertising for apps that *"can only be played when my girlfriend isn't at home"* containing cartoon school girls who wish to follow me home, or "sexretarys" who wish to help me enjoy work **a lot** more, in quite graphical ways I have to assume. Here I should probably add that I haven't clicked or otherwise interacted with these ads, as I'm afraid I'll see no end to them then, and the same is/was true for the egg-boiler/sous-vide ads described above.

The point here is not that I'm critizing the advertisers themselves or their products. What I'm after is the advertising algorithm Facebook has. How much it reads and "concludes" based on what I, or my friends, do and/or interact with. Or listens...because this weekend it took a new and very scary twist.

### But wait...there is even more!

A Sunday family breakfast with my entire family, and also my friends and their two kids, took a very interesting turn indeed. We were discussing kids clothes and how quickly kids grow out of them, how difficult it was to find fitting, suitable, durable and yet nice looking clothes in general and trousers in particular, for our kids. My phone was lying on the table, not being used. We discussed stores and how different both our two kids are compared to each other, and how different our friends, the other family, kids are compared to each other even though they are siblings etc.

I had to go to the toilet and grabbed my phone (as you do) and went there. I opened the Facebook app. The very first ad (which is in Swedish) was this:

![Swedish ad for kids clothes](fb_image2.jpg?classes=caption,caption-right,figure-right "Swedish Facebook ad for kids clothes")


I was shocked. I finished up what I was doing and went back to the table and told my friends about it, and the "amazing coincidence" of an ad appearing that was pretty much spot on the target of what we had discussed not less than 15 minutes earlier. We stared at each other and the ad in shock. Then I saw my mate had a glimmer in his eye and he all of a sudden started rambling about buying a car, upgrading a car, he went bonkers and just started mentioning car makes (though he doesn't actually need a new car). Volvo. Tesla. Passat. BMW. He said all the things he suspected could be key words (hybrid, electrical, diesel etc). We all joined in for a hilarious couple of nonsensical minutes of talking jibberish about cars, without actually saying anything that made sense.

10 minutes later he went up to his phone, which was lying on a bench in the kitchen, and opened his Facebook app. The very first ad was from a car dealership.

Our phones were listening. Facebook was listening. Facebook adjusted their advertising based on what we talked about.

Do note that all the examples above are my own first-hand experiences, not some hearsay I have read about anecdotally.

### So this means...

Yeah. This means Facebook is also listening when you are in the toilet. In the bedroom. When you dine. When you have a board meeting at work. When you discuss confidential things with your best friend. When you talk to your partner about your kids.

And honestly, if you aren't scared by now, consider this: if Facebook can do it, who else can? Surely the big guys like Apple, Google, Microsoft et al also can. Besides the fact that they already can track you via your device with harrowing precision. (Don't get me started on the fact that Huawei is owned by the Chinese state and all Huawei owners happily share all kinds of intelligence/data with China)

Those guys probably provide you with a least one e-mail account too. And you are using an operating system in your phone/device that is created by one of them for sure, which reports back your position, what apps you open, if any of your friends are at the same location etc. They probably manufacture the web browser you are using to read this very post.

And that is the big guys. Do you know and/or trust the manufacturers of all the other apps you have installed? Apparently it is possible to sneak listen. Even if they say they don't, can you trust them not to?

Now, before you dismiss the above completely, who knows, it could ALL be just massive coincidences or something, do give a few minutes of thought on *why* they would want this.

Is there any money to be made from knowing not only everything about you personally, but also who you socialize and/or work with, and how you move, what you think, what you do? Obviously. Of course. We are talking about invaluable data if you ever wish to sell something to someone. Here my friends you have the reason to why these very convenient apps/softwares, which require data centres with 1000's of servers and 10's of thousands of employees, are free. As they say, if the service/product is free, you are the product. You (and your data) and your privacy. You are being harvested. You are being sold. You are being traded.

When Facebook and the others are being hacked, this data is what the hackers are after. The hackers don't care if Facebook also has it, as long as they, the hackers, too have it. This is gold in an information driven world. [Cambridge Analytica scandal][2] anyone?

So...please do yourself a favour, and uninstall all those apps. What should you do next? I don't know. But I will be looking to ways to detach myself from them, and to use those services in a more sensible and securer way. I will be outlining my progress here, on this blog, so do follow this series of articles if you are interested in doing the same.

[1]: https://www.facebook.com/mathias.hellquist/posts/10157478580469903?__tn__=-R
[2]: https://www.theguardian.com/technology/2019/mar/17/the-cambridge-analytica-scandal-changed-the-world-but-it-didnt-change-facebook