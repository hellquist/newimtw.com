---
title: 'Checklist for Conspiracy Theorists'
media_order: tunnel_mixed_media.jpg
published: false 
date: '2020-11-17 14:45'
taxonomy:
    tag:
        - conspiracy
        - theories
hide_git_sync_repo_link: false
body_classes: header-transparent
aura:
    pagetype: article
    description: 'For all aspiring conspiracy theorists out there it can be hard to remember what you should and shouldn''t be doing. Here is a handy checklist with tips and tricks.'
    image: tunnel_mixed_media.jpg
hero_classes: 'overlay-dark text-light parallax'
hero_image: tunnel_mixed_media.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: tunnel_mixed_media.jpg
summary:
    delimiter: '==='
hide_from_post_list: false
feed:
    limit: 10
maintitle: 'Checklist for'
subtitle: '<b>Conspiracy</b> Theorists'
metadata:
    description: 'For all aspiring conspiracy theorists out there it can be hard to remember what you should and shouldn''t be doing. Here is a handy checklist with tips and tricks.'
    'og:url': 'https://imakethingswork.com/blog/checklist-for-conspiracy-theorists'
    'og:type': article
    'og:title': 'Checklist for Conspiracy Theorists | I Make Things Work'
    'og:description': 'For all aspiring conspiracy theorists out there it can be hard to remember what you should and shouldn''t be doing. Here is a handy checklist with tips and tricks.'
    'og:image': 'https://imakethingswork.com/blog/checklist-for-conspiracy-theorists/tunnel_mixed_media.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '854'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Checklist for Conspiracy Theorists | I Make Things Work'
    'twitter:description': 'For all aspiring conspiracy theorists out there it can be hard to remember what you should and shouldn''t be doing. Here is a handy checklist with tips and tricks.'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/checklist-for-conspiracy-theorists/tunnel_mixed_media.jpg'
    'article:published_time': '2020-11-17T14:45:00+01:00'
    'article:modified_time': '2020-11-17T14:44:24+01:00'
    'article:author': 'I Make Things Work'
---

For all aspiring conspiracy theorists out there it can be hard to remember what you should and shouldn't be doing. Here is a handy list with tips and tricks.

===

<br>**1.** Make sure you have a platform where you can be heard by as many as possible, but constantly remind people that no one dares to listen because they, unlike you, are all afraid of the truth as you describe it.

<br>**2.** You have done the thinking already, others need not bother, they only need to listen to you.

<br>**3.** Surround yourself with like-minded that applaud you and everything you say.

<br>**4.** Your true friends are the ones that agree with anything and everything you say. Where you lead them they shall nod, agree and go. It is not up to them to give alternative view or opinions, let alone facts. The true measure for a free-thinking intelligent and cool person is that they buy in to whatever you say (as you are the coolest) without objection.

<br>**5.** If someone offers a "reasonable" explanation, i.e. an explanation the rest of the world accepts, to what you just said, make sure you announce that he or she is "sadly" not up to par on "average and normal" intelligence levels.

<br>**6.** So called "educated people", and schools and educational institutions in general, lie and are controlled by the enemy.

<br>**7.** So called "experts" lie and are bought by the enemy. No expert experience or education (see above) can stand up to the clear YouTube evidence video you saw yesterday.

<br>**8.** So called "journalists" lie and are controlled by the enemy, most often by Soros himself directly, who tells them exactly what to write and/or say. Except in Russia. In Russia the honorable President Putin informs journalists what truth they should write about. As you have learnt sooo much from his news channels this is a good thing.

<br>**9.** All politicians are co-operating on the same evil plan, which is opposed to your TRUTH, except for your preferred candidate, who is fighting for justice and THE TRUTH, pretty much alone in the political world.

<br>**10.** Repeat to any critic of your truths that the critic themselves are braindead imbecills, and that they are embarrasing themselves (unlike your own cool and clever self), _especially_ if they ask for more information.

<br>**11.** Tell everyone who doesn't applaud you that they are SHEEP, ideally in CAPS, and that you feel sorry for them (but actually, you don't).

<br>**12.** If someone asks you to prove something, try to work out how that person is best insulted, and say _that_ instead, in as many ways and versions as you can. It'll make you appear really clever and cool to your true followers and fans.

<br>**13.** Tell people about THE TRUTH, but not the whole thing in one go. Hint to it here and there. The clever ones will understand.

<br>**14.** Make fun of the fact that people call you a conspiracy theorist. They wouldn't know the truth if it stared them in the face. If anything, you are merely helping them. You are the good guy here. They'll see, when the time comes.

<br>**15.** If someone brings facts to your "discussion" repeat whatever thing you said last. If they persist, just repeat what you said last. Again. And again. And again. They'll go away soon enough. It is very important to not meet up with what they just said/asked. Stand firm. This is not _actually_ a discussion, **you are telling people how it is**.

<br>**16.** There is absolutely no point in reading up on facts, facts can not stand up to your conviction of what the truth is. At least that is what the guys you yourself follow says, so it must be true.

<br>**17.** If someone is really annoying and leaving facts and articles that might contradict what you say, tell them you'll read it. The idiot probably will shut up and wait for you to come back, but you never will (as there is no point in reading it in the first place).

<br>**18.** There aren't many conspiracy theories, all of them are the same, they are just different angles of the same thing, part of a network, and logical results of each other.

<br>**19.** Besides, a conspiracy theory is only a conspiracy theory until it is proven true, which is what you are doing.

<br>**20.** Do not give away clues to how you know what you know. If they are stupid enough to not figure it out themselves they are not worthy.

<br>**21.** Do not EVER give out links or reading material, unless it is for material you have created yourself. If someone asks for more information they are clearly braindead imbecill SHEEP who doesn't deserve any information you could grace them with.

<br>**22.** Your critics are not "with you", they are quite clearly "actively against you", on a mission from the enemy.