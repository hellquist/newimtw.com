---
title: 'Dependency Confusion'
maintitle: Dependency
subtitle: '<b>Confusion</b>'
published: true
date: '2021-02-12 14:28'
taxonomy:
    tag:
        - code
        - frameworks
hide_git_sync_repo_link: false
body_classes: header-transparent
hero_classes: 'overlay-dark text-light parallax title-h1h2'
hero_image: code_soup.png
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: code_soup.png
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
    description: 'Installing one framework usually leads to installing a string of interdependent frameworks'
    image: code_soup.png
metadata:
    description: 'Installing one framework usually leads to installing a string of interdependent frameworks'
    'og:url': 'https://imakethingswork.com/blog/dependency-confusion'
    'og:type': article
    'og:title': 'Dependency Confusion | I Make Things Work'
    'og:description': 'Installing one framework usually leads to installing a string of interdependent frameworks'
    'og:image': 'https://imakethingswork.com/blog/dependency-confusion/code_soup.png'
    'og:image:type': image/png
    'og:image:width': '1200'
    'og:image:height': '692'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Dependency Confusion | I Make Things Work'
    'twitter:description': 'Installing one framework usually leads to installing a string of interdependent frameworks'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/blog/dependency-confusion/code_soup.png'
    'article:published_time': '2021-02-12T14:28:00+01:00'
    'article:modified_time': '2021-02-12T16:07:20+01:00'
    'article:author': 'I Make Things Work'
summary:
    enabled: '1'
    delimiter: '==='
---

To install A you need B which depends on C which can only be installed by D which...

===

Looking at installing a new "[theme][1]" for a blog. It's using [Tailwindcss][5], so heading over there to look at how to install it. This is my "log" over how it went.

It apparently needs [Node and Npm][2]. I have, at some point, installed both, so I try to find which version of Node I have and which I would need, by simply updating it. Turns out Node now needs [user accounts][3] even to use the free tier as it has gone commercial, so to be able to work out if I'm a paying user or not they need to know who I am, at all, first. I decide to wait a bit with registering. I most definitely do not wish to pay a monthly subscription for it, I just wish to check out a blog theme.

I'm being reminded that to update Node I need Nvm. Trying to work out what version of Nvm I have and which I would need, by simply updating it. Turns out the command isn't registered on my command line. Did I perhaps install it via Homebrew, "back in the day"? I can't remember. I do remember being as confused at the time as I am starting to feel right now.

Checking to see what version of Homebrew I have, and which one I would need, by updating it. It, surprisingly, goes well. I again try to update Nvm. It says it has updated but also that it is most definitely not registered as a command. It also has a note saying the way I installed it, via Homebrew, isn't guaranteed to work, and thus not supported, though "it should probably" work. Basically it means "if you get errors, which you shouldn't, we think, don't come to us for support".

Getting instructions on suggested configuration lines that I should add to to Zsh, which is what I use on my command line. I log out, log in again to ensure it reloads the amended config. It complains that the config file contains folder paths that are incorrect. Nvm is now accessible as a command though. Probably need to fix those paths though. Checking the paths, they do in fact exist. Permissions? Checking. Weirdly it all looks correct.

Logging out again.

Searching the Internets to verify how to best update macOS to latest Node. Hundreds of search results. I'm apparently not the only one confused here.

Turns out I can do it via Nvm, Npm and Homebrew, which is kind of what I knew, though if and how they are interconnected has always been a mystery to me (I'm clearly not "a javascript dude"). Right now it feels like I have installed and updated all three of Nvm, Npm and Homebrew once from each of the others. Probably "over-updating" something somewhere.

Logging in again.

It seems to work anyways, but I can't update Node that way. Get a message that I didn't install Node via Homebrew. Looking at what version Npm is. Decides to update that. It goes well.

Manage to update Npm to the absolutely latest version.<br>
Manage to update Nvm to the absolutely latest version.<br>
Manage to update Node to the latest(?) version...it has jumped from 3 to 6, from 6 to 7 and now reports it is 15.8. Quite a difference. Don't know if that is the correct/right version still though.

Now I've reached a point where Npm, Nvm, Homebrew and Node at least function as commands on my Mac. Now, where was I? What was I trying to do? Ah, right, "checking-theme-which-led-to-Tailwindcss".

From the Tailwindcss documentation I work out that I need Node 12.13+ so I should now be sufficiently "up-noded". Oh, right, it also instructs me to "install it to a project" bla bla. So I should probably think this through some more, I don't want to install it where I am (my user home folder) as it will create project specific things there, and I haven't even downloaded the theme that set me off on this long and winding road in the first place.

Also the [Tailwindcss documentation][6] tells me I will be needing to install something called "PostCSS" as well as an "autoprefixer". It wouldn't surprise me in the slightest if they have their own very specific dependency/package management/handling solution too, which quite likely will have their own dependencies.

This will have to wait though, now it is time for lunch. I shall ponder over, and silently remember the days when there was HTML, CSS and JavaScript. I'm too old for this shit I guess.

So...how is your Friday?



[1]: https://demo.getgrav.org/typhoon/blog/
[2]: https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
[3]: https://docs.npmjs.com/managing-your-profile-settings
[4]: https://bytearcher.com/articles/ways-to-get-the-latest-node.js-version-on-a-mac/
[5]: https://tailwindcss.com/
[6]: https://tailwindcss.com/docs/installation