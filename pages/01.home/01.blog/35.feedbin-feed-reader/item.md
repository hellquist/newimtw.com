---
title: 'Feedbin feedreader'
maintitle: Feedbin
subtitle: '<b>feedreader</b>'
media_order: '276893_watercolor.jpg,regular_article.png,expanded_article.png,hacker_news_expanded.png,hacker_news_headline.png,news_letters.png,tracking_changes.jpg,tracking_changes.png,twitter_list_news.png'
published: true
date: '2021-08-04 14:28'
taxonomy:
    tag:
        - rss
        - feedreader
        - feedbin
hide_git_sync_repo_link: false
body_classes: header-transparent
aura:
    pagetype: article
    description: 'Looking at alternatives for feedreaders I came across Feedbin. This is my view on it and their service.'
    image: 276893_watercolor.jpg
hero_classes: 'overlay-dark-gradient text-light parallax hero-fullscreen title-h1h2'
hero_image: 276893_watercolor.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
header_image: '1'
header_image_file: 276893_watercolor.jpg
summary:
    enabled: '1'
    delimiter: '==='
hide_from_post_list: false
feed:
    limit: 10
metadata:
    description: 'Looking at alternatives for feedreaders I came across Feedbin. This is my view on it and their service.'
    'og:url': 'https://imakethingswork.com/blog/feedbin-feed-reader'
    'og:type': article
    'og:title': 'Feedbin feedreader | I Make Things Work'
    'og:description': 'Looking at alternatives for feedreaders I came across Feedbin. This is my view on it and their service.'
    'og:image': 'https://imakethingswork.com/user/pages/01.home/01.blog/35.feedbin-feed-reader/276893_watercolor.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '1280'
    'og:image:height': '926'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Feedbin feedreader | I Make Things Work'
    'twitter:description': 'Looking at alternatives for feedreaders I came across Feedbin. This is my view on it and their service.'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/user/pages/01.home/01.blog/35.feedbin-feed-reader/276893_watercolor.jpg'
    'article:published_time': '2021-08-04T14:28:00+02:00'
    'article:modified_time': '2021-08-04T18:45:32+02:00'
    'article:author': 'I Make Things Work'
---

Looking at alternatives for feedreaders I came across Feedbin. This is my view on it and their service.

===

After my [Feedly article][1] I got a few comments saying *"who uses RSS feeds these days?"* and *"why don't you just read the articles where you find them?"*.

The answers are "consolidation" and "privacy" along with "saving time".

Finding good quality news is hard and takes time. A lot of it is behind paywalls and/or on places/sites that are littered with advertising. Feedreaders get around some of those issues by only showing me news from sources I have chosen, without advertising, in one app where all articles look similar and according to my settings. I could expand on the benefits if you'd like but for now:

### Feedbin

I decided to try [Feedbin][2]. I'm home. It is great. It is turning in to my fave service for getting news, from all types of sources, be it RSS feeds, news letters via email and also Twitter, all consolidated into segments/categories I have set/decided.

!!!! Just to clarify: this article does not contain any paid-for links. This is just me wishing to inform you of a service I really like but I'm not getting paid for writing this. In fact, I haven't even started paying for Feedbin myself just yet as I'm still in my 14-day trial of their service.

[![](regular_article.png?classes=caption,caption-right,figure-right "Regular article")](regular_article.png)

The regular articles look like well laid out articles with a bunch of settings for fonts/typography & colours, which was kind of expected. In this sense it doesn't differ a lot from other alternatives in general, and [Feedly][3] (which is what I used to use) in particular. It looks better than a lot of the lesser known alternatives though, where some of the alternatives feel like remnants from the 2000's.

### Twitter lists

What I couldn't get in Feedly though was Twitter lists. Well, I could in Feedly too, if I subscribed to "Pro+" (for a higher subscription cost) as opposed to the subscription I had, "Pro", which was the only subscription option when I went for it. Feedbin handles tweets and Twitter lists just fine though. It even shows you the original tweet but more importantly goes out to fetch the article that is linked to from that tweet, which is great.

[![](expanded_article.png?classes=caption,caption-right,figure-right "Expanded article")](expanded_article.png)

Not only do I **not** have to follow news sources to add them to a list. This is great as some of them I do not wish to inflate their follower number for, others I don't want to be confused for being a supporter of, but I still wish to see their coverage of certain topics, to avoid only getting news from a single source/side.

So I have a Twitter list called "[News - All sides][4]" where I've added various news sources (from...umm..."all" sides, as I like to keep track of what they all say, not just from one of the "bubbles").

Feedbin takes the list of tweets and shows me the original tweet, however (and this is the trick) it ALSO expands the tweet with the article that is linked in the tweet. This makes it MUCH more digestable, at a lower effort, than reading those tweets on Twitter.

Also, for twitter "threads", they simply turn in to one "article" in Feedbin. One "thread" = one article, with a start/finish. Much more readable.

### Expanding all article types

[![](hacker_news_headline.png?classes=caption,caption-right,figure-right "Hacker News headline")](hacker_news_headline.png)

The same goes with feeds from for example HackerNews etc, which usually is just a headline and a link. I get the full article within Feedbin without having to visit the link  myself.

[![](hacker_news_expanded.png?classes=caption,caption-right,figure-right "Hacker News expanded")](hacker_news_expanded.png)

If the article isn't automatically expanded, most often because the feed is in a certain format, I can still just press "C" on my keyboard and Feedbin goes out and fetches the article in the post.

### News letters

[![](news_letters.png?classes=caption,caption-right,figure-right "News letters")](news_letters.png)

And, finally, the news letter thing: I get a secret email address for Feedbin. If I swap out my email address for various news letters to that address, the mails come in to Feedbin, and perhaps as importantly, **out of my mail inbox**, decluttering my mail clients.

As I follow a bunch (around 60 I think) of news letter authors on Substack (which apparently is the publishing service of the hour), this takes a huge load off my mail client.

### Track changes in articles

One interesting feature in Feedbin is that it also keeps track of changes to the articles. This means I can see what has changed or been corrected within the article content.

[![](tracking_changes.png?classes=caption,caption-right,figure-right "Tracking changes")](tracking_changes.png)

Altogether this means Feedbin becomes the main source for news intake for me, regardless of if it is tweets, feeds, mails etc, all managed in a superslick web interface (or app on mobile) that is made for reading, and it saves me a lot of time.

### Conclusion

All in all this means I have found a good replacement from Feedly in Feedbin and I'm really quite happy about it. Not only is Feedbin nice and slick, it also has a good iOS app that works on both iPhone/iPad. It is becoming my one-stop-shop for my daily news intake. It doesn't have a desktop client though. However, as it is a web based service, for those times when I wish to "click out" to an article my browser obviously keeps track of the news sources where I am a subscriber already, creating a seamless experience for me, which is great.

There are all kinds of "helpers" and "tools" out there to wrap it in a solution that makes it feel like a desktop app, but I haven't bothered with those, partly because I am thinking those solutions wouldn't necessarily keep track of the various news sources in such a fine-grained way as I already have in my browser ([Firefox][5]) where I am using specific containers for specific services, which I have [outlined in an older article][6] and which is still as relevant.

[1]: /blog/bye-bye-feedly
[2]: https://feedbin.com/
[3]: https://feedly.com/
[4]: https://twitter.com/i/lists/1336603443356725249
[5]: https://www.mozilla.org/en-US/firefox/new/?redirect_source=firefox-com
[6]: /blog/setting-up-firefox