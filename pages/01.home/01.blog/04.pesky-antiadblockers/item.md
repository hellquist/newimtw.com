---
title: 'Pesky Anti-ad-blockers'
media_order: photo-1449528561296-e24f6c80d8c8-1400x933.jpeg
date: '2018-02-02 23:28'
taxonomy:
    tag:
        - privacy
        - advertising
        - mobile
        - adblock
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /privacy/anti-ad-blockers
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2'
hero_image: photo-1449528561296-e24f6c80d8c8-1400x933.jpeg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: website
---

I found this as a Facebook post under "today a previous year" or whatever it is called. As my opinion on this hasn't changed, and it still is a problem, I figured I should publish the text here instead of having it locked in on Facebook.

===

Here it goes:

Things that rub me the wrong way when it comes to “mobile” web sites and ads, and also in some cases mobile apps that mainly use web sites in the background:

  1. You load a site in your device. You see what you were looking for. Sadly the page hasn’t loaded fully due to a million ads placed on it, so your click/tap doesn’t time well with the headline you actually clicked/tapped on, but on the advertisment that hadn’t loaded yet but should be placed on those coordinates you clicked/tapped, which in turn opens up a new window in your browser, trying to sell you stuff you don’t need.

  2. You load a site in your device. You think it has all loaded, as you have been waiting (due to point 1 above) so you start to scroll down to get to the content you were looking for. The page has in fact not loaded fully, and when it finishes loading of the various ads they all execute a script that pushes you up to the top of the page. You start scrolling as you think it is all loaded. It isn’t. When the next ad is loaded it pushes you up to the top of the page. Rince. Repeat. Scream.

  3. You load a site in your device. You think it has all loaded as you have waited patiently (due to points 1 & 2 above) even though you are on a mega-super-duper fast connection. You start scrolling and you actually manage to read a couple of paragraphs of the article you came there for. All of a sudden it all dims down, and there is a grey layer on top of everything. You sigh, as you now know you have to play hide-and-seek with an overlay that tries to make you sign up for a newsletter, or an “important message” or whatever the site owner has conjured up. Most likely it will be placed somewhere where another script has decided is “the middle of the screen”, which may or may not be off screen on your device.

So, you install <strike>Chrome</strike> Firefox and you install adblockers, just like on your desktop machine, but this time in your device instead…

  1. You load a site in your device. You wait patiently despite having adblocker installed, as you know there are ways for advertisements to fool ad-blockers, which may well lead to points 1, 2 or 3 above. When you think the page is fully loaded you find the article you came there for and start reading. All of a sudden it all dims out, with a grey layer on top of everything. You suspect point 3 (above) has happened, but oh no, lo and behold, NOW it is a message from the site owner saying “hey man, you are using an adblocker and that seriously hurts our business, please disable it”.

Wait. What? NO!!!! YOU fix your site and your content. If you want to monetise on your content, write content good enough that I’d pay for it! I would, I really would. Now FO!!