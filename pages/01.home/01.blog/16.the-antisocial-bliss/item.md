---
title: 'The anti-social bliss'
media_order: '239005.jpg,lamp.jpg'
date: '2019-04-17 23:36'
taxonomy:
    tag:
        - privacy
        - mastodon
        - services
        - tracking
hide_git_sync_repo_link: false
body_classes: header-transparent
visible: false
routes:
    aliases:
        - /article/the-anti-social-bliss
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2 hero-fullscreen'
hero_image: 239005.jpg
blog_url: /blog
show_sidebar: true
show_breadcrumbs: true
show_pagination: true
hide_from_post_list: false
feed:
    limit: 10
aura:
    pagetype: article
maintitle: 'The <b>anti-social</b> bliss'
---

Due to both work and interests I have been very active on social media for many years. All of them. Facebook, Twitter, Instagram etc, but also the old ones that are no longer in business. Basically, if there was a new service I signed up on it and tried it out. Often it was to figure out what the service was all about and thus also to figure out how it worked.

===

If it did something new, and/or in an interesting way, it would then be possible to incorporate similar functionality for whatever services or customers we had at work. If nothing else it was good to find out what the users of a service expected in their user journey.

I'm my new world order I have now cut back on my usage of social media services quite drastically, much due to privacy reasons and how those services treat my personal information. At first I figured I would be using Facebook, Twitter and the other services just as much as previously, and that I would just tweak my settings to be more privacy concious. After a while I started feeling I wanted to, if possible, ditch or at least cut down my usage of the "big" social media services to use something that is more in line with what I think should be the route forward, some of which is included in this list:

* [Federated networks][2]
* Ideally they should be [Decentralized/Distributed][3]
* No, or at least less, advertising.
* I can be more in control, both over what data I share and also what is shared to me.

Also, as outlined in previous posts on this blog, I wanted to get away from the negative people that are seemingly everywhere, complaining about everything but doing absolutely nothing about it themselves apart from complaining even more about the people who *are* trying to do something about it, as they **obviously** are doing it wrongly. Etc.

A couple of weeks into it all I have some reflections. Some of them I will cover in this post.

### Less of everything

Firstly the most surprising thing to me: I basically have cut down on usage of **all** social media. And it feels **great!** No really. I literally feel happier by not even *seeing* peoples complaints about arbitrary things they feel they have to voice their opinion about and which they are **not** topic experts on (despite them claiming the opposite). I don't see them. They are quite probably still right where I left them, but I'm not there, and my absence from them and their bile has improved my life.

It has also given me more time on each day to do "normal", or at least other, things. Because lets face it: posting about every minute about your life online shouldn't be normal. Reading someone elses posts about every minute of their life shouldn't be normal. I never did either to that degree, apart from possibly posting photos when out travelling, and posting a funny joke now and then, along with the daily Happy Birthday greetings (which I still do). But I checked the services a lot more than I posted, and I could see everyone else doing it. Some of their posts made me interact with them, or made me feel things. Too often that feeling was being upset or even, at times, anger.

Today, by not concerning myself with a so-called "news" feed that contains everyones breakfast, their dogs or their latest rant on something, it means I don't have to empathize, or spend brain cycles on them, which I then can use for other more positive things in my own life. One of the goals I outlined in a [previous post][1] is being met.

And for the few times I actually look at Facebook: my re-newed "don't Like things on Facebook" also has done wonders to my "news" feed on Facebook when I do pop in my head there now and then. Again. I knew this beforehand as I've done it before, but it is extra clear now when I also don't really post content myself. Facebook is now doing *its utmost* when I login to fill my feed with things that I possibly could be interested in interacting with.

This in turn means I see a lot more posts from other/older friends that I haven't seen posts from in a long while. I haven't completely "not liked" *everything*, but I have been extremely selective, and the general guiding rule is that I don't "Like" anything. The exception to that rule can be when a London mate I haven't seen in years posts a photo of his newborn child or something, which is an alright exception I think. Also I do "Like" some comments on my own rare posts these days, but it is so far only positive people I care about who has commented in any case, and I don't mind their posts having a higher frequency in my feed.

### Building slowly

![A lit oil lamp in a dark night](lamp.jpg?classes=caption,caption-right,figure-right "Controlled light")

Another reflection is related to [Mastodon][6] and "social media friends". On the "old" social networks like Facebook and Twitter I have literally 1000's of "friends". On Mastodon I started recently and afresh. At the time of writing this I follow about 30 people on there. About 20 follow me. Quite a few of those are in turn not overly active. The feed is nice and slow. Actually the most active account I follow is from Reuters World News. Reuters rate well in **not** being biased (on for example [Media Bias Fact Check][4] and [AllSides][5]) and thus my Mastodon feed also becomes my "World News Feed", mixed up with posts from the people I follow.

It is also nice to carefully add who to follow for once. Now I check if they seem interesting and are providing good content, otherwise I treat the post I noticed them via as just that: a good/funny post from someone passing by. I don't need to add them just yet. I don't need to add anyone just yet, unless they provide me with something that is good.

That also has to be said about Mastodon: in general the users of Mastodon are more "clued up" than if I compare with for example Facebook. Perhaps it is the fact that you have to be pretty nerdy to use a competitor (to both Facebook & Twitter) in the first place. Perhaps it is that the people on Mastodon actually don't post about their breakfasts every day unless it is truly extraordinary. But most posts on Mastodon seem to be more carefully crafted compared to the other big Social Networks.

Obviously you can find the odd/weird ones there too, and if you look for spam or porn etc, you will quite likely find it. But it isn't there up in your face unless you search for it. Also as many users have clustered together on Mastodon instances that often are related to their hobbies or topic interests, they tend to keep their posts on those topics too. That is actually one of the reasons to why I'm currently quite happy I'm still on the big "mastodon.social" instance: I see it as a learning experience, and my hobbies/interests are too varied that I wouldn't feel comfortable with only posting about one of them. I like the freedom of being able to post about whatever I feel like right then. Or, like now, not posting that much at all, anywhere.

I can foresee that I'll possibly create my own "instance-of-one" in the future, but that only makes sense if I have friends to migrate with me, otherwise my own instance will be very quiet indeed.

But in general, and most importantly, I'm now more happy. Maybe I'm just a grumpy old man who should avoid people as much as possible. :)

[1]: /privacy/tightening-up-privacy
[2]: https://en.wikipedia.org/wiki/Federation_(information_technology)
[3]: https://medium.com/delta-exchange/centralized-vs-decentralized-vs-distributed-41d92d463868
[4]: https://mediabiasfactcheck.com/
[5]: https://www.allsides.com/media-bias/media-bias-ratings
[6]: https://mastodon.social/@hellquist