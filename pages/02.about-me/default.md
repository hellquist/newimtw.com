---
title: 'About me'
date: '2020-04-27 16:01'
hide_git_sync_repo_link: true
published: true
aura:
    pagetype: website
metadata:
    'og:url': 'https://imakethingswork.com/about-me'
    'og:type': website
    'og:title': 'About me | I Make Things Work'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'About me | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'article:published_time': '2020-04-27T16:01:00+02:00'
    'article:modified_time': '2021-07-28T14:47:01+02:00'
    'article:author': 'I Make Things Work'
---

**The short version:** Working with Business Development & Innovation, IT Architecture, User Experience. Learning Infosec/Cybersecurity. Photographer, Father, Geek, Guitarist, Headbanger, Musician.

There is also the longer version…


Hi, my name is Mathias Hellquist and I write imakethingswork.com, mainly for my own amusement, but still, it is fun that you have found your way here. It has to be said though, I got so busy with work I sort of stopped blogging, and I kind of do it in bursts anyways, even when I do blog. Currently here isn’t much to see though, especially as I’ve cleared out a lot of old stuff that wasn’t relevant any longer.

**Quick facts about me:** I’m 54 years old, I’m Swedish and these days live in Sweden after having spent 12 years in other countries, working in London (11 years) and a brief stint in Hamburg before that.

I am married to [Ssu-Pi](https://flic.kr/p/iXHvhU) and I have two extraordinary amazing kids, one boy ([Emil](https://www.instagram.com/p/9RC0KZnaYh/), who now is 17) and one girl ([Emma](https://www.instagram.com/p/8tozcYnaft/), she now is 14).

I have been doing Internet, online marketing and IT since 1993, commercially since 1994, and I have done so in various companies in Sweden (Projector, Catwalk, Tieto, Society46, Telia), Germany (I-D Media) and the UK (I-D Media, Halogen, Dare, Profero), most often as the Chief Geek (Creative Technology Director etc), and if you’d like to find out more about those bits you can have a look at my [LinkedIn profile](https://se.linkedin.com/in/mathiashellquist).

In my spare time I mainly [make music](https://ayepad.com/) as well as photograph a lot ([Flickr](https://www.flickr.com/photos/hellquist/)/[Instagram](https://www.instagram.com/mathiashellquist/)/[500px](https://500px.com/hellquist)). Also, being a geek, I tweak stuff that involves code, often trying to [optimize my operating system](https://www.flickr.com/photos/hellquist/3491092645/sizes/l/in/photostream/), regardless of which one I am using at the time, but most often something Linux based at home, though I also use my beloved Macs for Design and creating music, as well as Windows machines, mainly for gaming.

!!!! **General info:** This site contains no tracking links. It contains no statistics package that can follow you and/or me. It is possible that Adobe/Typekit can deduct I indeed have visitors, as I'm using one of their web fonts. I often link out to relevant content instead of re-typing it myself. Click those links, then come back and continue reading.

! **Disclaimer 1, for textual content:** All textual content on this site is written by me, unless otherwise noted. I may or may not quote others. In such occasions I do try to make it clear "who" said "what", and ideally also "when" (though often not as important). All my textual content is licensed under the Creative Commons, as per outlined at the bottom of each page.

! **Disclaimer 2, for images:** As far as possible I try to use images/photos that I have taken myself, be it from my camera or via screenshots. The main exceptions to that are most often the dashing "hero" images on top of articles, that usually comes from [Unsplash](https://unsplash.com/), but could come from other places and where the license allows me to use them. If you think I have used your image wrongly/incorrectly, please contact me and we can discuss (I will most likely just remove it).

!! **Disclaimer in general:** I am not here to solve world peace, pollution or your problems (I have my hands full with my own problems and their solutions) and the opinions, thoughts and other expressions on this web site are mine, those of me, personally, not the ones of my current or previous employer(s) or any other organisation or body I might have participated in/with.

!!! **Fun fact:** If you think I have written something factually wrong (which then doesn't include opinions, even if my opinion differs from yours), or if you spot spelling mistakes (here are plenty, and I am Swedish so English isn't actually my first language) and you have loads of time to kill, the content of this site is completely possible to edit. Click on the "edit" link that you'll find on each page, it does require a [Gitlab](https://gitlab.com) account, and you can go in and change whatever you'd like and then submit that as a request for me to consider changing to. Yeah, Open Source baby!

## Quick FAQ

### Q: Didn’t you use to run the d-x-b design portal a few years ago?

A: Yep, during late 90’s and early noughties. Didn’t have time to update it as often as I wanted though, due to commitments at work and I found that particular “market segment” quite full with loads of good sites out there covering both great design, interactive development and web development in general I quit whilst on top. I did keep the domain for a while, but last year I forgot to renew it, and it is now with someone else. EDIT: I have now bought it back again, though I haven’t gotten anything on it, or done anything with it, I just wanted to own it as it is a major part of my online history.

### Q: Didn’t you use to play guitar in Europe, the band (Final Countdown etc)?

A: Heh. Nope. I did attend the gig when the video for Final Countdown was recorded, but not even my mother can pick me out in the audience.

The main reason for the confusion (not on my part, trust me) is probably due to the fact that one of the bands I did play guitar in (called Thrash Inc) did win the very same competition (Rock SM) that Europe had won a few years earlier, and which kick-started their career. We played a different type of metal though (less hair, more brutality) and were quite fed up with the confusion at gigs etc where we didn’t sound at all Europe-ish (which was what the venue bookers were thinking). So much fed up in fact that we changed name of the band (to Clench, yeah, I know, more “buttocks” than “fists” etc) when we released our first album.

The other guitarist in our band did buy a few Marshall stacks that had belonged to the original Europe guitarist (Sweden isn’t extremely big in population, most metal-heads/rockers are only 1-2 degrees separated), but that is about it.  
Have a look at the [Bandcamp site for Thrash Inc/Clench](https://thrashinc-clench.bandcamp.com/releases) to have a listen.

### Q: So now you are making music solo instead?

Well, yes, though I’m mainly doing electronic music these days. If you want to check that out you can have a look at the web site for my music alter ego [AyePad](https://ayepad.com/), or check out the [Bandcamp site for AyePad](https://ayepad.bandcamp.com/).

You can also find my music on all the major services, such as Spotify, iTunes, Google Play, Amazon etc. Just search for AyePad and you shall find.

### Q: Didn’t you use to role play (AD&D etc)?

A: Yep, quite a bit. I’m an old school geek and back in -83 I started one of Swedens first role playing communities, FEAR (entirely off-line, obviously). I should possibly add that when I talk about Role Playing, I am talking about pens, paper, dice, long nights with loads of snacks etc, I am NOT talking about Live Roleplaying, which is a different beast (doh) all together.

Actually we still manage to do roleplaying, at least once a month, though we are all fathers and professional IT managers these days.

### Q: Didn’t you use to play Paint Ball quite a bit?

A: Yes, again, I started Swedens first legal (it used to be illegal) paint ball community (offline). I quit it though when people took it too seriously. If you can’t have fun doing it (anything in fact) you shouldn’t do it at all.

### Q: Is it true you used to be a Quake 2 Rocket Arena 2 God?

A: Nope. But I was indeed “clan leader” for Swedens (and thereby, by most peoples scale at the time, the worlds) best clan (Advanced Core Military Engine, ACME). In that clan we had loads of great (and high ranking…or is that “low” ranking…anyways, whichever is the better and indicate world domination) players, including most of the Top10 Ranking in the world. Myself, even then, I was better at the networking (and chatting) and hovered around the Top100 mark when I was at my best (which isn’t too bad given it was amongst 13 million players), so “God”? No. I can still hold my own though. ;)
