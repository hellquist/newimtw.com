---
title: Sidebar
media_order: mathias2.png
hide_git_sync_repo_link: false
routable: false
visible: false
aura:
    pagetype: website
position: top
metadata:
    'og:url': 'https://imakethingswork.com/sidebar'
    'og:type': website
    'og:title': 'Sidebar | I Make Things Work'
    'og:image': 'https://imakethingswork.com/sidebar/mathias2.png'
    'og:image:type': image/png
    'og:image:width': '540'
    'og:image:height': '540'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Sidebar | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/sidebar/mathias2.png'
    'article:published_time': '2021-02-16T11:33:19+01:00'
    'article:modified_time': '2021-02-16T11:33:19+01:00'
    'article:author': 'I Make Things Work'
---

&nbsp;

![](mathias2.png?classes=u-photo)

#### <a rel="author" class="p-author p-name u-url" href="/">Mathias Hellquist</a>

<p class="p-note">Working with Business Development & Innovation, IT Architecture, User Experience. Learning Infosec/Cybersecurity. Photographer, Father, Geek, Guitarist, Headbanger, Musician.</p>
    
Read more on the [About me page](/about-me).

<script type='text/javascript' src='https://storage.ko-fi.com/cdn/widget/Widget_2.js'></script><script type='text/javascript'>kofiwidget2.init('Buy me a coffee', '#29abe0', 'U7U05RJ3L');kofiwidget2.draw();</script>

<br>

Follow my learning on Infosec:

<a href="https://tryhackme.com/p/HellQuest"><img src="https://tryhackme-badges.s3.amazonaws.com/HellQuest.png" alt="TryHackMe"></a>

Would you like to receive an email whenever I post something new? Sign up to [my posts on Substack](https://hellquist.substack.com/) or follow/subscribe to [my posts on Medium](https://hellquist.medium.com/).

<link href="https://github.com/hellquist" rel="me" />
<link href="https://www.instagram.com/mathiashellquist/" rel="me" />
<link href="https://flickr.com/people/hellquist/" rel="me" />
<link href="https://mastodon.social/web/accounts/7877" rel="me" />
<link href="https://twitter.com/hellquist" rel="me" />
<link href="https://www.facebook.com/mathias.hellquist" rel="me" />
