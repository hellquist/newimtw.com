---
title: Footer
hide_git_sync_repo_link: false
routable: false
visible: false
aura:
    pagetype: website
metadata:
    'og:url': 'https://imakethingswork.com/footer'
    'og:type': website
    'og:title': 'Footer | I Make Things Work'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Footer | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'article:published_time': '2020-11-10T09:22:56+01:00'
    'article:modified_time': '2020-11-10T09:22:56+01:00'
    'article:author': 'I Make Things Work'
---

Another site made by [ideasthatwork.se](https://ideasthatwork.se)<br>
<a class="h-card" rel="me" href="https://imakethingswork.com/">Mathias Hellquist</a>
