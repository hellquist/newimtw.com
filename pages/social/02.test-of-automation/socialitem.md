---
title: 'Test of automation'
date: '2020-05-03 12:56'
taxonomy:
    tag:
        - social
hide_git_sync_repo_link: true
external_links:
    process: false
    title: false
    no_follow: false
visible: false
blog_url: /social
show_sidebar: true
show_breadcrumbs: false
show_pagination: true
summary:
    enabled: '0'
hide_from_post_list: false
aura:
    pagetype: article
feed:
    limit: 10
---

Now I shall be posting this on my web site only and see if it gets autoposted to Twitter and/or Mastodon.

<a href="https://brid.gy/publish/mastodon"></a><a href="https://brid.gy/publish/twitter"></a>