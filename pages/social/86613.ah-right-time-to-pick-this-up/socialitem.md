---
title: 'Ah right, time to pick this up'
published: true
date: '2020-11-11 13:59'
hide_git_sync_repo_link: true
aura:
    pagetype: article
blog_url: /social
show_sidebar: true
show_breadcrumbs: false
show_pagination: true
service_twitter: '1'
service_mastodon: '1'
hide_from_post_list: false
feed:
    limit: '10'
    description: ''
metadata:
    'og:url': 'https://imakethingswork.com/social/ah-right-time-to-pick-this-up'
    'og:type': article
    'og:title': 'Ah right, time to pick this up | I Make Things Work'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Ah right, time to pick this up | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'article:published_time': '2020-11-11T13:59:00+01:00'
    'article:modified_time': '2020-11-11T14:04:03+01:00'
    'article:author': 'I Make Things Work'
---

Ah right, I had forgotten I was trying to build a thing on my site that publishes tweets and messages to Twitter/Mastodon automagically, but never got it to work properly and I forgot about it. Time to have a look again I think.