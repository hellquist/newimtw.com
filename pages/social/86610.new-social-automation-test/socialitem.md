---
title: 'New social automation test'
date: '2020-05-05 19:49'
hide_git_sync_repo_link: false
visible: false
blog_url: /social
show_sidebar: true
show_breadcrumbs: false
show_pagination: false
service_twitter: '1'
service_mastodon: '1'
hide_from_post_list: false
aura:
    pagetype: website
---

Time to make another test to see both if automation of social publishing works, and also if so, what the actual post looks like.