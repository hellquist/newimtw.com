---
title: 'Testing new services'
date: '2020-05-02 17:13'
hide_git_sync_repo_link: true
external_links:
    process: false
    title: false
    no_follow: false
visible: false
blog_url: /social
show_sidebar: true
show_breadcrumbs: false
show_pagination: false
summary:
    enabled: '0'
hide_from_post_list: false
aura:
    pagetype: website
feed:
    limit: 10
maintitle: 'Testing new services'
---

Came across some new services today that I need to try out. Let's see where this ends up. Will this be automatically posted from my site?

<a href="https://brid.gy/publish/mastodon"></a><a href="https://brid.gy/publish/twitter"></a>