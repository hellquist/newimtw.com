---
title: Social
media_order: 86609.jpg
published: false
date: '2020-05-02 17:11'
hide_git_sync_repo_link: true
external_links:
    process: false
aura:
    pagetype: website
    image: 86609.jpg
hero_classes: 'text-light parallax overlay-dark-gradient title-h1h2 hero-large'
hero_image: 86609.jpg
blog_url: /social
show_sidebar: true
show_breadcrumbs: false
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 10
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
bricklayer_layout: false
display_post_summary:
    enabled: false
metadata:
    'og:url': 'https://imakethingswork.com/social'
    'og:type': website
    'og:title': 'Social | I Make Things Work'
    'og:image': 'https://imakethingswork.com/social/86609.jpg'
    'og:image:type': image/jpeg
    'og:image:width': '900'
    'og:image:height': '537'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'Social | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'twitter:image': 'https://imakethingswork.com/social/86609.jpg'
    'article:published_time': '2020-05-02T17:11:00+02:00'
    'article:modified_time': '2021-02-06T01:39:57+01:00'
    'article:author': 'I Make Things Work'
feed:
    limit: 10
    description: 'IMTW - Social posts'
---

