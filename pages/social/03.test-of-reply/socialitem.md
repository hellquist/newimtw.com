---
title: 'Test of reply'
date: '2020-05-03 15:01'
hide_git_sync_repo_link: true
external_links:
    process: false
    title: false
    no_follow: false
visible: false
blog_url: /social
show_sidebar: true
show_breadcrumbs: false
show_pagination: true
summary:
    enabled: '0'
hide_from_post_list: false
aura:
    pagetype: website
feed:
    limit: 10
---

https://twitter.com/hellquist/status/1256901953357000708

...and here's a test of a reply (to myself). Will it work?

<a href="https://brid.gy/publish/twitter"></a>