---
title: 'Aaaand another test'
date: '2020-05-05 19:59'
hide_git_sync_repo_link: true
external_links:
    process: false
    title: false
    no_follow: false
    mode: passive
visible: false
blog_url: /social
show_sidebar: true
show_breadcrumbs: false
show_pagination: false
service_twitter: '1'
service_mastodon: '1'
hide_from_post_list: false
aura:
    pagetype: website
---

It looks like my CMS is adding classes to the links that ensure them being ignored by automation services.<br><br><a href="https://brid.gy/publish/mastodon"></a><a href="https://brid.gy/publish/twitter"></a>