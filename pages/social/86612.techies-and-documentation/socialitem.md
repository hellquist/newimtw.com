---
title: 'Techies and documentation'
date: '2020-05-05 21:36'
hide_git_sync_repo_link: false
external_links:
    mode: passive
visible: false
blog_url: /social
show_sidebar: true
show_breadcrumbs: false
show_pagination: false
service_twitter: '1'
service_mastodon: '1'
hide_from_post_list: false
aura:
    pagetype: website
---

Techies and developers should probably let "users" UX test their documentation. The web is full of half-baked instructions. :/