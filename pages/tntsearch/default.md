---
title: tntSearch
published: false
hide_git_sync_repo_link: true
visible: false
aura:
    pagetype: website
hero_classes: ''
hero_image: ''
header_image_alt_text: ''
header_image_credit: ''
header_image_creditlink: ''
blog_url: /blog
show_sidebar: '1'
show_breadcrumbs: '1'
show_pagination: '1'
content:
    items: '@self.children'
    limit: '5'
    order:
        by: date
        dir: desc
    pagination: '1'
    url_taxonomy_filters: '1'
bricklayer_layout: '1'
display_post_summary:
    enabled: '0'
post_icon: ''
metadata:
    'og:url': 'https://imakethingswork.com/tntsearch'
    'og:type': website
    'og:title': 'tntSearch | I Make Things Work'
    'fb:app_id': '280913550514'
    'og:author': 'I Make Things Work'
    'twitter:card': summary_large_image
    'twitter:title': 'tntSearch | I Make Things Work'
    'twitter:site': '@hellquist'
    'twitter:creator': '@hellquist'
    'article:published_time': '2020-11-10T10:13:33+01:00'
    'article:modified_time': '2021-02-06T01:35:15+01:00'
    'article:author': 'I Make Things Work'
feed:
    limit: '10'
    description: ''
icon: search
---

